# PRACTICAS-SQL



## Juan Daniel Zapana Lucano

## Solucionario

[1. Crear tablas (create table - describe - all_tables - drop table)](Sesión_03/consultas_sql.md)

[2. Ingresar registros (insert into- select)](Sesión_03/consultas_sql.md)

[3. Tipos de datos](Sesión_03/consultas_sql.md)

[4. Recuperación de algunos campos (select)](Solucionario/Solucionario_4.md)

[5. Recuperar algunos registros (where)](Solucionario/Solucionario_5.md)

[6. Operadores relacionales](Solucionario/Solucionario_6.md)

[7. Borrar registros (delete)](Solucionario/Solucionario_7.md)

[8. Actualizar registros (update)](Solucionario/Solucionario_8.md)

[9. Comentarios](Solucionario/Solucionario_9.md)

[10. Valores nulos (null)](Solucionario/Solucionario_10.md)

[11. Operadores relacionales (is null)](Solucionario/Solucionario_11.md)

[12. Clave primaria (primary key)](Solucionario/Solucionario_12.md)

[13. Vaciar la tabla (truncate table)](Solucionario/Solucionario_13.md)

[14. Tipos de datos alfanuméricos](Solucionario/Solucionario_14.md)

[15. Tipos de datos numéricos](Solucionario/Solucionario_15.md)

[16. Ingresar algunos campos](Solucionario/Solucionario_16.md)

[17. Valores por defecto (default)](Solucionario/Solucionario_17.md)

[18. Operadores aritméticos y de concatenación (columnas calculadas)](Solucionario/Solucionario_18.md)

[19.  Alias (encabezados de columnas)](Solucionario/Solucionario_19.md)

[20. Funciones string](Solucionario/Solucionario_20.md)

[21. Funciones matemáticas](Solucionario/Solucionario_21.md)

[22. Funciones de fechas y horas](Solucionario/Solucionario_22.md)

[23. Ordenar registros (order by)](Solucionario/Solucionario_23.md)

[24. Operadores lógicos (and - or - not)](Solucionario/Solucionario_24.md)

[25. Otros operadores relacionales (between)](Solucionario/Solucionario_25.md)

[26. Otros operadores relacionales (in)](Solucionario/Solucionario_26.md)

[27. Búsqueda de patrones (like - not like)](Solucionario/Solucionario_27.md)

[28. Contar registros (count)](Solucionario/Solucionario_28.md)

[29. Funciones de grupo (count - max - min - sum - avg)](Solucionario/Solucionario_28.md)

[30. Agrupar registros (group by)](Solucionario/Solucionario_30.md)

[31. Seleccionar grupos (Having)](Solucionario/Solucionario_31.md)

[32. Registros duplicados (Distinct)](Solucionario/Solucionario_32.md)

[33. Clave primaria compuesta](Solucionario/Solucionario_33.md)

[34. Secuencias (create sequence - currval - nextval - drop sequence)](Solucionario/Solucionario_34.md)

[35. Alterar secuencia (alter sequence)](Solucionario/Solucionario_35.md)

[36. Integridad de datos](Solucionario/Solucionario_36.md)

[37. Restricción primary key](Solucionario/Solucionario_37.md)

[38. Restricción unique](Solucionario/Solucionario_38.md)

[39. Restriccioncheck](Solucionario/Solucionario_39.md)

[40. Restricciones: validación y estados (validate - novalidate - enable - disable)](Solucionario/Solucionario_40.md)

[41. Restricciones: información (user_constraints - user_cons_columns)](Solucionario/Solucionario_41.md)

[42. Restricciones: eliminación (alter table - drop constraint)](Solucionario/Solucionario_42.md)

[43. Indices](Solucionario/Solucionario_43.md)

[44. Indices (Crear . Información)](Solucionario/Solucionario_44.md)

[45. Indices (eliminar)](Solucionario/Solucionario_45.md)

[46. Varias tablas (join)](Solucionario/Solucionario_46.md)

[47. Combinación interna (join)](Solucionario/Solucionario_47.md)

[48. Combinación externa izquierda (left join)](Solucionario/Solucionario_48.md)

[49. Combinación externa derecha (right join)](Solucionario/Solucionario_49.md)

[50. Combinación externa completa (full join)](Solucionario/Solucionario_50.md)

[51. Combinaciones cruzadas (cross)](Solucionario/Solucionario_51.md)

[52. Autocombinación](Solucionario/Solucionario_52.md)

[53. Combinaciones y funciones de agrupamiento](Solucionario/Solucionario_53.md)

[54. Combinar más de 2 tablas](Solucionario/Solucionario_54.md)

[55. Otros tipos de combinaciones](Solucionario/Solucionario_55.md)

[56. Clave foránea](Solucionario/Solucionario_56.md)

[57. Restricciones (foreign key)](Solucionario/Solucionario_57.md)

[58. Restricciones foreign key en la misma tabla](Solucionario/Solucionario_58.md)

[59. Restricciones foreign key (eliminación)](Solucionario/Solucionario_59.md)

[60. Restricciones foreign key deshabilitar y validar](Solucionario/Solucionario_60.md)

[61. Restricciones foreign key (acciones)](Solucionario/Solucionario_61.md)

[62. Información de user_constraints](Solucionario/Solucionario_62.md)

[63. Restricciones al crear la tabla](Solucionario/Solucionario_63.md)

[64. Unión](Solucionario/Solucionario_64.md)

[65. Intersección](Solucionario/Solucionario_65.md)

[66. Minus](Solucionario/Solucionario_66.md)

[67. Agregar campos (alter table-add)](Solucionario/Solucionario_67.md)

[68. Modificar campos (alter table - modify)](Solucionario/Solucionario_68.md)

[69. Eliminar campos (alter table - drop)](Solucionario/Solucionario_69.md)

[70.  Agregar campos y restricciones (alter table)](Solucionario/Solucionario_70.md)

[71. Subconsultas](Solucionario/Solucionario_71.md)

[72. Subconsultas como expresion](Solucionario/Solucionario_72.md)

[73. Subconsultas con in](Solucionario/Solucionario_73.md)

[74. Subconsultas any- some - all](Solucionario/Solucionario_74.md)

[75. Subconsultas correlacionadas](Solucionario/Solucionario_75.md)

[76. Exists y No Exists](Solucionario/Solucionario_76.md)

[77. Subconsulta simil autocombinacion](Solucionario/Solucionario_77.md)

[78. Subconsulta conupdate y delete](Solucionario/Solucionario_78.md)

[79. Subconsulta e insert](Solucionario/Solucionario_79.md)

[80. Crear tabla a partir de otra (create table-select)](manual/80.md)

[81. Vistas (create view)](Solucionario/Solucionario_81.md)

[82. Vistas (información)](Solucionario/Solucionario_82.md)

[83. Vistas eliminar (drop view)](Solucionario/Solucionario_83.md)

[84. Vistas (modificar datos a través de ella)](Solucionario/Solucionario_84.md)

[85. Vistas (with read only)](Solucionario/Solucionario_85.md)

[86. Vistas modificar (create or replace view)](Solucionario/Solucionario_86.md)

[87. Vistas (with check option)](Solucionario/Solucionario_87.md)

[88. Vistas (otras consideraciones: force)](Solucionario/Solucionario_88.md)

[89. Vistas materializadas (materialized view)](Solucionario/Solucionario_89.md)

[90. Procedimientos almacenados](Solucionario/Solucionario_90.md)

[91. Procedimientos Almacenados (crear- ejecutar)](Solucionario/Solucionario_91.md)

[92. Procedimientos Almacenados (eliminar)](Solucionario/Solucionario_92.md)

[93. Procedimientos almacenados (parámetros de entrada)](Solucionario/Solucionario_93.md)

[94. Procedimientos almacenados (variables)](Solucionario/Solucionario_94.md)

[95. Procedimientos Almacenados (informacion)](Solucionario/Solucionario_95.md)

[96. Funciones](Solucionario/Solucionario_96.md)

[97. Control de flujo (if)](Solucionario/Solucionario_97.md)

[98. Control de flujo (case)](Solucionario/Solucionario_98.md)

[99. Control de flujo (loop)](Solucionario/Solucionario_99.md)

[100. Control de flujo (for)](Solucionario/Solucionario_100.md)

[101. Control de flujo (while loop)](Solucionario/Solucionario_101.md)

[102. Disparador (trigger)](Solucionario/Solucionario_102.md)

[103. Disparador (información)](Solucionario/Solucionario_103.md)

[104. Disparador de inserción a nivel de sentencia](Solucionario/Solucionario_104.md)

[105. Disparador de insercion a nivel de fila (insert trigger for each row)](Solucionario/Solucionario_105.md)

[106. Disparador de borrado (nivel de sentencia y de fila)](Solucionario/Solucionario_106.md)

[107. Disparador de actualizacion a nivel de sentencia (update trigger)](Solucionario/Solucionario_107.md)

[108. Disparador de actualización a nivel de fila (update trigger)](Solucionario/Solucionario_108.md)

[109. Disparador de actualización - lista de campos (update trigger)](Solucionario/Solucionario_109.md)

[110. Disparador de múltiples eventos](Solucionario/Solucionario_110.md)

[111. Disparador (old y new)](Solucionario/Solucionario_111.md)

[112. Disparador condiciones (when)](Solucionario/Solucionario_112.md)

[113. Disparador de actualizacion - campos (updating)](Solucionario/Solucionario_113.md)

[114. Disparadores (habilitar y deshabilitar)](Solucionario/Solucionario_114.md)

[115. Disparador (eliminar)](Solucionario/Solucionario_115.md)

[116. Errores definidos por el usuario en trigger](Solucionario/Solucionario_116.md)

[117. Seguridad y acceso a Oracle](Solucionario/Solucionario_117.md)

[118. Usuarios (crear)](Solucionario/Solucionario_118.md)

[119. 	Permiso de conexión](Solucionario/Solucionario_119.md)

[120. Privilegios del sistema (conceder)](Solucionario/Solucionario_120.md)

[121. Privilegios del sistema (with admin option)](Solucionario/Solucionario_121.md)

[122. Modelado de base de datos](Solucionario/Solucionario_122.md)

