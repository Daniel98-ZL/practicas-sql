# RESOLUCION Y CONSULTAS EJECUTADAS PARA EL DESARROLLO DEL TRABAJO FINAL
```sql
-- Crear la tabla Cliente
CREATE TABLE Cliente (
    RUC VARCHAR(15) PRIMARY KEY,
    nombre VARCHAR(50),
    apellido VARCHAR(50),
    direccion VARCHAR(100)
);

-- Crear la tabla Empresa
CREATE TABLE Empresa (
    RUC VARCHAR(15) PRIMARY KEY,
    nombre VARCHAR(50),
    direccion VARCHAR(100)
);

-- Crear la tabla Factura
CREATE TABLE Factura (
    id_factura INT PRIMARY KEY,
    RUC_empresa VARCHAR(15),
    estado VARCHAR(50),
    fecha DATE,
    sub_total DECIMAL(10, 2),
    total DECIMAL(10, 2),
    RUC_cliente VARCHAR(15),
    FOREIGN KEY (RUC_empresa) REFERENCES Empresa(RUC),
    FOREIGN KEY (RUC_cliente) REFERENCES Cliente(RUC)
);

-- Crear la tabla Producto
CREATE TABLE Producto (
    id_producto INT PRIMARY KEY,
    cantidad INT,
    descripcion VARCHAR(255),
    precio_unitario DECIMAL(10, 2),
    precio_venta DECIMAL(10, 2),
    id_factura INT,
    FOREIGN KEY (id_factura) REFERENCES Factura(id_factura)
);



-- Insertar registros en la tabla Empresa (San Jorge)
INSERT INTO Empresa (RUC, nombre, direccion)
VALUES ('RUC_20428729201', 'Confecciones San Jorge', 'Dirección Av Simon Bolivar');

-- Insertar registros en la tabla Cliente
INSERT INTO Cliente (RUC, nombre, apellido, direccion)
VALUES ('RUC_10702570871', 'Juan Daniel', 'Zapana Lucano', 'Jr Chirihuanos 183 int 1');
INSERT INTO Cliente (RUC, nombre, apellido, direccion)
VALUES ('RUC_10234565431', 'Andres', 'Lopez Pilco', 'Jr Chirhuanos 171');

CREATE SEQUENCE seq_factura START WITH 1 INCREMENT BY 1;
-- Insertar registros en la tabla Factura (con empresa San Jorge)
INSERT INTO Factura (id_factura, RUC_empresa, estado, fecha, sub_total, total, RUC_cliente)
VALUES (seq_factura.NEXTVAL, 'RUC_20428729201', 'Pendiente', TO_DATE('2023-06-20', 'YYYY-MM-DD'), 100.00, 120.00, 'RUC_10702570871');

INSERT INTO Factura (id_factura, RUC_empresa, estado, fecha, sub_total, total, RUC_cliente)
VALUES (seq_factura.NEXTVAL, 'RUC_20428729201', 'Pagada', TO_DATE('2023-06-19', 'YYYY-MM-DD'), 150.00, 180.00, 'RUC_10234565431');

-- Insertar registros en la tabla Producto (asociados a la factura 1)
INSERT INTO Producto (id_producto, cantidad, descripcion, precio_unitario, precio_venta, id_factura)
VALUES (1, 5, 'Casaca', 100.00, 500.00, 1);
INSERT INTO Producto (id_producto, cantidad, descripcion, precio_unitario, precio_venta, id_factura)
VALUES (2, 3, 'Busos', 50.00, 150.00, 1);

-- Insertar registros en la tabla Producto (asociados a la factura 2)
INSERT INTO Producto (id_producto, cantidad, descripcion, precio_unitario, precio_venta, id_factura)
VALUES (3, 2, 'Polos', 30.00, 60.00, 2);
INSERT INTO Producto (id_producto, cantidad, descripcion, precio_unitario, precio_venta, id_factura)
VALUES (4, 4, 'Chompas', 45.00, 180.00, 2);

SELECT * FROM Empresa;
SELECT * FROM Cliente;
SELECT * FROM Factura;
SELECT * FROM Producto;
```

```sh
Table CLIENTE creado.


Table EMPRESA creado.


Table FACTURA creado.


Table PRODUCTO creado.

1 fila insertadas.

1 fila insertadas.

1 fila insertadas.

Sequence SEQ_FACTURA creado.

1 fila insertadas.

1 fila insertadas.

1 fila insertadas.

1 fila insertadas.

1 fila insertadas.

1 fila insertadas.

```