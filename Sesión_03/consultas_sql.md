# 1. Crear tablas (create table - describe - all_tables - drop table)
## Práctica 01
```sql
create table usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

describe usuarios;

drop table usuarios;

select * from all_tables;

```
# EJERCICIOS
## Ejercicio 1:
### Necesita almacenar los datos de amigos en una tabla. Los datos que guardará serán: apellido, nombre, domicilio y teléfono.
#### 1. Elimine la tabla "agenda" Si no existe, un mensaje indicará tal situación.

```sql
drop table agenda;
```
Salida del Script

Nos muestra un error ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 1 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Intente crear una tabla llamada "*agenda".

```sql
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script

Nos muestra un error ORA-00903: nombre de tabla no válido
```sh
Error que empieza en la línea: 3 del comando :
create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
)
Informe de error -
ORA-00903: nombre de tabla no válido
00903. 00000 -  "invalid table name"
*Cause:    
*Action:

```
#### 3. Cree una tabla llamada "agenda", debe tener los siguientes campos: apellido, varchar2(30); nombre, varchar2(20); domicilio, varchar2 (30) y telefono, varchar2(11).

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script

```sh
Table AGENDA creado.

```

#### 4. Intente crearla nuevamente.Aparece mensaje de error indicando que el nombre ya lo tiene otro objeto.

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script
Nos muestra un error ORA-00955: este nombre ya lo está utilizando otro objeto existente.

```sh
Error que empieza en la línea: 1 del comando :
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
)
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:

```

#### 5. Visualice las tablas existentes (all_tables) La tabla "agenda" aparece en la lista.

```sql
select * from all_tables;

```
Salida del Script

```sh
DANIEL	AGENDA	SENATI			VALID	10		1	255								YES	N									         1	         1	    N	ENABLED			NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	NO	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED

```
#### 6. Visualice la estructura de la tabla "agenda" (describe) Aparece la siguiente tabla:

```sql
describe agenda;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```

## Ejercicio 2:
### Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.
#### 1. Elimine la tabla "libros" Si no existe, un mensaje indica tal situación.

```sql
drop table libros;

```
Salida del Script
Nos da un error ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 1 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Verifique que la tabla "libros" no existe (all_tables) No aparece en la lista.

```sql
select * from all_tables;

```
Salida del Script 
Solo existe nuestra anterior tabla AGENDA.

```sh
DANIEL	AGENDA	SENATI			VALID	10		1	255								YES	N	0	0	0	0	0	0	0	0	         1	         1	    N	ENABLED	0	25/05/23	NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	YES	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED

```

#### 3. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15).

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);

```
Salida del Script

```sh
Table LIBROS creado.

```

#### 4. Intente crearla nuevamente: Aparece mensaje de error indicando que existe un objeto con el nombre "libros".

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);

```
Salida del Script
Nos da un error ORA-00955: este nombre ya lo está utilizando otro objeto existente

```sh
Error que empieza en la línea: 5 del comando :
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
)
Informe de error -
ORA-00955: este nombre ya lo está utilizando otro objeto existente
00955. 00000 -  "name is already used by an existing object"
*Cause:    
*Action:

```

#### 5. Visualice las tablas existentes.

```sql
select * from all_tables;

```
Salida del Script

```sh
DANIEL	AGENDA	SENATI
DANIEL	LIBROS	SENATI

```

#### 6. Visualice la estructura de la tabla "libros": Aparece "libros" en la lista.

```sql
describe libros;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15)

```

#### 7. Elimine la tabla.

```sql
drop table libros;

```
Salida del Script

```sh
Table LIBROS borrado.

```

#### 8. Intente eliminar la tabla Un mensaje indica que no existe.

```sql
drop table libros;

```
Salida del Script
Nos da un error ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 15 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```

# 2. Ingresar registros (insert into- select)

## Práctica 02

```sql
select * from all_tables;

drop table usuarios;

create table usuarios(
    nombre varchar2(30),
    clave varchar2(10)
);

select *from usuarios;

insert into usuarios (nombre, clave) values ('Mariano','payaso');

select *from usuarios;

insert into usuarios (clave, nombre) values ('River','Juan');

insert into usuarios (nombre,clave) values ('Boca','Luis');

select *from usuarios;
```
Salida del Script

```sh
DANIEL	USUARIOS	SENATI			VALID	10		1	255	65536	1048576	1	2147483645				YES	N	0	5	0	0	0	0	0	0	         1	         1	    N	ENABLED	0	25/05/23	NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	YES	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	YES	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED

Table USUARIOS borrado.

Table USUARIOS creado.

1 fila insertadas.

Mariano	payaso

1 fila insertadas.

1 fila insertadas.

Mariano	payaso
Juan	River
Boca	Luis

```
# EJERCICIOS
## Ejercicio 1:
### Trabaje con la tabla "agenda" que almacena información de sus amigos.
#### 1. Elimine la tabla "agenda".

```sql
drop table agenda;

```
Salida del Script

```sh
Table AGENDA borrado.

```
#### 2. Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11).

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```
Salida del Script

```sh
Table AGENDA creado.

```
#### 3. Visualice las tablas existentes para verificar la creación de "agenda" (all_tables).

```sql
select * from all_tables;

```
Salida del Script

```sh
SYS	DBMS_SQLPATCH_FILES	SYSTEM
DANIEL	USUARIOS	SENATI
DANIEL	AGENDA	SENATI

```
#### 4. Visualice la estructura de la tabla "agenda" (describe).

```sql
describe agenda;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(20) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11) 

```
#### 5. Ingrese los siguientes registros:

```sql
insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


```
#### 6. Seleccione todos los registros de la tabla.

```sql
select * from agenda;

```
Salida del Script

```sh
Moreno	Alberto	Colon 123	4234567
Torres	Juan	Avellaneda 135	4458787

```
#### 7. Elimine la tabla "agenda".

```sql
drop table agenda;

```
Salida del Script

```sh
Table AGENDA borrado.

```
#### 8. Intente eliminar la tabla nuevamente (aparece un mensaje de error).

```sql
drop table agenda;

```
Salida del Script
Nos da un error de ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 1 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
## Ejercicio 02
### Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.
#### 1. Elimine la tabla "libros"

```sql
drop table libros;

```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe

```sh
Error que empieza en la línea: 1 del comando :
drop table libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15).

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(30),
    editorial varchar2(15)
);


```
Salida del Script

```sh
Table LIBROS creado.

```
#### 3. Visualice las tablas existentes.

```sql
select * from all_tables;

```
Salida del Script

```sh
DANIEL	LIBROS	SENATI			VALID	10		1	255								YES	N									         1	         1	    N	ENABLED			NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	NO	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED

```
#### 4. Visualice la estructura de la tabla "libros" Muestra los campos y los tipos de datos de la tabla "libros".

```sql
describe libros;

```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 

```
#### 5. Ingrese los siguientes registros:

```sql
insert into libros (titulo,autor,editorial) values ('El aleph','Borges','Planeta');
insert into libros (titulo,autor,editorial) values ('Martin Fierro','Jose Hernandez','Emece');
insert into libros (titulo,autor,editorial) values ('Aprenda PHP','Mario Molina','Emece');

```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```
#### 6. Muestre todos los registros (select) de "libros".

```sql
select * from libros;

```
Salida del Script

```sh
El aleph	Borges	Planeta
Martin Fierro	Jose Hernandez	Emece
Aprenda PHP	Mario Molina	Emece

```
# 3. Tipos de datos
## Practica 03

```sql
drop table libros;

create table libros(
    titulo varchar2(20),
    autor varchar2(15),
    editorial varchar2(10),
    precio number(6,2),
    cantidad number(3,0)
);

describe libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',25.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Siglo XXI',18.8,200);

select * from libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Atlantida',10,200);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais','Lewis Carroll','Atlantida',10,200);

select * from libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10,2000);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10.123,200);

select * from libros;

```
Salida del Script

```sh
Table LIBROS borrado.


Table LIBROS creado.

Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(15) 
EDITORIAL        VARCHAR2(10) 
PRECIO           NUMBER(6,2)  
CANTIDAD         NUMBER(3)    

1 fila insertadas.


1 fila insertadas.

>>Query Run In:Resultado de la Consulta

Error que empieza en la línea: 18 del comando -
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Atlantida',10,200)
Error en la línea de comandos : 18 Columna : 69
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."LIBROS"."TITULO" (real: 35, máximo: 20)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

1 fila insertadas.

>>Query Run In:Resultado de la Consulta 1

Error que empieza en la línea: 24 del comando -
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10,2000)
Error en la línea de comandos : 24 Columna : 114
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.

1 fila insertadas.

>>Query Run In:Resultado de la Consulta 2

El aleph	Borges	Emece	25,5	100
Matematica estas ahi	Paenza	Siglo XXI	18,8	200
Alicia en el pais	Lewis Carroll	Atlantida	10	200

El aleph	Borges	Emece	25,5	100
Matematica estas ahi	Paenza	Siglo XXI	18,8	200
Alicia en el pais	Lewis Carroll	Atlantida	10	200
El gato con botas	Anonimo	Atlantida	10,12	200

El aleph	Borges	Emece	25,5	100
Matematica estas ahi	Paenza	Siglo XXI	18,8	200
Alicia en el pais	Lewis Carroll	Atlantida	10	200
El gato con botas	Anonimo	Atlantida	10,12	200
```
# EJERCICIOS
## Ejercicio 01
### Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

```sh
 -nombre, cadena de caracteres de 20 de longitud,
 -actor, cadena de caracteres de 20 de longitud,
 -duración, valor numérico entero que no supera los 3 dígitos.
 -cantidad de copias: valor entero de un sólo dígito (no tienen más de 9 copias de cada película).

```
#### 1. Elimine la tabla "peliculas" si ya existe.

```sql
drop table peliculas;

```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 1 del comando :
drop table peliculas
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo.

```sql
CREATE TABLE peliculas (
    nombre VARCHAR2(20),
    actor VARCHAR2(20),
    duracion NUMBER(3),
    cantidad NUMBER(1)
);


```
Salida del Script

```sh
Table PELICULAS creado.

```
#### 3. Vea la estructura de la tabla.

```sql
DESCRIBE peliculas;

```
Salida del Script

```sh
Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
NOMBRE          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(3)    
CANTIDAD        NUMBER(1)    

```
#### 4. Ingrese los siguientes registros: 

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);


```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
#### 5. Muestre todos los registros (4 registros).

```sql
select * from peliculas;

```
Salida del Script

```sh
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	China Zorrilla	110	2

```
#### 6. Intente ingresar una película con valor de cantidad fuera del rango permitido:

```sql
 insert into peliculas (nombre, actor, duracion, cantidad)values ('Mujer bonita','Richard Gere',1200,10);

```
Salida del Script

Nos da un error de ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
```sh
Error que empieza en la línea: 19 del comando -
 insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10)
Error en la línea de comandos : 20 Columna : 41
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.

```
#### 7. Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);

```
Salida del Script

```sh
1 fila insertadas.

```
#### 8. Muestre todos los registros para ver cómo se almacenó el último registro ingresado.

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);

```
Salida del Script

```sh
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	China Zorrilla	110	2
Mujer bonita	Richard Gere	120	4

```
#### 9. Intente ingresar un nombre de película que supere los 20 caracteres.

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Rapidos y furiosos XXXXXXXXXXXXXXXXXXXXXXXXXXX','Jason Momoa',230,6);

```
Salida del Script

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "DANIEL"."PELICULAS"."NOMBRE" (real: 46, máximo: 20) 

```sh
Error que empieza en la línea: 24 del comando -
insert into peliculas (nombre, actor, duracion, cantidad) values ('Rapidos y furiosos XXXXXXXXXXXXXXXXXXXXXXXXXXX','Jason Momoa',230,6)
Error en la línea de comandos : 24 Columna : 67
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."PELICULAS"."NOMBRE" (real: 46, máximo: 20)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

```
## Ejercicio 02
### Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico. 
#### 1. Elimine la tabla si existe.

```sql
DROP TABLE empleados;
```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 1 del comando :
DROP TABLE empleados
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```
#### 2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo: 


```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);

```
Salida del Script


```sh
Table EMPLEADOS creado.

```
#### 3. Verifique que la tabla existe consultando
```sql
select *from all_tables;

```
Salida del Script

```sh
ZAPANA	EMPLEADOS	SENATI			VALID	10		1	255								YES	N									         1	         1	    N	ENABLED			NO		N	N	NO	DEFAULT	DEFAULT	DEFAULT	DISABLED	NO	NO		DISABLED	YES		DISABLED	DISABLED		NO	NO	NO	DEFAULT	NO			NO	NO	DISABLED					USING_NLS_COMP	N	N	N	N	NO	NO		NO	NO	NO	NO			NO	DISABLED	DISABLED	NO	NO	NO	ENABLED
```
#### 4. Vea la estructura de la tabla (5 campos).

```sql
describe empleados;
```
Salida del Script

```sh
Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        NUMBER(6,2)  

```
#### 5. Ingrese algunos registros:

```sql 
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);
```
Salida de Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
#### 6. Seleccione todos los registros (3 registros).

```sql 
select * from empleados;

```
Salida de Script 

```sh
Juan Perez	22333444	m	Sarmiento 123	500
Ana Acosta	24555666	f	Colon 134	650
Bartolome Barrios	27888999	m	Urquiza 479	800

```
#### 7. Intente ingresar un registro con el valor "masculino" en el campo "sexo". Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.

```sql 
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','masculino','Urquiza 479',800);

```
Salida de Script 

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "ZAPANA"."EMPLEADOS"."SEXO" (real: 9, máximo: 1)

```sh
Error que empieza en la línea: 21 del comando -
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','masculino','Urquiza 479',800)
Error en la línea de comandos : 21 Columna : 105
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "ZAPANA"."EMPLEADOS"."SEXO" (real: 9, máximo: 1)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).


```
#### 8. Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.

```sql 
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','m','Urquiza 479',200000000000000000);

```
Salida de Script 

Nos da un error de ORA-01438: valor mayor que el que permite la precisión especificada para esta columna.

```sh
Error que empieza en la línea: 21 del comando -
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolito','27888999','m','Urquiza 479',200000000000000000)
Error en la línea de comandos : 21 Columna : 123
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.

```
#### 9. Elimine la tabla.

```sql 
DROP TABLE empleados;

```
Salida de Script 

```sh
Table EMPLEADOS borrado.

```

