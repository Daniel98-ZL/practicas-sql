# ENUNCIADO DEL EJERCICIO 1
A partir del siguiente enunciado se desea realiza el modelo entidad-relación.
“Una empresa vende productos a varios clientes. Se necesita conocer los datos
personales de los clientes (nombre, apellidos, dni, dirección y fecha de
nacimiento). Cada producto tiene un nombre y un código, así como un precio
unitario. Un cliente puede comprar varios productos a la empresa, y un mismo
producto puede ser comprado por varios clientes. Los productos son
suministrados por diferentes proveedores. Se debe tener en cuenta que un
producto sólo puede ser suministrado por un proveedor, y que un proveedor
puede suministrar diferentes productos. De cada proveedor se desea conocer el
RUC, nombre y dirección”.

```sql
--Creación de la tabla "Cliente":
CREATE TABLE Cliente (
  dni VARCHAR(20) PRIMARY KEY,
  nombre VARCHAR(50),
  apellidos VARCHAR(50),
  direccion VARCHAR(100),
  fecha_nacimiento DATE
);
--Creación de la tabla "Producto":
CREATE TABLE Producto (
  codigo VARCHAR(20) PRIMARY KEY,
  nombre VARCHAR(100),
  precio_unitario DECIMAL(10,2)
);
--Creación de la tabla "Proveedor":
CREATE TABLE Proveedor (
  RUC VARCHAR(20) PRIMARY KEY,
  nombre VARCHAR(100),
  direccion VARCHAR(100)
);
--Creación de la tabla "Venta":
CREATE TABLE Venta (
  dni_cliente VARCHAR(20),
  codigo_producto VARCHAR(20),
  PRIMARY KEY (dni_cliente, codigo_producto),
  FOREIGN KEY (dni_cliente) REFERENCES Cliente(dni),
  FOREIGN KEY (codigo_producto) REFERENCES Producto(codigo)
);
--Creación de la tabla "Suministro":
CREATE TABLE Suministro (
  codigo_producto VARCHAR(20),
  RUC_proveedor VARCHAR(20),
  PRIMARY KEY (codigo_producto, RUC_proveedor),
  FOREIGN KEY (codigo_producto) REFERENCES Producto(codigo),
  FOREIGN KEY (RUC_proveedor) REFERENCES Proveedor(RUC)
);


--Ingresar registros
INSERT INTO Cliente (dni, nombre, apellidos, direccion, fecha_nacimiento)
VALUES
  ('11111111', 'Juan', 'Perez', 'Calle A, Ciudad', TO_DATE('1990-01-01', 'YYYY-MM-DD'));

INSERT INTO Cliente (dni, nombre, apellidos, direccion, fecha_nacimiento)
VALUES
  ('22222222', 'Maria', 'Lopez', 'Calle B, Ciudad', TO_DATE('1995-05-15', 'YYYY-MM-DD'));

INSERT INTO Cliente (dni, nombre, apellidos, direccion, fecha_nacimiento)
VALUES
  ('33333333', 'Pedro', 'Gomez', 'Calle C, Ciudad', TO_DATE('1988-12-10', 'YYYY-MM-DD'));

INSERT INTO Cliente (dni, nombre, apellidos, direccion, fecha_nacimiento)
VALUES
  ('44444444', 'Laura', 'Garcia', 'Calle D, Ciudad', TO_DATE('1992-07-20', 'YYYY-MM-DD'));

INSERT INTO Cliente (dni, nombre, apellidos, direccion, fecha_nacimiento)
VALUES
  ('55555555', 'Carlos', 'Rodriguez', 'Calle E, Ciudad', TO_DATE('1998-03-05', 'YYYY-MM-DD'));


INSERT INTO Producto (codigo, nombre, precio_unitario)
VALUES
  ('P1', 'Producto A', 10.99);

INSERT INTO Producto (codigo, nombre, precio_unitario)
VALUES
  ('P2', 'Producto B', 15.99);

INSERT INTO Producto (codigo, nombre, precio_unitario)
VALUES
  ('P3', 'Producto C', 5.99);

INSERT INTO Producto (codigo, nombre, precio_unitario)
VALUES
  ('P4', 'Producto D', 12.99);

INSERT INTO Producto (codigo, nombre, precio_unitario)
VALUES
  ('P5', 'Producto E', 8.99);


INSERT INTO Proveedor (RUC, nombre, direccion)
VALUES
  ('11111111', 'Proveedor 1', 'Calle X, Ciudad');

INSERT INTO Proveedor (RUC, nombre, direccion)
VALUES
  ('22222222', 'Proveedor 2', 'Calle Y, Ciudad');

INSERT INTO Proveedor (RUC, nombre, direccion)
VALUES
  ('33333333', 'Proveedor 3', 'Calle Z, Ciudad');

INSERT INTO Proveedor (RUC, nombre, direccion)
VALUES
  ('44444444', 'Proveedor 4', 'Calle W, Ciudad');

INSERT INTO Proveedor (RUC, nombre, direccion)
VALUES
  ('55555555', 'Proveedor 5', 'Calle V, Ciudad');


INSERT INTO Venta (dni_cliente, codigo_producto)
VALUES
  ('11111111', 'P1');

INSERT INTO Venta (dni_cliente, codigo_producto)
VALUES
  ('22222222', 'P2');

INSERT INTO Venta (dni_cliente, codigo_producto)
VALUES
  ('33333333', 'P3');

INSERT INTO Venta (dni_cliente, codigo_producto)
VALUES
  ('44444444', 'P1');

INSERT INTO Venta (dni_cliente, codigo_producto)
VALUES
  ('55555555', 'P4');
  
INSERT INTO Suministro (codigo_producto, RUC_proveedor)
VALUES
  ('P1', '11111111');

INSERT INTO Suministro (codigo_producto, RUC_proveedor)
VALUES
  ('P2', '22222222');

INSERT INTO Suministro (codigo_producto, RUC_proveedor)
VALUES
  ('P3', '33333333');

INSERT INTO Suministro (codigo_producto, RUC_proveedor)
VALUES
  ('P4', '11111111');

INSERT INTO Suministro (codigo_producto, RUC_proveedor)
VALUES
  ('P5', '44444444');

--vemos los registros
SELECT * FROM Cliente;
SELECT * FROM Producto;
SELECT * FROM Proveedor;
SELECT * FROM Venta;
SELECT * FROM Suministro;
```

```sh

Table CLIENTE creado.


Table PRODUCTO creado.


Table PROVEEDOR creado.


Table VENTA creado.


Table SUMINISTRO creado.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```