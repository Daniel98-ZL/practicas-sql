# ENUNCIADO DEL EJERCICIO 5
A partir del siguiente supuesto diseñar el modelo entidad-relación: “La clínica
“SAN PATRÁS” necesita llevar un control informatizado de su gestión de
pacientes y médicos. De cada paciente se desea guardar el código, nombre,
apellidos, dirección, población, provincia, código postal, teléfono y fecha de
nacimiento. De cada médico se desea guardar el código, nombre, apellidos,
teléfono y especialidad. Se desea llevar el control de cada uno de los ingresos
que el paciente hace en el hospital. Cada ingreso que realiza el paciente queda
registrado en la base de datos. De cada ingreso se guarda el código de ingreso
(que se incrementará automáticamente cada vez que el paciente realice un
ingreso), el número de habitación y cama en la que el paciente realiza el ingreso
y la fecha de ingreso. Un médico puede atender varios ingresos, pero el ingreso
de un paciente solo puede ser atendido por un único médico. Un paciente puede
realizar varios ingresos en el hospital”.

```sql
-- Crear tabla Paciente
CREATE TABLE Paciente (
    Codigo INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Apellidos VARCHAR(100),
    Direccion VARCHAR(200),
    Poblacion VARCHAR(50),
    Provincia VARCHAR(50),
    CodigoPostal VARCHAR(10),
    Telefono VARCHAR(15),
    FechaNacimiento DATE
);

-- Crear tabla Medico
CREATE TABLE Medico (
    Codigo INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Apellidos VARCHAR(100),
    Telefono VARCHAR(15),
    Especialidad VARCHAR(100)
);

-- Crear tabla Ingreso
CREATE TABLE Ingreso (
    CodigoIngreso INT PRIMARY KEY,
    CodigoPaciente INT,
    CodigoMedico INT,
    Habitacion INT,
    Cama INT,
    FechaIngreso DATE,
    FOREIGN KEY (CodigoPaciente) REFERENCES Paciente(Codigo),
    FOREIGN KEY (CodigoMedico) REFERENCES Medico(Codigo)
);

```

```sh

Table PACIENTE creado.


Table MEDICO creado.


Table INGRESO creado.


```