# ENUNCIADO DEL EJERCICIO 4
A partir del siguiente supuesto diseñar el modelo entidad-relación: “Se desea
diseñar una base de datos para almacenar y gestionar la información empleada
por una empresa dedicada a la venta de automóviles, teniendo en cuenta los
siguientes aspectos: La empresa dispone de una serie de coches para su venta.
Se necesita conocer la matrícula, marca y modelo, el color y el precio de venta
de cada coche. Los datos que interesa conocer de cada cliente son el RUC,
nombre, dirección, ciudad y número de teléfono: además, los clientes se
diferencian por un código interno de la empresa que se incrementa
automáticamente cuando un cliente se da de alta en ella. Un cliente puede
comprar tantos coches como desee a la empresa. Un coche determinado solo
puede ser comprado por un único cliente. El concesionario también se encarga
de llevar a cabo las revisiones que se realizan a cada coche. Cada revisión tiene
asociado un código que se incrementa automáticamente por cada revisión que
se haga. De cada revisión se desea saber si se ha hecho cambio de filtro, si se
ha hecho cambio de aceite, si se ha hecho cambio de frenos u otros. Los coches
pueden pasar varias revisiones en el concesionario”.

```sql
-- Crear secuencia para generar el CodigoInterno en la tabla Cliente
CREATE SEQUENCE secuencia_cliente START WITH 1 INCREMENT BY 1;

-- Crear tabla Coche
CREATE TABLE Coche (
    Matricula VARCHAR2(10) PRIMARY KEY,
    Marca VARCHAR2(50),
    Modelo VARCHAR2(50),
    Color VARCHAR2(20),
    PrecioVenta NUMBER(10, 2)
);

-- Crear tabla Cliente
CREATE TABLE Cliente (
    CodigoInterno INT,
    RUC VARCHAR2(20),
    Nombre VARCHAR2(100),
    Direccion VARCHAR2(200),
    Ciudad VARCHAR2(50),
    NumeroTelefono VARCHAR2(15),
    PRIMARY KEY (CodigoInterno)
);

-- Crear disparador (trigger) para generar el valor de CodigoInterno automáticamente
CREATE OR REPLACE TRIGGER trg_generar_codigo_interno
BEFORE INSERT ON Cliente
FOR EACH ROW
BEGIN
    :NEW.CodigoInterno := secuencia_cliente.NEXTVAL;
END;
/

-- Crear tabla Revision
CREATE TABLE Revision (
    CodigoRevision INT,
    MatriculaCoche VARCHAR2(10),
    CodigoInternoCliente INT,
    CambioFiltro NUMBER(1),
    CambioAceite NUMBER(1),
    CambioFrenos NUMBER(1),
    OtrosDetalles CLOB,
    PRIMARY KEY (CodigoRevision),
    FOREIGN KEY (MatriculaCoche) REFERENCES Coche(Matricula),
    FOREIGN KEY (CodigoInternoCliente) REFERENCES Cliente(CodigoInterno)
);


```

```sh

Sequence SECUENCIA_CLIENTE creado.


Table COCHE creado.


Table CLIENTE creado.


Trigger TRG_GENERAR_CODIGO_INTERNO compilado


Table REVISION creado.


```