# ENUNCIADO DEL EJERCICIO 2
A partir del siguiente enunciado se desea realizar el modelo entidad-relación. “Se
desea informatizar la gestión de una empresa de transportes que reparte
paquetes por toda España. Los encargados de llevar los paquetes son los
camioneros, de los que se quiere guardar el dni, nombre, teléfono, dirección,
salario y población en la que vive. De los paquetes transportados interesa
conocer el código de paquete, descripción, destinatario y dirección del
destinatario. Un camionero distribuye muchos paquetes, y un paquete sólo
puede ser distribuido por un camionero. De las provincias a las que llegan los
paquetes interesa guardar el código de provincia y el nombre. Un paquete sólo
puede llegar a una provincia. Sin embargo, a una provincia pueden llegar varios
paquetes. De los camiones que llevan los camioneros, interesa conocer la
matrícula, modelo, tipo y potencia. Un camionero puede conducir diferentes
camiones en fechas diferentes, y un camión puede ser conducido por varios
camioneros”.

```sql
--Creación de la tabla "Camionero":

CREATE TABLE Camionero (
  dni VARCHAR(9) PRIMARY KEY,
  nombre VARCHAR(50),
  telefono VARCHAR(15),
  direccion VARCHAR(100),
  salario DECIMAL(10, 2),
  poblacion VARCHAR(50)
);
--Creación de la tabla "Paquete":

CREATE TABLE Paquete (
  codigo_paquete VARCHAR(10) PRIMARY KEY,
  descripcion VARCHAR(100),
  destinatario VARCHAR(50),
  direccion_destinatario VARCHAR(100)
);
--Creación de la tabla "Provincia":

CREATE TABLE Provincia (
  codigo_provincia VARCHAR(2) PRIMARY KEY,
  nombre VARCHAR(50)
);
-- Creación de la tabla "Camion":

CREATE TABLE Camion (
  matricula VARCHAR(10) PRIMARY KEY,
  modelo VARCHAR(50),
  tipo VARCHAR(50),
  potencia INTEGER
);
--Creación de la tabla "Distribucion" para la relación entre Camionero y Paquete:

CREATE TABLE Distribucion (
  dni_camionero VARCHAR(9),
  codigo_paquete VARCHAR(10),
  PRIMARY KEY (dni_camionero, codigo_paquete),
  FOREIGN KEY (dni_camionero) REFERENCES Camionero (dni),
  FOREIGN KEY (codigo_paquete) REFERENCES Paquete (codigo_paquete)
);
--Creación de la tabla "Llegada" para la relación entre Paquete y Provincia:

CREATE TABLE Llegada (
  codigo_paquete VARCHAR(10),
  codigo_provincia VARCHAR(2),
  PRIMARY KEY (codigo_paquete),
  FOREIGN KEY (codigo_paquete) REFERENCES Paquete (codigo_paquete),
  FOREIGN KEY (codigo_provincia) REFERENCES Provincia (codigo_provincia)
);
--Creación de la tabla "Conduccion" para la relación entre Camionero y Camion:

CREATE TABLE Conduccion (
  dni_camionero VARCHAR(9),
  matricula VARCHAR(10),
  fecha DATE,
  PRIMARY KEY (dni_camionero, matricula, fecha),
  FOREIGN KEY (dni_camionero) REFERENCES Camionero (dni),
  FOREIGN KEY (matricula) REFERENCES Camion (matricula)
);

--Ingresar Registros
INSERT INTO Camionero (dni, nombre, telefono, direccion, salario, poblacion)
VALUES
  ('111111111', 'Juan Perez', '123456789', 'Calle A, Ciudad', 2000.00, 'Poblacion A');

INSERT INTO Camionero (dni, nombre, telefono, direccion, salario, poblacion)
VALUES
  ('222222222', 'Maria Lopez', '987654321', 'Calle B, Ciudad', 1800.00, 'Poblacion B');

INSERT INTO Camionero (dni, nombre, telefono, direccion, salario, poblacion)
VALUES
  ('333333333', 'Pedro Gomez', '456789123', 'Calle C, Ciudad', 2200.00, 'Poblacion C');

INSERT INTO Camionero (dni, nombre, telefono, direccion, salario, poblacion)
VALUES
  ('444444444', 'Laura Garcia', '321654987', 'Calle D, Ciudad', 1900.00, 'Poblacion D');

INSERT INTO Camionero (dni, nombre, telefono, direccion, salario, poblacion)
VALUES
  ('555555555', 'Carlos Rodriguez', '789123456', 'Calle E, Ciudad', 2100.00, 'Poblacion E');



INSERT INTO Paquete (codigo_paquete, descripcion, destinatario, direccion_destinatario)
VALUES
  ('P1', 'Paquete 1', 'Destinatario A', 'Calle A, Ciudad');

INSERT INTO Paquete (codigo_paquete, descripcion, destinatario, direccion_destinatario)
VALUES
  ('P2', 'Paquete 2', 'Destinatario B', 'Calle B, Ciudad');

INSERT INTO Paquete (codigo_paquete, descripcion, destinatario, direccion_destinatario)
VALUES
  ('P3', 'Paquete 3', 'Destinatario C', 'Calle C, Ciudad');

INSERT INTO Paquete (codigo_paquete, descripcion, destinatario, direccion_destinatario)
VALUES
  ('P4', 'Paquete 4', 'Destinatario D', 'Calle D, Ciudad');

INSERT INTO Paquete (codigo_paquete, descripcion, destinatario, direccion_destinatario)
VALUES
  ('P5', 'Paquete 5', 'Destinatario E', 'Calle E, Ciudad');


INSERT INTO Provincia (codigo_provincia, nombre)
VALUES
  ('P1', 'Provincia 1');

INSERT INTO Provincia (codigo_provincia, nombre)
VALUES
  ('P2', 'Provincia 2');

INSERT INTO Provincia (codigo_provincia, nombre)
VALUES
  ('P3', 'Provincia 3');

INSERT INTO Provincia (codigo_provincia, nombre)
VALUES
  ('P4', 'Provincia 4');

INSERT INTO Provincia (codigo_provincia, nombre)
VALUES
  ('P5', 'Provincia 5');


INSERT INTO Camion (matricula, modelo, tipo, potencia)
VALUES
  ('M1', 'Modelo 1', 'Tipo 1', 200);
  
INSERT INTO Camion (matricula, modelo, tipo, potencia)
VALUES
  ('M2', 'Modelo 2', 'Tipo 2', 250);
  
INSERT INTO Camion (matricula, modelo, tipo, potencia)
VALUES
  ('M3', 'Modelo 3', 'Tipo 3', 300);
  
INSERT INTO Camion (matricula, modelo, tipo, potencia)
VALUES
  ('M4', 'Modelo 4', 'Tipo 1', 180);
  
INSERT INTO Camion (matricula, modelo, tipo, potencia)
VALUES
  ('M5', 'Modelo 5', 'Tipo 2', 220);

INSERT INTO Distribucion (dni_camionero, codigo_paquete)
VALUES
  ('111111111', 'P1');
  
INSERT INTO Distribucion (dni_camionero, codigo_paquete)
VALUES
  ('222222222', 'P2');
  
INSERT INTO Distribucion (dni_camionero, codigo_paquete)
VALUES
  ('333333333', 'P3');
  
INSERT INTO Distribucion (dni_camionero, codigo_paquete)
VALUES
  ('444444444', 'P4');
  
INSERT INTO Distribucion (dni_camionero, codigo_paquete)
VALUES
  ('555555555', 'P5');
  
INSERT INTO Llegada (codigo_paquete, codigo_provincia)
VALUES
  ('P1', 'P1');
  
INSERT INTO Llegada (codigo_paquete, codigo_provincia)
VALUES
  ('P2', 'P2');
  
INSERT INTO Llegada (codigo_paquete, codigo_provincia)
VALUES
  ('P3', 'P3');
  
INSERT INTO Llegada (codigo_paquete, codigo_provincia)
VALUES
  ('P4', 'P4');
  
INSERT INTO Llegada (codigo_paquete, codigo_provincia)
VALUES
  ('P5', 'P5');

INSERT INTO Conduccion (dni_camionero, matricula, fecha)
VALUES
  ('111111111', 'M1', TO_DATE('2023-06-01', 'YYYY-MM-DD'));
  
INSERT INTO Conduccion (dni_camionero, matricula, fecha)
VALUES
  ('222222222', 'M2', TO_DATE('2023-06-02', 'YYYY-MM-DD'));
  
INSERT INTO Conduccion (dni_camionero, matricula, fecha)
VALUES
  ('333333333', 'M3', TO_DATE('2023-06-03', 'YYYY-MM-DD'));
  
INSERT INTO Conduccion (dni_camionero, matricula, fecha)
VALUES
  ('444444444', 'M4', TO_DATE('2023-06-04', 'YYYY-MM-DD'));
  
INSERT INTO Conduccion (dni_camionero, matricula, fecha)
VALUES
  ('555555555', 'M5', TO_DATE('2023-06-05', 'YYYY-MM-DD'));
```

```sh

Table CAMIONERO creado.


Table PAQUETE creado.


Table PROVINCIA creado.


Table CAMION creado.


Table DISTRIBUCION creado.


Table LLEGADA creado.


Table CONDUCCION creado.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```