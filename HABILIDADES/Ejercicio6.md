# ENUNCIADO DEL EJERCICIO 6
Se desea informatizar la gestión de una tienda informática. La tienda dispone de
una serie de productos que se pueden vender a los clientes. “De cada producto
informático se desea guardar el código, descripción, precio y número de
existencias. De cada cliente se desea guardar el código, nombre, apellidos,
dirección y número de teléfono. Un cliente puede comprar varios productos en la
tienda y un mismo producto puede ser comprado por varios clientes. Cada vez
que se compre un artículo quedará registrada la compra en la base de datos
junto con la fecha en la que se ha comprado el artículo. La tienda tiene contactos
con varios proveedores que son los que suministran los productos. Un mismo
producto puede ser suministrado por varios proveedores. De cada proveedor se
desea guardar el código, nombre, apellidos, dirección, provincia y número de
teléfono”.

```sql
-- Crear tabla Producto
CREATE TABLE Producto (
    Codigo INT PRIMARY KEY,
    Descripcion VARCHAR(100),
    Precio DECIMAL(10, 2),
    Existencias INT
);

-- Crear tabla Cliente
CREATE TABLE Cliente (
    Codigo INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Apellidos VARCHAR(100),
    Direccion VARCHAR(200),
    Telefono VARCHAR(15)
);

-- Crear tabla Compra
CREATE TABLE Compra (
    CodigoCompra INT PRIMARY KEY,
    CodigoCliente INT,
    CodigoProducto INT,
    FechaCompra DATE,
    FOREIGN KEY (CodigoCliente) REFERENCES Cliente(Codigo),
    FOREIGN KEY (CodigoProducto) REFERENCES Producto(Codigo)
);

-- Crear tabla Proveedor
CREATE TABLE Proveedor (
    Codigo INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Apellidos VARCHAR(100),
    Direccion VARCHAR(200),
    Provincia VARCHAR(50),
    Telefono VARCHAR(15)
);

-- Crear tabla Producto_Proveedor para la relación muchos a muchos entre Producto y Proveedor
CREATE TABLE Producto_Proveedor (
    CodigoProducto INT,
    CodigoProveedor INT,
    PRIMARY KEY (CodigoProducto, CodigoProveedor),
    FOREIGN KEY (CodigoProducto) REFERENCES Producto(Codigo),
    FOREIGN KEY (CodigoProveedor) REFERENCES Proveedor(Codigo)
);

```

```sh

Table PRODUCTO creado.


Table CLIENTE creado.


Table COMPRA creado.


Table PROVEEDOR creado.


Table PRODUCTO_PROVEEDOR creado.


```
