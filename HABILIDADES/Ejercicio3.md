# ENUNCIADO DEL EJERCICIO 3
A partir del siguiente enunciado diseñar el modelo entidad-relación. “Se desea
diseñar la base de datos de un Instituto. En la base de datos se desea guardar
los datos de los profesores del Instituto (DNI, nombre, dirección y teléfono). Los
profesores imparten módulos, y cada módulo tiene un código y un nombre. Cada
alumno está matriculado en uno o varios módulos. De cada alumno se desea
guardar el nº de expediente, nombre, apellidos y fecha de nacimiento. Los
profesores pueden impartir varios módulos, pero un módulo sólo puede ser
impartido por un profesor. Cada curso tiene un grupo de alumnos, uno de los
cuales es el delegado del grupo”.

```sql
--Creación de la tabla "Profesor":

CREATE TABLE Profesor (
  DNI VARCHAR(10) PRIMARY KEY,
  nombre VARCHAR(50),
  direccion VARCHAR(100),
  telefono VARCHAR(20)
);
--Creación de la tabla "Módulo":

CREATE TABLE Módulo (
  código VARCHAR(10) PRIMARY KEY,
  nombre VARCHAR(50)
);
--Creación de la tabla "Alumno":

CREATE TABLE Alumno (
  número_expediente VARCHAR(10) PRIMARY KEY,
  nombre VARCHAR(50),
  apellidos VARCHAR(100),
  fecha_nacimiento DATE
);
--Creación de la tabla "Curso":

CREATE TABLE Curso (
  ID INT PRIMARY KEY,
  nombre VARCHAR(50)
);
--Creación de la tabla "Grupo":

CREATE TABLE Grupo (
  ID INT PRIMARY KEY,
  nombre VARCHAR(50)
);
--Creación de la tabla de relación "Imparte" entre Profesor y Módulo:

CREATE TABLE Imparte (
  DNI_profesor VARCHAR(10),
  código_módulo VARCHAR(10),
  FOREIGN KEY (DNI_profesor) REFERENCES Profesor(DNI),
  FOREIGN KEY (código_módulo) REFERENCES Módulo(código),
  PRIMARY KEY (DNI_profesor, código_módulo)
);
--Creación de la tabla de relación "Matricula" entre Alumno y Módulo:

CREATE TABLE Matricula (
  número_expediente_alumno VARCHAR(10),
  código_módulo VARCHAR(10),
  FOREIGN KEY (número_expediente_alumno) REFERENCES Alumno(número_expediente),
  FOREIGN KEY (código_módulo) REFERENCES Módulo(código),
  PRIMARY KEY (número_expediente_alumno, código_módulo)
);
--Creación de la tabla de relación "Asiste" entre Alumno y Curso:

CREATE TABLE Asiste (
  número_expediente_alumno VARCHAR(10),
  ID_curso INT,
  FOREIGN KEY (número_expediente_alumno) REFERENCES Alumno(número_expediente),
  FOREIGN KEY (ID_curso) REFERENCES Curso(ID),
  PRIMARY KEY (número_expediente_alumno, ID_curso)
);
--Creación de la tabla de relación "Pertenecer" entre Alumno y Grupo:

CREATE TABLE Pertenecer (
  número_expediente_alumno VARCHAR(10),
  ID_grupo INT,
  FOREIGN KEY (número_expediente_alumno) REFERENCES Alumno(número_expediente),
  FOREIGN KEY (ID_grupo) REFERENCES Grupo(ID),
  PRIMARY KEY (número_expediente_alumno, ID_grupo)
);

--ingresar registros

INSERT INTO Profesor (DNI, nombre, direccion, telefono)
VALUES
  ('111111111', 'Juan Pérez', 'Calle Principal 123', '1234567890');
  
INSERT INTO Profesor (DNI, nombre, direccion, telefono)
VALUES
  ('222222222', 'María López', 'Avenida Central 456', '9876543210');
  
INSERT INTO Profesor (DNI, nombre, direccion, telefono)
VALUES
  ('333333333', 'Pedro García', 'Plaza Mayor 789', '4567890123');
  
INSERT INTO Profesor (DNI, nombre, direccion, telefono)
VALUES
  ('444444444', 'Laura Fernández', 'Calle Secundaria 321', '0123456789');
  
INSERT INTO Profesor (DNI, nombre, direccion, telefono)
VALUES
  ('555555555', 'Carlos Rodríguez', 'Avenida Principal 789', '9876543210');


INSERT INTO Módulo (código, nombre)
VALUES
  ('M1', 'Módulo 1');
  
INSERT INTO Módulo (código, nombre)
VALUES
  ('M2', 'Módulo 2');
  
INSERT INTO Módulo (código, nombre)
VALUES
  ('M3', 'Módulo 3');
  
INSERT INTO Módulo (código, nombre)
VALUES
  ('M4', 'Módulo 4');
  
INSERT INTO Módulo (código, nombre)
VALUES
  ('M5', 'Módulo 5');

INSERT INTO Alumno (número_expediente, nombre, apellidos, fecha_nacimiento)
VALUES
  ('A1', 'Ana', 'Gómez', TO_DATE('2000-01-01', 'YYYY-MM-DD'));
  
INSERT INTO Alumno (número_expediente, nombre, apellidos, fecha_nacimiento)
VALUES
  ('A2', 'Mario', 'López', TO_DATE('2001-02-02', 'YYYY-MM-DD'));
  
INSERT INTO Alumno (número_expediente, nombre, apellidos, fecha_nacimiento)
VALUES
  ('A3', 'Sara', 'Martínez', TO_DATE('2002-03-03', 'YYYY-MM-DD'));
  
INSERT INTO Alumno (número_expediente, nombre, apellidos, fecha_nacimiento)
VALUES
  ('A4', 'Luis', 'Hernández', TO_DATE('2003-04-04', 'YYYY-MM-DD'));
  
INSERT INTO Alumno (número_expediente, nombre, apellidos, fecha_nacimiento)
VALUES
  ('A5', 'Elena', 'Fernández', TO_DATE('2004-05-05', 'YYYY-MM-DD'));


INSERT INTO Curso (ID, nombre)
VALUES
  (1, 'Curso 1');
  
INSERT INTO Curso (ID, nombre)
VALUES
  (2, 'Curso 2');
  
INSERT INTO Curso (ID, nombre)
VALUES
  (3, 'Curso 3');
  
INSERT INTO Curso (ID, nombre)
VALUES
  (4, 'Curso 4');
  
INSERT INTO Curso (ID, nombre)
VALUES
  (5, 'Curso 5');


INSERT INTO Grupo (ID, nombre)
VALUES
  (1, 'Grupo 1');
  
INSERT INTO Grupo (ID, nombre)
VALUES
  (2, 'Grupo 2');
  
INSERT INTO Grupo (ID, nombre)
VALUES
  (3, 'Grupo 3');
  
INSERT INTO Grupo (ID, nombre)
VALUES
  (4, 'Grupo 4');
  
INSERT INTO Grupo (ID, nombre)
VALUES
  (5, 'Grupo 5');


INSERT INTO Imparte (DNI_profesor, código_módulo)
VALUES
  ('111111111', 'M1');
  
INSERT INTO Imparte (DNI_profesor, código_módulo)
VALUES
  ('222222222', 'M2');
  
INSERT INTO Imparte (DNI_profesor, código_módulo)
VALUES
  ('333333333', 'M3');
  
INSERT INTO Imparte (DNI_profesor, código_módulo)
VALUES
  ('444444444', 'M4');
  
INSERT INTO Imparte (DNI_profesor, código_módulo)
VALUES
  ('555555555', 'M5');


INSERT INTO Matricula (número_expediente_alumno, código_módulo)
VALUES
  ('A1', 'M1');
  
INSERT INTO Matricula (número_expediente_alumno, código_módulo)
VALUES
  ('A2', 'M2');
  
INSERT INTO Matricula (número_expediente_alumno, código_módulo)
VALUES
  ('A3', 'M3');
  
INSERT INTO Matricula (número_expediente_alumno, código_módulo)
VALUES
  ('A4', 'M4');
  
INSERT INTO Matricula (número_expediente_alumno, código_módulo)
VALUES
  ('A5', 'M5');

INSERT INTO Asiste (número_expediente_alumno, ID_curso)
VALUES
  ('A1', 1);
  
INSERT INTO Asiste (número_expediente_alumno, ID_curso)
VALUES
  ('A2', 2);
  
INSERT INTO Asiste (número_expediente_alumno, ID_curso)
VALUES
  ('A3', 3);
  
INSERT INTO Asiste (número_expediente_alumno, ID_curso)
VALUES
  ('A4', 4);
  
INSERT INTO Asiste (número_expediente_alumno, ID_curso)
VALUES
  ('A5', 5);


INSERT INTO Pertenecer (número_expediente_alumno, ID_grupo)
VALUES
  ('A1', 1);
  
INSERT INTO Pertenecer (número_expediente_alumno, ID_grupo)
VALUES
  ('A2', 2);
  
INSERT INTO Pertenecer (número_expediente_alumno, ID_grupo)
VALUES
  ('A3', 3);
  
INSERT INTO Pertenecer (número_expediente_alumno, ID_grupo)
VALUES
  ('A4', 4);
  
INSERT INTO Pertenecer (número_expediente_alumno, ID_grupo)
VALUES
  ('A5', 5);


```
```sh

Table PROFESOR creado.


Table MÓDULO creado.


Table ALUMNO creado.


Table CURSO creado.


Table GRUPO creado.


Table IMPARTE creado.


Table MATRICULA creado.


Table ASISTE creado.


Table PERTENECER creado.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


```