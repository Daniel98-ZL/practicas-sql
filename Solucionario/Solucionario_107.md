# 107. Disparador de actualizacion a nivel de sentencia (update trigger)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica un registro en la tabla "libros".

Eliminamos la tabla "libros" y la tabla "control":

```sql
drop table libros;
drop table control;
```

Creamos la tabla con la siguiente estructura:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);
```

Creamos la tabla "control":

```sql
create table control(
    usuario varchar2(30),
    fecha date
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Creamos un disparador a nivel de sentencia, que se dispare cada vez que se actualice un registro en "libros"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "libros":

```sql
create or replace trigger tr_actualizar_libros
    before update
    on libros
begin
    insert into control values(user,sysdate);
end tr_actualizar_libros;
/
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_LIBROS';
```

obtenemos la siguiente información:

- trigger_name: nombre del disparador;

- trigger_type: momento y nivel, en este caso es un desencadenador "before" y a nivel de sentencia (statement);

- triggering_event: evento que lo dispara, en este caso, "update";

- base_object_type: a qué objeto está asociado, puede ser una tabla o una vista, en este caso, una tabla (table);

- table_name: nombre de la tabla al que está asociado (libros);

- y otras columnas que no analizaremos por el momento.

Actualizamos un registro en "libros":

```sql
update libros set codigo=99 where codigo=100;
```

Veamos si el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

Actualizamos varios registros de "libros":

```sql
update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';
```

Veamos si el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

Note que se modificaron 2 registros de "libros", pero como la modificación se realizó con una sola sentencia "update" y el trigger es a nivel de sentencia, se agregó solamente una fila a la tabla "control"; si el trigger hubiese sido creado a nivel de fila, la sentencia anterior, hubiese disparado el trigger 2 veces y habría ingresado en "control" 2 filas.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table control;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_actualizar_libros
    before update
    on libros
begin
    insert into control values(user,sysdate);
end tr_actualizar_libros;
/
 
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_LIBROS';

update libros set codigo=99 where codigo=100;

select *from control;

update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';

select *from control;
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra llamada "control" guarda un registro por cada vez que un empleado actualiza datos sobre la tabla "empleados".

1. Elimine las tablas:

```sql
drop table empleados;
drop table control;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(20),
    nombre varchar2(20),
    seccion varchar2(30),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

3. Ingrese algunos registros en "empleados":

insert into empleados values('22333444','ACOSTA','Ana','Secretaria',500);
insert into empleados values('22777888','DOMINGUEZ','Daniel','Secretaria',560);
insert into empleados values('22999000','FUENTES','Federico','Sistemas',680);
insert into empleados values('22555666','CASEROS','Carlos','Contaduria',900);
insert into empleados values('23444555','GOMEZ','Gabriela','Sistemas',1200);
insert into empleados values('23666777','JUAREZ','Juan','Contaduria',1000);
```

4. Cree un disparador a nivel de sentencia, que se dispare cada vez que se actualicen registros en "empleados"; el trigger debe ingresar en la tabla "control", el nombre del usuario y la fecha en la cual se realizó un "update" sobre "empleados"
```sql
create or replace trigger tr_empleados
after update on empleados
begin
    insert into control(usuario, fecha) values(USER, sysdate);
end;
/

```
5. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
select trigger_name, trigger_type, triggering_event, table_owner, table_name
from user_triggers
where table_name = 'EMPLEADOS';

```
6. Actualice un registro en "empleados":

```sql
update empleados set nombre='Graciela' where documento='23444555';
```

7. Vea si el trigger se disparó consultando la tabla "control"
```sql
select * from control;

```
8. Actualice varios registros de "empleados" en una sola sentencia
```sql
update empleados set seccion = 'Recursos Humanos', sueldo = 800 where seccion = 'Secretaria';

```
9. Vea si el trigger se disparó consultando la tabla "control"
Note que se modificaron 2 registros de "empleados", pero como la modificación se realizó con una sola sentencia "update" y el trigger es a nivel de sentencia, se agregó solamente una fila a la tabla "control"; si el trigger hubiese sido creado a nivel de fila, la sentencia anterior, hubiese disparado el trigger 2 veces y habría ingresado en "control" 2 filas.
```sql
select * from control;

```