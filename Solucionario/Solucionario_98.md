# 98. Control de flujo (case)

## Practica de laboratorio

Trabajamos con la tabla "empleados". Eliminamos la tabla y luego la creamos con la siguiente estructura:

```sql
 drop table empleados;

 create table empleados (
  documento char(8),
  nombre varchar(30),
  fechanacimiento date  
 );
```

Ingresamos algunos registros:

```sql
 insert into empleados values('20111111','Acosta Ana','10/05/1968');
 insert into empleados values('22222222','Bustos Bernardo','09/07/1970');
 insert into empleados values('22333333','Caseros Carlos','15/10/1971');
 insert into empleados values('23444444','Fuentes Fabiana','25/01/1972');
 insert into empleados values('23555555','Gomez Gaston','28/03/1979');
 insert into empleados values('24666666','Juarez Julieta','18/02/1981');
 insert into empleados values('25777777','Lopez Luis','17/09/1978');
 insert into empleados values('26888888','Morales Marta','22/12/1975');
```

Nos interesa el nombre del mes en el cual cada empleado cumple años. Podemos utilizar la estructura condicional "case". Para ello crearemos una función que reciba una fecha y retorne una cadena de caracteres indicando el nombre del mes de la fecha enviada como argumento:

```sql
 create or replace function f_mes(afecha date)
   return varchar2
 is
  mes varchar2(20);
 begin
   mes:='enero';
   case extract(month from afecha)
     when 1 then mes:='enero';
     when 2 then mes:='febrero';
     when 3 then mes:='marzo';
     when 4 then mes:='abril';
     when 5 then mes:='mayo';
     when 6 then mes:='junio';
     when 7 then mes:='julio';
     when 8 then mes:='agosto';
     when 9 then mes:='setiembre';
     when 10 then mes:='octubre';
     when 11 then mes:='noviembre';
     else mes:='diciembre';
   end case;
   return mes;
 end;
 /
```

Recuperamos el nombre del empleado y el mes de su cumpleaños realizando un "select":

```sql
 select nombre, f_mes(fechanacimiento) as cumpleaños from empleados;
```

Podemos probar la función creada anteriormente enviándole la siguiente fecha:

```sql
 select f_mes('10/10/2018') from dual;
```

obtenemos como resultado "octubre".

Realizamos una función que reciba una fecha y retorne si se encuentra en el 1º, 2º, 3º ó 4º trimestre del año:

```sql
create or replace function f_trimestre(afecha date)
   return varchar2
 is
  mes varchar2(20);
  trimestre number;
 begin
   mes:=extract(month from afecha);
   trimestre:=4;
   case mes
     when 1 then trimestre:=1;
     when 2 then trimestre:=1;
     when 3 then trimestre:=1;
     when 4 then trimestre:=2;
     when 5 then trimestre:=2;
     when 6 then trimestre:=2;
     when 7 then trimestre:=3;
     when 8 then trimestre:=3;
     when 9 then trimestre:=3;
     else trimestre:=4;
   end case;
   return trimestre;
 end;
 /
```

Recuperamos el nombre del empleado y el trimestre de su cumpleaños empleando la función creada anteriormente:

```sql
 select nombre, f_trimestre(fechanacimiento) from empleados;
```

Vamos a emplear "case" dentro de un "select". Veamos un ejemplo similar a la función anterior:

```sql
 select nombre,fechanacimiento,
  case extract(month from fechanacimiento)
   when 1 then 1
   when 2 then 1
   when 3 then 1
   when 4 then 2
   when 5 then 2
   when 6 then 2
   when 7 then 3
   when 8 then 3
   when 9 then 3
  else  4
  end as trimestre
  from empleados
  order by trimestre;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
 drop table empleados;

 create table empleados (
  documento char(8),
  nombre varchar(30),
  fechanacimiento date  
 );

 insert into empleados values('20111111','Acosta Ana','10/05/1968');
 insert into empleados values('22222222','Bustos Bernardo','09/07/1970');
 insert into empleados values('22333333','Caseros Carlos','15/10/1971');
 insert into empleados values('23444444','Fuentes Fabiana','25/01/1972');
 insert into empleados values('23555555','Gomez Gaston','28/03/1979');
 insert into empleados values('24666666','Juarez Julieta','18/02/1981');
 insert into empleados values('25777777','Lopez Luis','17/09/1978');
 insert into empleados values('26888888','Morales Marta','22/12/1975');

 create or replace function f_mes(afecha date)
   return varchar2
 is
  mes varchar2(20);
 begin
   mes:='enero';
   case extract(month from afecha)
     when 1 then mes:='enero';
     when 2 then mes:='febrero';
     when 3 then mes:='marzo';
     when 4 then mes:='abril';
     when 5 then mes:='mayo';
     when 6 then mes:='junio';
     when 7 then mes:='julio';
     when 8 then mes:='agosto';
     when 9 then mes:='setiembre';
     when 10 then mes:='octubre';
     when 11 then mes:='noviembre';
     else mes:='diciembre';
   end case;
   return mes;
 end;
 /

 -- Recuperamos el nombre del empleado y el mes de su cumpleaños realizando un "select":
 select nombre, f_mes(fechanacimiento) as cumpleaños from empleados;

 -- Podemos probar la función creada anteriormente enviándole la siguiente fecha:
 select f_mes('10/10/2018') from dual;
 --obtenemos como resultado "octubre".

 -- Realizamos una función que reciba una fecha y retorne si se encuentra
 -- en el 1º, 2º, 3º ó 4º trimestre del año:
create or replace function f_trimestre(afecha date)
   return varchar2
 is
  mes varchar2(20);
  trimestre number;
 begin
   mes:=extract(month from afecha);
   trimestre:=4;
   case mes
     when 1 then trimestre:=1;
     when 2 then trimestre:=1;
     when 3 then trimestre:=1;
     when 4 then trimestre:=2;
     when 5 then trimestre:=2;
     when 6 then trimestre:=2;
     when 7 then trimestre:=3;
     when 8 then trimestre:=3;
     when 9 then trimestre:=3;
     else trimestre:=4;
   end case;
   return trimestre;
 end;
 /

 -- Recuperamos el nombre del empleado y el trimestre de su cumpleaños empleando
 -- la función creada anteriormente:
 select nombre, f_trimestre(fechanacimiento) from empleados;

-- Vamos a emplear "case" dentro de un "select". 
-- Veamos un ejemplo similar a la función anterior:
 select nombre,fechanacimiento,
  case extract(month from fechanacimiento)
   when 1 then 1
   when 2 then 1
   when 3 then 1
   when 4 then 2
   when 5 then 2
   when 6 then 2
   when 7 then 3
   when 8 then 3
   when 9 then 3
  else  4
  end as trimestre
  from empleados
  order by trimestre;
```

## Ejercicios propuestos

Un profesor guarda los promedios de sus alumnos de un curso en una tabla llamada "alumnos".

1. Elimine la tabla.

```sql
  drop table alumnos;
```

2. Cree la tabla:

```sql
 create table alumnos(
  legajo char(5) not null,
  nombre varchar2(30),
  promedio number(4,2)
);
```

3. Ingrese los siguientes registros:

```sql
 insert into alumnos values(3456,'Perez Luis',8.5);
 insert into alumnos values(3556,'Garcia Ana',7.0);
 insert into alumnos values(3656,'Ludueña Juan',9.6);
 insert into alumnos values(2756,'Moreno Gabriela',4.8);
 insert into alumnos values(4856,'Morales Hugo',3.2);
 insert into alumnos values(7856,'Gomez Susana',6.4);
```

4. Si el alumno tiene un promedio menor a 4, muestre un mensaje "reprobado", si el promedio es mayor o igual a 4 y menor a 7, muestre "regular", si el promedio es mayor o igual a 7, muestre "promocionado", usando "case" (recuerde que "case" toma valores puntuales, emplee "trunc")
```sql
SELECT legajo, nombre,
    CASE
        WHEN trunc(promedio) < 4 THEN 'reprobado'
        WHEN trunc(promedio) >= 4 AND trunc(promedio) < 7 THEN 'regular'
        WHEN trunc(promedio) >= 7 THEN 'promocionado'
    END AS condicion
FROM alumnos;

```
5. Elimine la tabla "alumnos"
```sql
DROP TABLE alumnos;

```
6. La nueva tabla contendrá varias notas por alumno. Cree la tabla:

```sql
 create table alumnos(
  legajo char(5) not null,
  nombre varchar2(30),
  nota number(4,2)
);
```

7. Ingrese los siguientes registros:

```sql
 insert into alumnos values(3456,'Perez Luis',8.5);
 insert into alumnos values(3456,'Perez Luis',9.9);
 insert into alumnos values(3456,'Perez Luis',7.8);
 insert into alumnos values(3556,'Garcia Ana',7.0);
 insert into alumnos values(3556,'Garcia Ana',6.0);
 insert into alumnos values(3656,'Ludueña Juan',9.6);
 insert into alumnos values(3656,'Ludueña Juan',10);
 insert into alumnos values(2756,'Moreno Gabriela',4.2);
 insert into alumnos values(2756,'Moreno Gabriela',2.6);
 insert into alumnos values(2756,'Moreno Gabriela',2);
 insert into alumnos values(4856,'Morales Hugo',3.2);
 insert into alumnos values(4856,'Morales Hugo',4.7);
 insert into alumnos values(7856,'Gomez Susana',6.4);
 insert into alumnos values(7856,'Gomez Susana',8.6);
```

8. Si el alumno tiene un promedio menor a 4, muestre un mensaje "reprobado", si el promedio es mayor o igual a 4 y menor a 7 muestre "regular", si el promedio es mayor o igual a 7, muestre "promocionado", usando "case" (recuerde que "case" toma valores puntuales, emplee "trunc"). Para obtener el promedio agrupe por legajo y emplee la función "avg"
```sql
SELECT legajo, nombre,
    CASE
        WHEN trunc(AVG(nota)) < 4 THEN 'reprobado'
        WHEN trunc(AVG(nota)) >= 4 AND trunc(AVG(nota)) < 7 THEN 'regular'
        WHEN trunc(AVG(nota)) >= 7 THEN 'promocionado'
    END AS condicion
FROM alumnos
GROUP BY legajo, nombre;

```
9. Cree una tabla denominada "alumnosCondicion" con los campos "legajo", "notafinal" y "condicion":

```sql
  drop table alumnosCondicion;
  create table alumnosCondicion(
   legajo char(5),
   notafinal number(4,2),
   condicion varchar2(15)
  );
```

10. Cree o reemplace un procedimiento almacenado llamado "pa_CargarCondicion" que guarde en la tabla "alumnosCondicion" el legajo de cada alumno, el promedio de sus notas y la condición (libre, regular o promocionado)
```sql
CREATE OR REPLACE PROCEDURE pa_CargarCondicion AS
BEGIN
    DELETE FROM alumnosCondicion; -- Eliminar registros existentes en la tabla
    
    FOR rec IN (SELECT legajo, AVG(nota) AS promedio
                FROM alumnos
                GROUP BY legajo)
    LOOP
        DECLARE condicion VARCHAR2(15);
        BEGIN
            IF rec.promedio >= 7 THEN
                condicion := 'promocionado';
            ELSIF rec.promedio >= 4 THEN
                condicion := 'regular';
            ELSE
                condicion := 'libre';
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                condicion := 'Indefinida';
        END;

        INSERT INTO alumnosCondicion(legajo, notafinal, condicion)
        VALUES (rec.legajo, rec.promedio, condicion);
    END LOOP;
END;
/

```
11. Ejecute el procedimiento "pa_cargarCondicion" y recupere todos los datos de la tabla "alumnoscondicion"
```sql
CREATE OR REPLACE PROCEDURE pa_CargarCondicion AS
BEGIN
    DELETE FROM alumnosCondicion;
    
    INSERT INTO alumnosCondicion (legajo, notafinal, condicion)
    SELECT legajo, AVG(nota), 
        CASE
            WHEN trunc(AVG(nota)) < 4 THEN 'reprobado'
            WHEN trunc(AVG(nota)) >= 4 AND trunc(AVG(nota)) < 7 THEN 'regular'
            WHEN trunc(AVG(nota)) >= 7 THEN 'promocionado'
        END AS condicion
    FROM alumnos
    GROUP BY legajo;
    
    COMMIT;
END;
/

-- Ejecutar el procedimiento almacenado
EXECUTE pa_CargarCondicion;

-- Recuperar todos los datos de la tabla alumnosCondicion
SELECT * FROM alumnosCondicion;

```