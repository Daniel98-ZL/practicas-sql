# 67. Agregar campos (alter table - add)

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    titulo varchar2(30),
    editorial varchar2(20)
);
```

Agregamos el campo "cantidad" a la tabla "libros", de tipo number(4), con el valor por defecto cero y que NO acepta valores nulos:

```sql
alter table libros
add cantidad number(4) default 0 not null;
```

Verificamos la estructura de la tabla:

```sql
describe libros;
```

Aparece el nuevo campo.

Agregamos un nuevo campo "precio" a la tabla "libros", de tipo number(4) que acepta valores nulos:

```sql
alter table libros
add precio number(4);
```

Verificamos la estructura de la tabla:

```sql
describe libros;
```

aparece el nuevo campo.

Ingresamos algunos registros:

```sql
insert into libros values('El aleph','Emece',100,25.5);
insert into libros values('Uno','Planeta',150,null);
```

Intentamos agregar un nuevo campo "autor" de tipo varchar2(30) que no admita valores nulos:

```sql
alter table libros
add autor varchar2(30) not null;
```

Mensaje de error. Si el campo no aceptará valores nulos y no tiene definido un valor por defecto, no se pueden llenar los registros existentes con ningún valor. Por ello, debemos definirlo con un valor por defecto:

```sql
alter table libros
add autor varchar2(30) default 'Desconocido' not null;
```

Veamos qué sucedió con los registros existentes:

```sql
select *from libros;
```

contienen el valor por defecto en "autor".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
 drop table libros;

 create table libros(
  titulo varchar2(30),
  editorial varchar2(20)
 );

-- Agregamos el campo "cantidad" a la tabla "libros", de tipo number(4),
-- con el valor por defecto cero y que NO acepta valores nulos:
alter table libros
add cantidad number(4) default 0 not null;

-- Verificamos la estructura de la tabla:
describe libros;

-- Agregamos un nuevo campo "precio" a la tabla "libros", de
-- tipo number(4) que acepta valores nulos:
alter table libros
add precio number(4);

-- Verificamos la estructura de la tabla:
describe libros;

-- Ingresamos algunos registros:
insert into libros values('El aleph','Emece',100,25.5);
insert into libros values('Uno','Planeta',150,null);

-- Intentamos agregar un nuevo campo "autor" de tipo varchar2(30) que no admita valores nulos:
alter table libros
add autor varchar2(30) not null;
-- Mensaje de error. Si el campo no aceptará valores nulos y no tiene definido un valor por defecto,
-- no se pueden llenar los registros existentes con ningún valor. 
-- Por ello, debemos definirlo con un valor por defecto:
alter table libros
add autor varchar2(30) default 'Desconocido' not null;

-- Veamos qué sucedió con los registros existentes:
select * from libros;
```

## Ejercicios propuesto

Trabaje con una tabla llamada "empleados".

1. Elimine la tabla y créela:

```sql
drop table empleados;

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30)
);
```

2. Agregue un campo "fechaingreso" de tipo date que acepte valores nulos
```sql
ALTER TABLE empleados ADD fechaingreso DATE;

```
3. Verifique que la estructura de la tabla ha cambiado
```sql
DESCRIBE empleados;

```
4. Agregue un campo "seccion" de tipo caracter que no permita valores nulos y verifique que el nuevo campo existe
```sql
ALTER TABLE empleados ADD seccion CHAR(1) NOT NULL;

```
5. Ingrese algunos registros:

```sql
insert into empleados values('Lopez','Juan','Colon 123','10/10/1980','Contaduria');
insert into empleados values('Gonzalez','Juana','Avellaneda 222','01/05/1990','Sistemas');
insert into empleados values('Perez','Luis','Caseros 987','12/09/2000','Secretaria');
```

6. Intente agregar un campo "sueldo" que no admita valores nulos.
```sql
ALTER TABLE empleados
ADD sueldo NUMBER(10, 2) NOT NULL;

```
7. Agregue el campo "sueldo" no nulo y con el valor 0 por defecto.
```sql
ALTER TABLE empleados ADD sueldo NUMBER(10,2) NOT NULL DEFAULT 0;

```
8. Verifique que la estructura de la tabla ha cambiado.
```sql
DESCRIBE empleados;

```