# 13. Vaciar la tabla (truncate table)
## Ejercicio de laboratorio

### Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.

#### 1. Eliminamos la tabla:

```sql
drop table libros;
```
Salida del Script

```sh
Table LIBROS borrado.
```
#### 2. Creamos la tabla:

```sql
create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```
Salida del Script

```sh
Table LIBROS creado.
```
#### 3. Agregamos algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.
```
#### 4. Seleccionamos todos los registros:

```sql
select *from libros;
```
Salida del Script

```sh
1	El aleph	Borges	Emece	25,6
2	Uno	Richard Bach	Planeta	18
```
#### 5. Truncamos la tabla:

```sql
truncate table libros;
```
Salida del Script

```sh
Table LIBROS truncado.
```
#### 6. Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:

```sql
select *from libros;
```
Salida del Script

```sh
No muestra ningun registro.
```
#### 7. Ingresamos nuevamente algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.
```
#### 8. Eliminemos todos los registros con "delete":

```sql
delete from libros;
```
Salida del Script

```sh
2 filas eliminado
```
#### 9. Si consultamos la tabla, vemos que aún existe pero ya no tiene registros:

```sql
select *from libros;
```
Salida del Script

```sh
No muestra ningun registro
```
#### 10. Ingresamos nuevamente algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.
```
#### 11. Eliminamos la tabla:

```sql
drop table libros;
```
Salida del Script

```sh
Table LIBROS borrado.
```
#### 12. Intentamos seleccionar todos los registros:

```sql
select *from libros;
```
Resultado de la consulta

Nos da un error de ORA-00942: la tabla o vista no existe.
```sh
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
Error en la línea: 32, columna: 14
```
Aparece un mensaje de error, la tabla no existe.

#### 13. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(4),
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

select *from libros;

truncate table libros;

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

delete from libros;

select *from libros;

insert into libros (codigo,titulo,autor,editorial,precio) values (1,'El aleph','Borges','Emece',25.60);
insert into libros (codigo,titulo,autor,editorial,precio) values (2,'Uno','Richard Bach','Planeta',18);

drop table libros;

select *from libros;
```

