# 39. Restriccioncheck

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla:

```sql
drop table libros;
```

La creamos e ingresamos algunos registros:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    preciomin number(5,2),
    preciomay number(5,2)
);

insert into libros values (1,'Uno','Bach','Planeta',22,20);
insert into libros values (2,'El quijote','Cervantes','Emece',15,13);
insert into libros values (3,'Aprenda PHP','Mario Molina','Siglo XXI',53,48);
insert into libros values (4,'Java en 10 minutos','Garcia','Siglo XXI',35,40);
```

Agregamos una restricción "primary key" para el campo "codigo":

```sql
alter table libros
add constraint PK_libros_codigo
primary key (codigo);
```

Agregamos una restricción única, la clave única consta de 3 campos, "titulo", "autor" y "editorial":

```sql
alter table libros
add constraint UQ_libros
unique (titulo,codigo,editorial);
```

Agregamos una restricción "check" para asegurar que los valores de los campos correspondientes a precios no puedan ser negativos:

```sql
alter table libros
add constraint CK_libros_precios_positivo
check (preciomin>=0 and preciomay>=0);
```

Intentamos ingresar un valor inválido para algún campo correspondiente al precio, que vaya en contra de la restricción (por ejemplo el valor "-15"):

```sql
insert into libros values (5,'Matematica estas ahi','Paenza','Siglo XXI',-15,30);
```

Aparecerá un mensaje de error indicando que hay conflicto con la restricción de control creada anteriormente y la inserción no se realiza.

Igualmente si intentamos actualizar un precio, que vaya en contra de la restricción:

```sql
update libros set preciomay=-20 where titulo='Uno';
```

Si intentamos agregar una restricción que no permita que el precio mayorista supere el precio minorista:

```sql
alter table libros
add constraint CK_libros_preciominmay
check (preciomay<=preciomin);
```

Aparece un mensaje de error y la sentencia no se ejecuta, porque hay un registro que no cumple con la restricción que intentamos establecer. Podemos modificar los datos que no cumplen la condición de la restricción:

```sql
update libros set preciomay=30
where titulo='Java en 10 minutos';
```

Ahora Oracle si nos permite agregar la restricción "check" que impida que se ingresen valores para "preciomay" superiores a "preciomin":

```sql
alter table libros
add constraint CK_libros_preciominmay
check (preciomay<=preciomin);
```

Veamos las restricciones de la tabla:

```sql
select *from user_constraints where table_name='LIBROS';
```

Aparece la siguiente tabla (simplificada) resultado:

```sh
OWNER     CONSTRAINT_NAME             CONSTRAINT_TYPE    TABLE_NAME     SEARCH_CONDITION
--------------------------------------------------------------------------------------------------
SYSTEM    PK_LIBROS_CODIGO            P                  LIBROS
SYSTEM    UQ_LIBROS                   U                  LIBROS
SYSTEM    CK_LIBROS_PRECIOS_POSITIVO  C                  LIBROS         preciomin>=0 and preciomay>=0
SYSTEM    CK_LIBROS_PRECIOMINMAY      C                  LIBROS         preciomay<=preciomin
```

Note que en el caso de las restricciones de control, en las cuales muestra "C" en el tipo de constraint, la columna "SEARCH_CONDITION" muestra la regla que debe cumplirse; en caso de ser una restricción "primary key" ounique", esa columna queda vacía.

Intentamos ingresar un registro que infrinja la restricción "CK_libros_preciominmax":

```sql
 inser into libros values (6,'El gato con botas',null,'Planeta',25,30);
```

Mensaje de error.

Consultamos "user_cons_columns":

```sql
select *from user_cons_columns where table_name='LIBROS';
```

Aparece la siguiente información simplificada:

```sh
OWNER     CONSTRAINT_NAME               TABLE_NAME   COLUMN_NAME       POSITION
-----------------------------------------------------------------------------------------
SYSTEM    PK_LIBROS_CODIGO              LIBROS       CODIGO            1
SYSTEM    UQ_LIBROS                     LIBROS       EDITORIAL         3
SYSTEM    UQ_LIBROS                     LIBROS       CODIGO            2
SYSTEM    UQ_LIBROS                     LIBROS       TITULO            1
SYSTEM    CK_LIBROS_PRECIOS_POSITIVO    LIBROS       PRECIOMAY
SYSTEM    CK_LIBROS_PRECIOS_POSITIVO    LIBROS       PRECIOMIN
SYSTEM    CK_LIBROS_PRECIOMINMAY        LIBROS       PRECIOMAY
SYSTEM    CK_LIBROS_PRECIOMINMAY        LIBROS       PRECIOMIN
```

Analizamos la información: la tabla tiene 4 restricciones, 1 "primary key", 1 "unique" y 2 "check". La restricción "primarykey" ocupa una sola fila porque fue definida para 1 solo campo, por ello, en la columna "POSITION" aparece "1". La restricción única ocupa tres filas porque fue definida con 3 campos cuyo orden está indicado en la columna "POSITION". La columna "POSITION" muestra información si la restricción es "primary key" o "unique" indicando el orden de los campos. La restricción de control "CK_libros_precios_positivo" ocupa 2 filas porque en su definición se nombran 2 campos (indicados en "COLUMN_NAME"). La restricción de control "CK_libros_preciominmax" ocupa 2 filas porque en su definición se nombran 2 campos (indicados en "COLUMN_NAME").

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    preciomin number(5,2),
    preciomay number(5,2)
);

insert into libros values (1,'Uno','Bach','Planeta',22,20);
insert into libros values (2,'El quijote','Cervantes','Emece',15,13);
insert into libros values (3,'Aprenda PHP','Mario Molina','Siglo XXI',53,48);
insert into libros values (4,'Java en 10 minutos','Garcia','Siglo XXI',35,40);

alter table libros
add constraint PK_libros_codigo
primary key (codigo);

alter table libros
add constraint UQ_libros
unique (titulo,codigo,editorial);

alter table libros
add constraint CK_libros_precios_positivo
check (preciomin>=0 and preciomay>=0);

insert into libros values (5,'Matematica estas ahi','Paenza','Siglo XXI',-15,30);

update libros set preciomay=-20 where titulo='Uno';

alter table libros
add constraint CK_libros_preciominmay
check (preciomay<=preciomin);

update libros set preciomay=30
where titulo='Java en 10 minutos';

alter table libros
add constraint CK_libros_preciominmay
check (preciomay<=preciomin);

select *from user_constraints where table_name='LIBROS';

insert into libros values (6,'El gato con botas',null,'Planeta',25,30);

select *from user_cons_columns where table_name='LIBROS';
```

## Ejercicios propuestos

## Ejercicio 01

Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla:
 
```sql
drop table empleados;
```

2. Créela con la siguiente estructura:

```sql
create table empleados (
    documento char(8),
    nombre varchar2(30),
    cantidadhijos number(2),
    seccion varchar2(20),
    sueldo number(6,2) default -1
);
```

3. Agregue una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo.
```sql
ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_sueldo_positivo CHECK (sueldo >= 0);

```
Note que el campo "sueldo" tiene establecido un valor por defecto (el valor -1) que va contra la restricción; Oracle no controla esto, permite establecer la restricción, pero al intentar ingresar un registro con el valor por defecto en tal campo, muestra un mensaje de error.

4. Intente ingresar un registro con la palabra clave "default" en el campo "sueldo" (mensaje de error)
```sql
INSERT INTO empleados VALUES ('11111111','Juan Perez',0,'Ventas',DEFAULT);

```
5. Ingrese algunos registros válidos:

```sql
insert into empleados values ('22222222','Alberto Lopez',1,'Sistemas',1000);
insert into empleados values ('33333333','Beatriz Garcia',2,'Administracion',3000);
insert into empleados values ('34444444','Carlos Caseres',0,'Contaduría',6000);
```

6. Intente agregar otra restricción "check" al campo sueldo para asegurar que ninguno supere el valor 5000.
La sentencia no se ejecuta porque hay un sueldo que no cumple la restricción.
```sql
ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_sueldo_max CHECK (sueldo <= 5000);

```
7. Elimine el registro infractor y vuelva a crear la restricción
```sql
DELETE FROM empleados WHERE sueldo > 5000;

ALTER TABLE empleados
DROP CONSTRAINT CK_empleados_sueldo_max;

ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_sueldo_max CHECK (sueldo <= 5000);

```
8. Establezca una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría".
```sql
ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_seccion_valores CHECK (seccion IN ('Sistemas', 'Administracion', 'Contaduría'));

```
9. Ingrese un registro con valor "null" en el campo "seccion".
```sql
INSERT INTO empleados VALUES ('55555555','Elena Martínez',1,NULL,2000);

```
10. Establezca una restricción "check" para "cantidadhijos" que permita solamente valores entre 0 y 15.
```sql
ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_cantidadhijos_rango CHECK (cantidadhijos BETWEEN 0 AND 15);

```
11. Vea todas las restricciones de la tabla (4 filas)
```sql
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
12. Intente agregar un registro que vaya contra alguna de las restricciones al campo "sueldo".
Mensaje de error porque se infringe la restricción "CK_empleados_sueldo_positivo".
```sql
INSERT INTO empleados VALUES ('66666666','Fernando González',1,'Ventas',-100);

```
13. Intente modificar un registro colocando en "cantidadhijos" el valor "21".
```sql
UPDATE empleados
SET cantidadhijos = 21
WHERE documento = '22222222

```
14. Intente modificar el valor de algún registro en el campo "seccion" cambiándolo por uno que no esté incluido en la lista de permitidos.
```sql
UPDATE empleados
SET seccion = 'Marketing'
WHERE documento = '22222222';

```
15. Intente agregar una restricción al campo sección para aceptar solamente valores que comiencen con la letra "B".
Note que NO se puede establecer esta restricción porque va en contra de la establecida anteriormente para el mismo campo, si lo permitiera, no podríamos ingresar ningún valor para "seccion".
```sql
ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_seccion_letra_B
CHECK (seccion LIKE 'B%');

```
16. Agregue un registro con documento nulo.
```sql
INSERT INTO empleados VALUES (NULL,'Ana Morales',0,'Ventas',1500);

```
17. Intente agregar una restricción "primary key" para el campo "documento".
No lo permite porque existe un registro con valor nulo en tal campo.
```sql
ALTER TABLE empleados
ADD CONSTRAINT PK_empleados_documento PRIMARY KEY (documento);

```
18. Elimine el registro que infringe la restricción y establezca la restricción del punto 17.
```sql
DELETE FROM empleados WHERE documento IS NULL;

ALTER TABLE empleados
ADD CONSTRAINT PK_empleados_documento PRIMARY KEY (documento);

```
19. Consulte "user_constraints", mostrando los campos "constraint_name", "constraint_type" y "search_condition" de la tabla "empleados" (5 filas)
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
20. Consulte el catálogo "user_cons_colums" recuperando el nombre de las restricciones establecidas en el campo sueldo de la tabla "empleados" (2 filas)
```sql
SELECT constraint_name
FROM user_cons_columns
WHERE table_name = 'EMPLEADOS'
AND column_name = 'SUELDO';

```


## Ejercicio 02

Una playa de estacionamiento almacena los datos de los vehículos que ingresan en la tabla llamada "vehiculos".

1. Setee el formato de "date" para que nos muestre día, mes, año, hora y minutos:

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

2. Elimine la tabla "vehiculos" y créela con la siguiente estructura:

```sql
drop table vehiculos;

create table vehiculos(
    numero number(5),
    patente char(6),
    tipo char(4),
    fechahoraentrada date,
    fechahorasalida date
);
```

3. Ingrese algunos registros:

```sql
insert into vehiculos values(1,'AIC124','auto','17/01/2017 8:05','17/01/2017 12:30');
insert into vehiculos values(2,'CAA258','auto','17/01/2017 8:10',null);
insert into vehiculos values(3,'DSE367','moto','17/01/2017 8:30','17/01/2017 18:00');
```

4. Agregue una restricción de control que especifique que el campo "tipo" acepte solamente los valores "auto" y "moto":

```sql
alter table vehiculos
add constraint CK_vehiculos_tipo_valores
check (tipo in ('auto','moto'));
```

5. Intente modificar el valor del campo "tipo" ingresando un valor inexistente en la lista de valores permitidos por la restricción establecida a dicho campo.
```sql
update vehiculos
set tipo = 'camioneta'
where numero = 1;

```
6. Ingrese un registro con valor nulo para "tipo".
```sql
insert into vehiculos values(4, 'XYZ123', null, '18/01/2017 10:00', null);

```
7. Agregue una restricción de control al campo "fechahoraentrada" que establezca que sus valores no sean posteriores a "fechahorasalida".
```sql
alter table vehiculos
add constraint CK_vehiculos_fechas
check (fechahoraentrada <= fechahorasalida);

```
8. Intente modificar un registro para que la salida sea anterior a la entrada.
```sql
update vehiculos
set fechahoraentrada = '19/01/2017 14:00',
    fechahorasalida = '19/01/2017 12:00'
where numero = 1;

```
9. Vea todas las restricciones para la tabla "vehiculos".
```sql
select constraint_name, constraint_type
from user_constraints
where table_name = 'VEHICULOS';

```
10. Ingrese un registro con valor nulo para "fechahoraentrada".
```sql
insert into vehiculos values(5, 'ABC789', 'auto', null, '20/01/2017 16:30');

```
11. Vea todos los registros.
```sql
select * from vehiculos;

```
12. Consulte "user_cons_columns" y analice la información retornada.
```sql
select constraint_name, column_name
from user_cons_columns
where table_name = 'VEHICULOS';

```