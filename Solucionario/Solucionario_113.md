# 113. Disparador de actualizacion - campos (updating)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las actualizaciones de los precios de los libros almacenando en la tabla "controlprecios", la fecha, el código del libro, el antiguo y nuevo precio. También controla cualquier otra actualización almacenando en "control" el nombre del usuario, la fecha y el código del libro.

Elimine las tablas:

```sql
drop table controlprecios;
drop table libros;
drop table control;
```

Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2),
    stock number(4)
);

create table control(
    usuario varchar2(30),
    fecha date,
    codigo number(6)
);

create table controlprecios(
    fecha date,
    codigo number(6),
    precioanterior number(6,2),
    precionuevo number(6,2)
);
```

Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25,100);
insert into libros values(103,'El aleph','Borges','Emece',28,0);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12,50);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55,200);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35,10);
```

Cree un trigger a nivel de fila que se dispare "antes" que se ejecute un "update" sobre la tabla "libros".

En el cuerpo del trigger se debe averiguar el campo que ha sido modificado; en caso de modificarse el "precio", se ingresa en la tabla "controlPrecios" la fecha, el código del libro y el antiguo y nuevo precio; en caso de actualizarse cualquier otro campo, se almacena en la tabla "control" el nombre del usuario que realizó la modificación, la fecha y el código del libro modificado.

```sql
create or replace trigger tr_actualizar_libros
beforeupdate
on libros
    for each row
begin
    if updating ('precio') then
        insert into controlprecios values(sysdate,:old.codigo,:old.precio,:new.precio);
    else
        insert into control values(user,sysdate,:old.codigo);
    end if;
end tr_actualizar_libros;
/
```

Actualice el precio de un libro:

```sql
update libros set precio=35 where codigo=100;
```

Verifique que el trigger se ha disparado consultando la tabla "controlprecios":

```sql
select *from controlprecios;
```

Se ha insertado una fila.

Verifique que la tabla "control" no tiene registros.

```sq *from control;
```

Actualice un campo diferente de precio:

```sql
update libros set stock=0 where codigo=145;
```

Verifique que el trigger se ha disparado consultando la tabla "control":

```sql
select *from control;
```

Se ha insertado una fila.

Verifique que la tabla "controlprecios" no tiene nuevos registros:

```sql
select *from controlprecios;
```

## Ejercicios propuestos

Un comercio almacena los datos de los artículos que tiene para la venta en una tabla denominada "articulos". En otra tabla denominada "pedidos" almacena el código de cada artículo y la cantidad que necesita solicitar a los mayoristas. En una tabla llamada "controlPrecios" almacena la fecha, el código del artículo y ambos precios (antiguo y nuevo).

1. Elimine las tablas:

```sql
drop table articulos;
drop table pedidos;
drop table controlPrecios;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table articulos(
    codigo number(4),
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4)
);

create table pedidos(
    codigo number(4),
    cantidad number(4)
);

create table controlPrecios(
    fecha date,
    codigo number(4),
    anterior number(6,2),
    nuevo number(6,2)
);
```

3. Ingrese algunos registros en "articulos":

```sql
insert into articulos values(100,'cuaderno rayado 24h',4.5,100);
insert into articulos values(102,'cuaderno liso 12h',3.5,150);
insert into articulos values(104,'lapices color x6',8.4,60);
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas xxx',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);
```

4. Ingrese en "pedidos" todos los códigos de "articulos", con "cantidad" cero
```sql
insert into pedidos (codigo, cantidad)
select codigo, 0 from articulos;

```
5. Active el paquete "dbms_output":

```sql
set serveroutput on;
execute dbms_output.enable(20000);
```

6. Cada vez que se disminuye el stock de un artículo de la tabla "articulos", se debe incrementar la misma cantidad de ese artículo en "pedidos" y cuando se incrementa en "articulos", se debe disminuir la misma cantidad en "pedidos". Si se ingresa un nuevo artículo en "articulos", debe agregarse un registro en "pedidos" con "cantidad" cero. Si se elimina un registro en "articulos", debe eliminarse tal artículo de "pedidos". Cree un trigger para los tres eventos (inserción, borrado y actualización), a nivel de fila, sobre "articulos", para los campos "stock" y "precio", que realice las tareas descriptas anteriormente, si el campo modificado es "stock". Si el campo modificado es "precio", almacene en la tabla "controlPrecios", la fecha, el código del artículo, el precio anterior y el nuevo.
```sql
create or replace trigger tr_articulos
after insert or update or delete on articulos
for each row
begin
    if inserting then
        dbms_output.put_line('Trigger activado por inserción');
        insert into pedidos (codigo, cantidad) values (:new.codigo, 0);
    elsif deleting then
        dbms_output.put_line('Trigger activado por borrado');
        delete from pedidos where codigo = :old.codigo;
    elsif updating then
        if :new.stock < :old.stock then
            dbms_output.put_line('Trigger activado por actualización de stock');
            update pedidos set cantidad = cantidad + (:old.stock - :new.stock) where codigo = :new.codigo;
        elsif :new.stock > :old.stock then
            dbms_output.put_line('Trigger activado por actualización de stock');
            update pedidos set cantidad = cantidad - (:new.stock - :old.stock) where codigo = :new.codigo;
        end if;
        
        if :new.precio <> :old.precio then
            dbms_output.put_line('Trigger activado por actualización de precio');
            insert into controlPrecios (fecha, codigo, anterior, nuevo) values (sysdate, :new.codigo, :old.precio, :new.precio);
        end if;
    end if;
end;
/

```
El trigger muestra el mensaje "Trigger activado" cada vez que se dispara; en cada "if" muestra un segundo mensaje que indica cuál condición se ha cumplido.

7. Disminuya el stock del artículo "100" a 30
Un mensaje muestra que el trigger se ha disparado actualizando el "stock".
```sql
update articulos set stock = 30 where codigo = 100;

```
8. Verifique que el trigger se disparó consultando la tabla "pedidos" (debe aparecer "70" en "cantidad" en el registro correspondiente al artículo "100")
```sql
select * from pedidos where codigo = 100;

```
9. Ingrese un nuevo artículo en "articulos"
Un mensaje muestra que el trigger se ha disparado por una inserción.
```sql
insert into articulos values(280,'borrador grande',1.8,80);

```
10. Verifique que se ha agregado un registro en "pedidos" con código "280" y cantidad igual a 0
```sql
select * from pedidos where codigo = 280;

```
11. Elimine un artículo de "articulos"
Un mensaje muestra que el trigger se ha disparado por un borrado.
```sql
delete from articulos where codigo = 234;

```
12. Verifique que se ha borrado el registro correspondiente al artículo con código "234" en "pedidos"
```sql
select * from pedidos where codigo = 234;

```
13. Modifique el precio de un artículo
Un mensaje muestra que el trigger se ha disparado por una actualización de precio.
```sql
update articulos set precio = 5.5 where codigo = 160;

```
14. Verifique que se ha agregado un registro en "controlPrecios"
```sql
select * from controlPrecios;

```
15. Modifique la descripción de un artículo
El trigger no se ha disparado, no aparece mensaje.
```sql
update articulos set descripcion = 'regla 30cm.' where codigo = 160;

```
16. Modifique el precio, stock y descripcion de un artículo
Un mensaje muestra que el trigger se ha disparado por una actualización de stock y otra de precio. La actualización de "descripcion" no disparó el trigger.
```sql
update articulos set precio = 10.5, stock = 20, descripcion = 'compas ABC' where codigo = 173;

```
17. Verifique que se ha agregado un registro en "controlPrecios" y se ha modificado el campo "cantidad" con el valor "5"
```sql
select * from controlPrecios;

```
18. Modifique el stock de varios artículos en una sola sentencia
Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por actualizaciones de stock.
```sql
update articulos set stock = stock - 5 where codigo in (100, 102, 104, 160);

```
19. Verifique que se han modificado 4 registros en "pedidos"
```sql
select * from pedidos;

```
20. Modifique el precio de varios artículos en una sola sentencia
Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por actualizaciones de precio.
```sql
update articulos set precio = precio * 1.1 where codigo in (100, 102, 104, 160);

```
21. Verifique que se han agregado 4 nuevos registros en "controlPrecios"
```sql
select * from controlPrecios;

```
22. Elimine varios artículos en una sola sentencia
Cuatro mensajes muestran que el trigger se ha disparado 4 veces, por borrado de registros.
```sql
delete from articulos where codigo in (100, 102, 104, 160);

```
23. Verifique que se han eliminado 4 registros en "pedidos"
```sql
select * from pedidos;

```