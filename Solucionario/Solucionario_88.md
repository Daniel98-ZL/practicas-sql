# 88. Vistas (otras consideraciones: force)

## Practica de laboratorio

Una empresa almacena la información de sus empleados en una tabla llamada "empleados".

Eliminamos la tabla:

```sql
drop table empleados;
```

Eliminamos la vista "vista_empleados":

```sql
drop view vista_empleados;
```

Creamos la vista "vista_empleados" que muestre algunos campos de "empleados", pero la tabla "empleados" no existe, por ello, debemos agregar, al crear la vista "force":

```sql
create force view vista_empleados as
select documento,nombre,seccion
from empleados;
```

Creamos la tabla:

```sql
create table empleados(
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    seccion varchar2(30)
);
```

Ingresamos algunos registros:

```sql
insert into empleados values('22222222','Lopez Ana','Colon 123','Sistemas');
insert into empleados values('23333333','Lopez Luis','Sucre 235','Sistemas');
insert into empleados values('24444444','Garcia Marcos','Sarmiento 1234','Contaduria');
insert into empleados values('25555555','Gomez Pablo','Bulnes 321','Contaduria');
insert into empleados values('26666666','Perez Laura','Peru 1254','Secretaria');
```

Consultamos la vista:

```sql
select *from vista_empleados;
```

Veamos el texto de la vista consultando "user_views":

```sql
select view_name,text from user_views where view_name='VISTA_EMPLEADOS';
```

Creamos o reemplazamos (si existe) la vista "vista_empleados" que muestre todos los campos de la tabla "empleados":

```sql
create or replace view vista_empleados as
select *from empleados;
```

Consultamos la vista:

```sql
select *from vista_empleados;
```

Agregamos un campo a la tabla "empleados":

```sql
select table empleados
add sueldo number(6,2);
```

Consultamos la vista "vista_empleados":

```sql
select *from vista_empleados;
```

Note que el nuevo campo agregado a "empleados" no aparece, a pesar que la vista indica que muestre todos los campos de dicha tabla; esto sucede porque los campos se seleccionan al ejecutar "create view", para que aparezcan debemos volver a crear la vista:

```sql
create or replace view vista_empleados as
select *from empleados;
```

Consultemos la vista:

```sql
select *from vista_empleados; 
```

Ahora si aparece el nuevo campo "sueldo" de "empleados";

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table empleados;

drop view vista_empleados;

 -- Creamos la vista "vista_empleados" que muestre algunos campos de "empleados",
 -- pero la tabla "empleados" no existe, por ello, debemos agregar, al crear
 -- la vista "force":
create force view vista_empleados as
select documento,nombre,seccion
from empleados;

 -- Creamos la tabla:
create table empleados(
    documento char(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    seccion varchar2(30)
);

insert into empleados values('22222222','Lopez Ana','Colon 123','Sistemas');
insert into empleados values('23333333','Lopez Luis','Sucre 235','Sistemas');
insert into empleados values('24444444','Garcia Marcos','Sarmiento 1234','Contaduria');
insert into empleados values('25555555','Gomez Pablo','Bulnes 321','Contaduria');
insert into empleados values('26666666','Perez Laura','Peru 1254','Secretaria');

 -- Consultamos la vista:
select *from vista_empleados;

 -- Veamos el texto de la vista consultando "user_views":
select view_name,text from user_views where view_name='VISTA_EMPLEADOS';

 -- Creamos o reemplazamos (si existe) la vista "vista_empleados" que muestre
 -- todos los campos de la tabla "empleados":
create or replace view vista_empleados as
select *from empleados;

 -- Consultamos la vista:
select *from vista_empleados;

 -- Agregamos un campo a la tabla "empleados":
select table empleados
add sueldo number(6,2);

 -- Consultamos la vista "vista_empleados":
select * from vista_empleados;
 -- Note que el nuevo campo agregado a "empleados" no aparece,
 -- a pesar que la vista indica que muestre todos los campos
 -- de dicha tabla; esto sucede porque los campos se seleccionan
 -- al ejecutar "create view", para que aparezcan debemos volver
 -- a crear la vista:
create or replace view vista_empleados as
select *from empleados;

 -- Consultemos la vista:
select * from vista_empleados; 
```

## Ejercicios propuestos

Una empresa almacena la información de sus clientes en una tabla llamada "clientes".

1. Elimine la tabla:

```sql
drop table clientes;
```

2. Elimine la vista "vista_clientes":

```sql
drop view vista_clientes;
```

3. Intente crear o reemplazar la vista "vista_clientes" para que muestre el nombre, domicilio y ciudad de todos los clientes de "Cordoba" (sin emplear "force")
Mensaje de error porque la tabla referenciada no existe.
```sql
CREATE OR REPLACE VIEW vista_clientes AS
SELECT nombre, domicilio, ciudad
FROM clientes
WHERE ciudad = 'Cordoba';

```
4. Cree o reemplace la vista "vista_clientes" para que recupere el nombre, apellido y ciudad de todos los clientes de "Cordoba" empleando "force"
```sql
CREATE OR REPLACE FORCE VIEW vista_clientes AS
SELECT nombre, domicilio, ciudad
FROM clientes
WHERE ciudad = 'Cordoba';

```
5. Cree la tabla:

```sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);
```

6. Ingrese algunos registros:

```sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');
```

7. Cree o reemplace la vista "vista_clientes" para que muestre todos los campos de la tabla "clientes"
```sql
CREATE OR REPLACE VIEW vista_clientes AS
SELECT * FROM clientes;

```
8. Consulte la vista
```sql
SELECT * FROM vista_clientes;

```
9. Agregue un campo a la tabla "clientes"
```sql
ALTER TABLE clientes ADD telefono VARCHAR2(15);

```
10. Consulte la vista "vista_clientes"
El nuevo campo agregado a "clientes" no aparece, pese a que la vista indica que muestre todos los campos de dicha tabla.
```sql
SELECT * FROM vista_clientes;

```
11. Modifique la vista para que aparezcan todos los campos.
```sql
CREATE OR REPLACE VIEW vista_clientes AS
SELECT * FROM clientes;

```