# 32. Registros duplicados (Distinct)

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values('El aleph','Borges','Planeta');

insert into libros
values('Martin Fierro','Jose Hernandez','Emece');

insert into libros
values('Martin Fierro','Jose Hernandez','Planeta');

insert into libros
values('Antologia poetica','Borges','Planeta');

insert into libros
values('Aprenda PHP','Mario Molina','Emece');

insert into libros
values('Aprenda PHP','Lopez','Emece');

insert into libros
values('Manual de PHP', 'J. Paez', null);

insert into libros
values('Cervantes y el quijote',null,'Paidos');

insert into libros
values('Harry Potter y la piedra filosofal','J.K. Rowling','Emece');

insert into libros
values('Harry Potter y la camara secreta','J.K. Rowling','Emece');

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Paidos');

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');

insert into libros
values('PHP de la A a la Z',null,null);

insert into libros
values('Uno','Richard Bach','Planeta');
```

Para obtener la lista de autores sin repetición tipeamos:

```sql
select distinct autor from libros;
```

Note que aparece "null" como un valor para "autor"· Para obtener la lista de autores conocidos, es decir, no incluyendo "null" en la lista:

```sql
select distinct autor from libros
where autor is not null;
```

Contamos los distintos autores:

```sql
select count(distinct autor)
from libros;
```

Queremos los nombres de las editoriales sin repetir:

```sql
select distinct editorial from libros;
```

Queremos saber la cantidad de editoriales distintas:

```sql
select count(distinct editorial) from libros;
```

La combinamos con "where" para obtener los distintos autores de la editorial "Planeta":

```sql
select distinct autor from libros
where editorial='Planeta';
```

Contamos los distintos autores que tiene cada editorial empleando "group by":

```sql
select editorial,count(distinct autor)
from libros
group by editorial;
```

Mostramos los títulos y editoriales de los libros sin repetir títulos ni editoriales:

```sql
select distinct titulo,editorial
from libros
order by titulo;
```

Note que los registros no están duplicados, aparecen títulos iguales pero con editorial diferente, cada registro es diferente.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15)
);

insert into libros
values('El aleph','Borges','Planeta');

insert into libros
values('Martin Fierro','Jose Hernandez','Emece');

insert into libros
values('Martin Fierro','Jose Hernandez','Planeta');

insert into libros
values('Antologia poetica','Borges','Planeta');

insert into libros
values('Aprenda PHP','Mario Molina','Emece');

insert into libros
values('Aprenda PHP','Lopez','Emece');

insert into libros
values('Manual de PHP', 'J. Paez', null);

insert into libros
values('Cervantes y el quijote',null,'Paidos');

insert into libros
values('Harry Potter y la piedra filosofal','J.K. Rowling','Emece');

insert into libros
values('Harry Potter y la camara secreta','J.K. Rowling','Emece');

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Paidos');

insert into libros
values('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');

insert into libros
values('PHP de la A a la Z',null,null);

insert into libros
values('Uno','Richard Bach','Planeta');

select distinct autor from libros;

select distinct autor from libros
where autor is not null;

select count(distinct autor)
from libros;

select distinct editorial from libros;

select count(distinct editorial) from libros;

select distinct autor from libros
where editorial='Planeta';

select editorial,count(distinct autor)
from libros
group by editorial;

select distinct titulo,editorial
from libros
order by titulo;
```

## Ejercicios propuestos

## Ejercicio 01

Una empresa tiene registrados sus clientes en una tabla llamada "clientes".

1. Elimine la tabla "clientes" y créela con la siguiente estructura:

```sql
 drop table clientes;

 create table clientes (
  nombre varchar2(30) not null,
  domicilio varchar2(30),
  ciudad varchar2(20),
  provincia varchar2(20)
);
```

2. Ingrese algunos registros:

```sql
insert into clientes
values ('Lopez Marcos','Colon 111','Cordoba','Cordoba');

insert into clientes
values ('Perez Ana','San Martin 222','Cruz del Eje','Cordoba');

insert into clientes
values ('Garcia Juan','Rivadavia 333','Villa del Rosario','Cordoba');

insert into clientes
values ('Perez Luis','Sarmiento 444','Rosario','Santa Fe');

insert into clientes
values ('Pereyra Lucas','San Martin 555','Cruz del Eje','Cordoba');

insert into clientes
values ('Gomez Ines','San Martin 666','Santa Fe','Santa Fe');

insert into clientes
values ('Torres Fabiola','Alem 777','Villa del Rosario','Cordoba');

insert into clientes
values ('Lopez Carlos',null,'Cruz del Eje','Cordoba');

insert into clientes
values ('Ramos Betina','San Martin 999','Cordoba','Cordoba');

insert into clientes
values ('Lopez Lucas','San Martin 1010','Posadas','Misiones');
```

3. Obtenga las provincias sin repetir (3 registros)
```sql
SELECT DISTINCT provincia
FROM clientes;

```
4. Cuente las distintas provincias (retorna 3)
```sql
SELECT COUNT(DISTINCT provincia) AS cantidad_provincias
FROM clientes;

```
5. Se necesitan los nombres de las ciudades sin repetir (6 registros)
```sql
SELECT DISTINCT ciudad
FROM clientes;

```
6. Obtenga la cantidad de ciudades distintas (devuelve 6)
```sql
SELECT COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM clientes;

```
7. Combine con "where" para obtener las distintas ciudades de la provincia de Cordoba (3 registros)
```sql
SELECT DISTINCT ciudad
FROM clientes
WHERE provincia = 'Cordoba';

```
8. Contamos las distintas ciudades de cada provincia empleando "group by" (3 filas)
```sql
SELECT provincia, COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM clientes
GROUP BY provincia;

```
## Ejercicio 02

La provincia almacena en una tabla llamada "inmuebles" los siguientes datos de los inmuebles y sus propietarios para cobrar impuestos:

1. Elimine la tabla:

```sql
drop table inmuebles;
```

2. Créela con la siguiente estructura:

```sql
create table inmuebles (
    documento varchar2(8) not null,
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(20),
    barrio varchar2(20),
    ciudad varchar2(20),
    tipo char(1),..b=baldio, e: edificado
    superficie number(8,2)
);
```

3. Ingrese algunos registros:

```sql
insert into inmuebles
values ('11000000','Perez','Alberto','San Martin 800','Centro','Cordoba','e',100);

insert into inmuebles
values ('11000000','Perez','Alberto','Sarmiento 245','Gral. Paz','Cordoba','e',200);

insert into inmuebles
values ('12222222','Lopez','Maria','San Martin 202','Centro','Cordoba','e',250);

insert into inmuebles
values ('13333333','Garcia','Carlos','Paso 1234','Alberdi','Cordoba','b',200);

insert into inmuebles
values ('13333333','Garcia','Carlos','Guemes 876','Alberdi','Cordoba','b',300);

insert into inmuebles
values ('14444444','Perez','Mariana','Caseros 456','Flores','Cordoba','b',200);

insert into inmuebles
values ('15555555','Lopez','Luis','San Martin 321','Centro','Carlos Paz','e',500);

insert into inmuebles
values ('15555555','Lopez','Luis','Lopez y Planes 853','Flores','Carlos Paz','e',350);

insert into inmuebles
values ('16666666','Perez','Alberto','Sucre 1877','Flores','Cordoba','e',150);
```

4. Muestre los distintos apellidos de los propietarios, sin repetir (3 registros)
```sql
SELECT DISTINCT apellido
FROM inmuebles;

```
5. Recupere los distintos documentos de los propietarios y luego muestre los distintos documentos de los propietarios, sin repetir y vea la diferencia (9 y 6 registros respectivamente)
```sql
SELECT documento
FROM inmuebles;

SELECT DISTINCT documento
FROM inmuebles;
```

6. Cuente, sin repetir, la cantidad de propietarios de inmuebles de la ciudad de Cordoba (5)
```sql
SELECT COUNT(DISTINCT documento) AS cantidad_propietarios
FROM inmuebles
WHERE ciudad = 'Cordoba';
```

7. Cuente la cantidad de inmuebles con domicilio en 'San Martin' (3)
```sql
SELECT COUNT(*) AS cantidad_inmuebles
FROM inmuebles
WHERE domicilio LIKE '%San Martin%';
```

8. Cuente la cantidad de inmuebles con domicilio en 'San Martin', sin repetir la ciudad (2 registros). Compare con la sentencia anterior.
```sql
SELECT COUNT(DISTINCT ciudad) AS cantidad_ciudades
FROM inmuebles
WHERE domicilio LIKE '%San Martin%';
```

9. Muestre los apellidos y nombres de todos los registros(9 registros)
```sql
SELECT apellido, nombre
FROM inmuebles;
```

10. Muestre los apellidos y nombres, sin repetir (5 registros)
Note que si hay 2 personas con igual nombre y apellido aparece una sola vez.
```sql
SELECT DISTINCT apellido, nombre
FROM inmuebles;

```

11. Muestre la cantidad de inmuebles que tiene cada propietario en barrios conocidos, agrupando por documento (6 registros)
```sql
SELECT documento, COUNT(*) AS cantidad_inmuebles
FROM inmuebles
WHERE barrio IS NOT NULL
GROUP BY documento;
```

12. Realice la misma consulta anterior pero en esta oportunidad, sin repetir barrio (6 registros)
Compare los valores con los obtenidos en el punto 11.
```sql
SELECT documento, COUNT(DISTINCT barrio) AS cantidad_barrios
FROM inmuebles
WHERE barrio IS NOT NULL
GROUP BY documento;

```
