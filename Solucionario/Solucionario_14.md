# 14. Tipos de datos alfanuméricos
## Ejercicios de laboratorio

### Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.

#### 1. Eliminamos la tabla "visitantes":

```sql
drop table visitantes;
```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 1 del comando :
drop table visitantes
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
#### 2. Creamos con la siguiente estructura:

```sql
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1),
    domicilio varchar2(30),
    ciudad varchar2(20),
    telefono varchar2(11)
);
```
Salida del Script

```sh
Table VISITANTES creado.
```
Los campos "nombre", "domicilio" y "ciudad" almacenarán valores cuya longitud varía, por ello elegimos el tipo "varchar2" y le damos a cada uno una longitud máxima estimando su tamaño. El campo "sexo" se define de tipo "char", porque necesitamos solamente 1 caracter "f" o "m", que siempre será fijo. El campo "telefono" también se define como varchar2 porque no todos los números telefónicos tienen la misma longitud.

#### 3. Ingresamos un registro:

```sql
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Ana Acosta',25,'f','Avellaneda 123','Cordoba','4223344');
```
Salida del Script

```sh
1 fila insertadas.
```
#### 4. Intentamos ingresar una cadena de mayor longitud que la definida en el campo "sexo":

```sql
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'fem','Bulnes 234','Cordoba','4515151');
```
Salida del Script

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "DANIEL"."VISITANTES"."SEXO" (real: 3, máximo: 1).
```sh
Error que empieza en la línea: 14 del comando -
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'fem','Bulnes 234','Cordoba','4515151')
Error en la línea de comandos : 14 Columna : 96
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."VISITANTES"."SEXO" (real: 3, máximo: 1)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
Aparece un mensaje de error y la sentencia no se ejecuta.

#### 5. Ingresamos el mismo registro, esta vez con un sólo caracter para el campo "sexo":

```sql
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'f','Bulnes 234','Cordoba','4515151');
```
Salida del Script

```sh
1 fila insertadas.
```
#### 6. Ingresamos un número telefónico olvidando las comillas, es decir, como un valor numérico:

```sql
insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Carlos Caseres',43,'m','Colon 345','Cordoba',03514555666);
```
Salida del Script
```sh
1 fila insertadas.
```
#### 7. Lo convierte a cadena, veámoslo:

```sql
select *from visitantes;
```
Salida del Script

```sh
Ana Acosta	25	f	Avellaneda 123	Cordoba	4223344
Betina Bustos	32	f	Bulnes 234	Cordoba	4515151
Carlos Caseres	43	m	Colon 345	Cordoba	3514555666
```
#### 8. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table visitantes;

create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1),
    domicilio varchar2(30),
    ciudad varchar2(20),
    telefono varchar2(11)
);

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Ana Acosta',25,'f','Avellaneda 123','Cordoba','4223344');

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'fem','Bulnes 234','Cordoba','4515151');

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Betina Bustos',32,'f','Bulnes 234','Cordoba','4515151');

insert into visitantes (nombre,edad,sexo,domicilio,ciudad,telefono) values ('Carlos Caseres',43,'m','Colon 345','Cordoba',03514555666);

select *from visitantes;
```

## Ejercicios propuestos

### Ejercico 01

#### Una concesionaria de autos vende autos usados y almacena los datos de los autos en una tabla llamada "autos".

##### 1. Elimine la tabla "autos"
```sql
drop table autos;
```
Salida del Script


Nos da un error de ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 22 del comando :
drop table autos
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
##### 2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo, estableciendo el campo "patente" como clave primaria:

```sql
create table autos(
    patente char(6),
    marca varchar2(20),
    modelo char(4),
    precio number(8,2),
    primary key (patente)
);
```
Salida del Script

```sh
Table AUTOS creado.
```
Hemos definido el campo "patente" de tipo "char" y no "varchar2" porque la cadena de caracteres siempre tendrá la misma longitud (6 caracteres). Lo mismo sucede con el campo "modelo", en el cual almacenaremos el año, necesitamos 4 caracteres fijos.

##### 3. Ingrese los siguientes registros:

```sql
insert into autos (patente,marca,modelo,precio) values('ABC123','Fiat 128','1970',15000);
insert into autos (patente,marca,modelo,precio) values('BCD456','Renault 11','1990',40000);
insert into autos (patente,marca,modelo,precio) values('CDE789','Peugeot 505','1990',80000);
insert into autos (patente,marca,modelo,precio) values('DEF012','Renault Megane','1998',95000);
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.

1 fila insertadas.

1 fila insertadas.
```
##### 4. Ingrese un registro omitiendo las comillas en el valor de "modelo"
```sql
insert into autos (patente,marca,modelo,precio) values('DEF013','Renault',1997,95500);

```
Salida del Script

```sh
1 fila insertadas.
```
Oracle convierte el valor a cadena.

##### 5. Vea cómo se almacenó.
```sql
select *from autos;
```
Salida del Script

```sh
ABC123	Fiat 128	1970	15000
BCD456	Renault 11	1990	40000
CDE789	Peugeot 505	1990	80000
DEF012	Renault Megane	1998	95000
DEF013	Renault	1997	95500
```
##### 6. Seleccione todos los autos modelo "1990"
```sql
select * from autos where modelo='1990';
```
Salida del Script

```sh
BCD456	Renault 11	1990	40000
CDE789	Peugeot 505	1990	80000
```
##### 7. Intente ingresar un registro con un valor de patente de 7 caracteres
```sql
insert into autos (patente,marca,modelo,precio) values('DEF0145','Renault',1997,95500);
```
Salida del Script

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "DANIEL"."AUTOS"."PATENTE" (real: 7, máximo: 6).
```sh
Error que empieza en la línea: 43 del comando -
insert into autos (patente,marca,modelo,precio) values('DEF0145','Renault',1997,95500)
Error en la línea de comandos : 43 Columna : 56
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."AUTOS"."PATENTE" (real: 7, máximo: 6)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
##### 8. Intente ingresar un registro con valor de patente repetida.
```sql
insert into autos (patente,marca,modelo,precio) values('DEF013','Renault',1997,95500);
```
Salida del Script

Nos da un error de ORA-00001: restricción única (DANIEL.SYS_C008334) violada.
```sh
Error que empieza en la línea: 45 del comando :
insert into autos (patente,marca,modelo,precio) values('DEF013','Renault',1997,95500)
Informe de error -
ORA-00001: restricción única (DANIEL.SYS_C008334) violada
```
### Ejercico 02

#### Una empresa almacena los datos de sus clientes en una tabla llamada "clientes".

##### 1. Elimine la tabla "clientes"
```sql
drop table clientes;
```
Salida del Script

Nos das un error de ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 47 del comando :
drop table clientes
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
##### 2. Créela eligiendo el tipo de dato más adecuado para cada campo:

```sql
create table clientes(
    documento char(8) not null,
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2 (11)
);
```
Salida del Script 

```sh
Table CLIENTES creado.
```
##### 3. Analice la definición de los campos. Se utiliza char(8) para el documento porque siempre constará de 8 caracteres. Para el número telefónico se usar "varchar2" y no un tipo numérico porque si bien es un número, con él no se realizarán operaciones matemáticas.
```sql
describe clientes;
```
Salida del Script

```sh
Nombre    ¿Nulo?   Tipo         
--------- -------- ------------ 
DOCUMENTO NOT NULL CHAR(8)      
APELLIDO           VARCHAR2(20) 
NOMBRE             VARCHAR2(20) 
DOMICILIO          VARCHAR2(30) 
TELEFONO           VARCHAR2(11)
```
##### 4. Ingrese algunos registros:

```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444','Perez','Juan','Sarmiento 980','4223344');
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('23444555','Perez','Ana','Colon 234',null);
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634',null);
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.

1 fila insertadas.
```
##### 5. Intente ingresar un registro con más caracteres que los permitidos para el campo "telefono"
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634','1111111111111111111');
```
Salida del Script

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "DANIEL"."CLIENTES"."TELEFONO" (real: 19, máximo: 11).
```sh
Error que empieza en la línea: 63 del comando -
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('30444555','Garcia','Luciana','Caseros 634','1111111111111111111')
Error en la línea de comandos : 63 Columna : 120
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."CLIENTES"."TELEFONO" (real: 19, máximo: 11)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
##### 6. Intente ingresar un registro con más caracteres que los permitidos para el campo "documento"
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('223334441234566362','Perez','Juan','Sarmiento 980','4223343');
```
Salida del Script

Nos da un error de ORA-12899: el valor es demasiado grande para la columna "DANIEL"."CLIENTES"."DOCUMENTO" (real: 18, máximo: 8).
```sh
Error que empieza en la línea: 65 del comando -
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('223334441234566362','Perez','Juan','Sarmiento 980','4223343')
Error en la línea de comandos : 65 Columna : 76
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."CLIENTES"."DOCUMENTO" (real: 18, máximo: 8)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).
```
##### 7. Intente ingresar un registro omitiendo las comillas en el campo "apellido"
```sql
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444',Zapana,'Juan','Sarmiento 980','4223344');
```
Salida del Script

Nos da un error de ORA-00984: columna no permitida aquí.
```sh
Error que empieza en la línea: 67 del comando -
insert into clientes (documento,apellido,nombre,domicilio,telefono) values('22333444',Zapana,'Juan','Sarmiento 980','4223344')
Error en la línea de comandos : 67 Columna : 87
Informe de error -
Error SQL: ORA-00984: columna no permitida aquí
00984. 00000 -  "column not allowed here"
*Cause:    
*Action:
```
##### 8. Seleccione todos los clientes de apellido "Perez" (2 registros)
```sql
SELECT * FROM clientes WHERE apellido ='Perez';
```
Salida del Script

```sh
22333444	Perez	Juan	Sarmiento 980	4223344
23444555	Perez	Ana	Colon 234	
```
