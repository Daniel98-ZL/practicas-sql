# 5. Recuperación de registros específicos (select . where)
## Ejercicios de laboratororio

### Trabajamos con la tabla "usuarios" que consta de 2 campos: nombre de usuario y clave.
#### 1. Eliminamos la tabla si ya existe:
```sql
drop table usuarios;
```
Salida del Script 

```sh
Table USUARIOS borrado.
```
#### 2. Creamos la tabla:

```sql
create table usuarios (
    nombre varchar2(30),
    clave varchar2(10)
);
```
Salida del Script

```sh
Table USUARIOS creado.
```

#### 3. Vemos la estructura de la tabla:

```sql
describe usuarios;
```
Salida del Script

```sh
Nombre ¿Nulo? Tipo         
------ ------ ------------ 
NOMBRE        VARCHAR2(30) 
CLAVE         VARCHAR2(10) 
```

#### 4. Ingresamos algunos registros:

```sql
insert into usuarios (nombre, clave) values ('Marcelo','Boca');
insert into usuarios (nombre, clave) values ('JuanPerez','Juancito');
insert into usuarios (nombre, clave) values ('Susana','River');
insert into usuarios (nombre, clave) values ('Luis','River');
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```

#### 5. Realizamos una consulta especificando una condición, queremos ver toda la información del usuario cuyo nombre es "Marcelo":

```sql
select *from usuarios where nombre='Marcelo';
```
Salida del Script

```sh
Marcelo	Boca
```
Nos muestra todos los campos del registro en el cual el campo "nombre" es igual a "Marcelo".

#### 6. Queremos ver el nombre de los usuarios cuya clave es "River":

```sql
select nombre from usuarios where clave='River';
```
Salida del Script

```sh
Susana
Luis
```
Nos muestra 2 usuarios.

#### 7. Realizamos un "select" de los nombres de los usuarios cuya clave es "Santi":

```sql
select nombre from usuarios where clave='Santi';
```
Salida del Script
```sh
No muestra nada
```
No se muestra ningún registro ya que ninguno cumple la condición.

#### 8. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table usuarios;
 
create table usuarios (
    nombre varchar2(30),
    clave varchar2(10)
);

describe usuarios;

insert into usuarios (nombre, clave) values ('Marcelo','Boca');
insert into usuarios (nombre, clave) values ('JuanPerez','Juancito');
insert into usuarios (nombre, clave) values ('Susana','River');
insert into usuarios (nombre, clave) values ('Luis','River');

select * from usuarios where nombre='Marcelo';

select nombre from usuarios where clave='River';

select nombre from usuarios where clave='Santi';
```

## Ejercicios propuestos

### Ejercicio 01

#### Trabaje con la tabla "agenda" en la que registra los datos de sus amigos.

##### 1. Elimine "agenda"
```sql
drop table agenda;
```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe
```sh
Error que empieza en la línea: 21 del comando :
drop table agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
##### 2. Cree la tabla, con los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11):

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
Salida del Script

```sh
Table AGENDA creado.
```
##### 3. Visualice la estructura de la tabla "agenda" (4 campos)
```sql
describe agenda;
```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
APELLIDO         VARCHAR2(30) 
NOMBRE           VARCHAR2(30) 
DOMICILIO        VARCHAR2(30) 
TELEFONO         VARCHAR2(11)
```
##### 4. Ingrese los siguientes registros ("insert into"):

```sql
insert into agenda(apellido,nombre,domicilio,telefono) values ('Acosta', 'Ana', 'Colon 123', '4234567');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Bustamante', 'Betina', 'Avellaneda 135', '4458787');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Hector', 'Salta 545', '4887788'); 
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Luis', 'Urquiza 333', '4545454');
insert into agenda(apellido,nombre,domicilio,telefono) values ('Lopez', 'Marisa', 'Urquiza 333', '4545454');
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 5. Seleccione todos los registros de la tabla (5 registros)
```sql
select * from agenda;
```
Salida del Script

```sh
Acosta	Ana	Colon 123	4234567
Bustamante	Betina	Avellaneda 135	4458787
Lopez	Hector	Salta 545	4887788
Lopez	Luis	Urquiza 333	4545454
Lopez	Marisa	Urquiza 333	4545454
```
##### 6. Seleccione el registro cuyo nombre sea "Marisa" (1 registro)
```sql
select * from agenda where nombre='Marisa';
```
Salida del Script

```sh
Lopez	Marisa	Urquiza 333	4545454
```
##### 7. Seleccione los nombres y domicilios de quienes tengan apellido igual a "Lopez" (3 registros)
```sql
select nombre,domicilio from agenda where apellido='Lopez';
```
Salida del Script

```sh
Hector	Salta 545
Luis	Urquiza 333
Marisa	Urquiza 333
```
##### 8. Seleccione los nombres y domicilios de quienes tengan apellido igual a "lopez" (en minúsculas)

```sql
select nombre,domicilio from agenda where apellido='lopez';
```
Salida del Script

```sh
No aparece ningun registro
```
No aparece ningún registro, ya que la cadena "Lopez" no es igual a la cadena "lopez".

##### 9. Muestre el nombre de quienes tengan el teléfono "4545454" (2 registros)

```sql
select nombre from agenda where telefono='4545454';
```
Salida del Script

```sh
Luis
Marisa
```
### Ejercicio 02

#### Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla llamada "articulos".

##### 1. Elimine la tabla si existe.

```sql
drop table articulos;
```
Salida del Script

```sh
Table ARTICULOS borrado.
```
##### 2. Cree la tabla "articulos" con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(7,2)
);
```
Salida del Script

```sh
Table ARTICULOS creado.
```

##### 3. Vea la estructura de la tabla:

```sql
describe articulos;
```
Salida del Script

```sh
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(7,2)  
```
##### 4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
insert into articulos (codigo, nombre, descripcion, precio) values (4,'teclado','ingles Biswal',100);
insert into articulos (codigo, nombre, descripcion, precio) values (5,'teclado','español Biswal',90);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

##### 5. Seleccione todos los datos de los registros cuyo nombre sea "impresora" (2 registros)

```sql
select * from articulos where nombre='impresora';
```
Salida del Script

```sh
1	impresora	Epson Stylus C45	400,8
2	impresora	Epson Stylus C85	500
```
##### 6. Muestre sólo el código, descripción y precio de los teclados (2 registros)

```sql
select codigo,descripcion,precio from articulos where nombre='teclado';
```
Salida del Script

```sh
4	ingles Biswal	100
5	español Biswal	90
```

