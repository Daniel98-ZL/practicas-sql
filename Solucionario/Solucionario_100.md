# 100. Control de flujo (for)

## Practica de laboratorio

En el siguiente ejemplo se muestra la tabla del 3. La variable "f" comienza en cero (límite inferior del for) y se va incrementando de a uno; el ciclo se repite hasta que "f" llega a 5 (límite superior del for), cuando llega a 6, el bucle finaliza.

```sql
 set serveroutput on;
 execute dbms_output.enable(20000);
 begin
  for f in 0..5 loop
   dbms_output.put_line('3x'||to_char(f)||'='||to_char(f*3));
  end loop;
 end;
 /
```

Para que el contador "f" se decremente en cada repetición, colocamos "reverse"; el contador comenzará por el valor del límite superior (5) y finalizará al llegar al límite inferior (0) decrementando de a uno. En este ejemplo mostramos la tabla del 3 desde el 5 hasta el 0:

```sql
 begin
  for f in reverse 0..5 loop
   dbms_output.put_line('3*'||to_char(f)||'='||to_char(f*3));
  end loop;
 end;
 /
```

Se pueden colocar "for" dentro de otro "for". Por ejemplo, con las siguientes líneas imprimimos las tablas del 2 y del 3 del 1 al 9:

```sql
begin
  for f in 2..3 loop
   dbms_output.put_line('tabla del '||to_char(f));
   for g in 1..9 loop
     dbms_output.put_line(to_char(f)||'x'||to_char(g)||'='||to_char(f*g));
   end loop;--fin del for g
  end loop;--fin del for f
end;
/
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
 set serveroutput on;
 execute dbms_output.enable(20000);
 begin
  for f in 0..5 loop
   dbms_output.put_line('3x'||to_char(f)||'='||to_char(f*3));
  end loop;
 end;
 /
 
 begin
  for f in reverse 0..5 loop
   dbms_output.put_line('3*'||to_char(f)||'='||to_char(f*3));
  end loop;
 end;
 /
 
 begin
  for f in 2..3 loop
   dbms_output.put_line('tabla del '||to_char(f));
   for g in 1..9 loop
     dbms_output.put_line(to_char(f)||'x'||to_char(g)||'='||to_char(f*g));
   end loop;--fin del for g
  end loop;--fin del for f
 end;
 /
```

## Ejercicios propuestos

1. Con la estructura repetitiva "for... loop" que vaya del 1 al 20, muestre los números pares.
Dentro del ciclo debe haber una estructura condicional que controle que el número sea par y si lo es, lo imprima por pantalla.
```sql
BEGIN
  FOR i IN 1..20 LOOP
    IF i MOD 2 = 0 THEN
      DBMS_OUTPUT.PUT_LINE(i);
    END IF;
  END LOOP;
END;
/

```
2. Con la estructura repetitiva "for... loop" muestre la sumatoria del número 5; la suma de todos los números del 1 al 5. Al finalizar el ciclo debe mostrarse por pantalla la sumatoria de 5 (15).
```sql
DECLARE
  sumatoria NUMBER := 0;
BEGIN
  FOR i IN 1..5 LOOP
    sumatoria := sumatoria + 5;
  END LOOP;
  DBMS_OUTPUT.PUT_LINE('Sumatoria de 5: ' || sumatoria);
END;
/

```
3. Cree una función que reciba un valor entero y retorne el factorial de tal número; el factorial se obtiene multiplicando el valor que recibe por el anterior hasta llegar a multiplicarlo por uno.
```sql
CREATE OR REPLACE FUNCTION calcular_factorial(numero IN NUMBER) RETURN NUMBER IS
  resultado NUMBER := 1;
BEGIN
  FOR i IN 1..numero LOOP
    resultado := resultado * i;
  END LOOP;
  RETURN resultado;
END;
/

```
4. Llame a la función creada anteriormente y obtenga el factorial de 5 y de 4 (120 y 24).
```sql
SELECT calcular_factorial(5) AS factorial_5 FROM dual;
SELECT calcular_factorial(4) AS factorial_4 FROM dual;

```
5. Cree un procedimiento que reciba dos parámetros numéricos; el procedimiento debe mostrar la tabla de multiplicar del número enviado como primer argumento, desde el 1 hasta el númeo enviado como segundo argumento. Emplee "for".
```sql
CREATE OR REPLACE PROCEDURE mostrar_tabla_multiplicar(
  numero IN NUMBER,
  limite IN NUMBER
) IS
BEGIN
  FOR i IN 1..limite LOOP
    DBMS_OUTPUT.PUT_LINE(numero || ' x ' || i || ' = ' || (numero * i));
  END LOOP;
END;
/

```
6. Ejecute el procedimiento creado anteriormente enviándole los valores necesarios para que muestre la tabla del 6 hasta el 20.
```sql
BEGIN
  mostrar_tabla_multiplicar(6, 20);
END;
/

```
7. Ejecute el procedimiento creado anteriormente enviándole los valores necesarios para que muestre la tabla del 9 hasta el 10.
```sql
BEGIN
  mostrar_tabla_multiplicar(9, 10);
END;
/

```