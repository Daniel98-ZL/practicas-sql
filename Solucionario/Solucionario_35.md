# 35. Alterar secuencia (alter sequence)

## Practica de laboratorio

Eliminamos la secuencia "sec_codigolibros":

```sql
drop sequence sec_codigolibros;
```

La creamos definiendo 1 como valor de inicio, 1 de incremento, 999 como valor máximo, 1 como mínimo valor y no circular:

```sql
create sequence sec_codigolibros
start with 1
increment by 1
maxvalue 999
minvalue 1
nocycle;
```

Vemos la información sobre la secuencia creada anteriormente:

```sql
select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';
```

Alteramos la secuencia para que el máximo valor sea 99999 y se incremente de a 2:

```sql
alter sequence sec_codigolibros
increment by 2
maxvalue 99999;
```

Veamos la información de la secuencia modificada consultando "all_sequences":

```sql
select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';
```

Ahora el valor de incremento es 2 y el máximo 99999, los demás valores permanecen como fueron definidos.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop sequence sec_codigolibros;

create sequence sec_codigolibros
start with 1
increment by 1
maxvalue 999
minvalue 1
nocycle;

select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';

alter sequence sec_codigolibros
increment by 2
maxvalue 99999;

select *from all_sequences where sequence_name='SEC_CODIGOLIBROS';
```

## Ejercicios propuestos

Una empresa registra los datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla "empleados":

```sql
drop table empleados;
```

2. Cree la tabla:

```sql
create table empleados(
    legajo number(3),
    documento char(8) not null,
    nombre varchar2(30) not null,
    primary key(legajo)
);
```

3. Elimine la secuencia "sec_legajoempleados" y luego créela estableciendo el valor mínimo (1), máximo (210), valor inicial (206), valor de incremento (2) y no circular. Finalmente inicialice la secuencia.
```sql
-- Eliminar la secuencia si existe
begin
    execute immediate 'drop sequence sec_legajoempleados';
exception
    when others then
        if sqlcode != -2289 then
            raise;
        end if;
end;
/

-- Crear la secuencia
create sequence sec_legajoempleados
    minvalue 1
    maxvalue 210
    start with 206
    increment by 2
    nocycle;

-- Inicializar la secuencia
select sec_legajoempleados.nextval from dual;

```
4. Ingrese algunos registros, empleando la secuencia creada para los valores de la clave primaria.

```sql
insert into empleados
values (sec_legajoempleados.currval,'22333444','Ana Acosta');

insert into empleados
values (sec_legajoempleados.nextval,'23444555','Betina Bustamante');

insert into empleados
values (sec_legajoempleados.nextval,'24555666','Carlos Caseros');
```

5. Recupere los registros de "libros" para ver los valores de clave primaria.
```sql
select * from empleados;

```
6. Vea el valor actual de la secuencia empleando la tabla "dual"
```sql
select sec_legajoempleados.currval from dual;

```
7. Intente ingresar un registro empleando "nextval":

```sql
insert into empleados
values (sec_legajoempleados.nextval,'25666777','Diana Dominguez');
```

Oracle muestra un mensaje de error indicando que la secuencia ha llegado a su valor máximo.

8. Altere la secuencia modificando el atributo "maxvalue" a 999.
```sql
alter sequence sec_legajoempleados
    maxvalue 999;

```
9. Obtenga información de la secuencia.
```sql
select sequence_name, min_value, max_value, last_number, increment_by, cycle_flag
from user_sequences
where sequence_name = 'SEC_LEGAJOEMPLEADOS';

```
10. Ingrese el registro del punto 7.
```sql
insert into empleados
values (sec_legajoempleados.nextval, '25666777', 'Diana Dominguez');

```
11. Recupere los registros.
```sql
select * from empleados;

```
12. Modifique la secuencia para que sus valores se incrementen en 1.
```sql
alter sequence sec_legajoempleados
    increment by 1;

```
13. Ingrese un nuevo registro:

```sql
insert into empleados
values (sec_legajoempleados.nextval,'26777888','Federico Fuentes');
```

14. Recupere los registros.
```sql
select * from empleados;

```
15. Elimine la secuencia creada.
```sql
-- Eliminar la secuencia
begin
    execute immediate 'drop sequence sec_legajoempleados';
exception
    when others then
        if sqlcode != -2289 then
            raise;
        end if;
end;
/

-- Confirmar la eliminación verificando si la secuencia todavía existe
select sequence_name
from user_sequences
where sequence_name = 'SEC_LEGAJOEMPLEADOS';

```
16. Consulte todos los objetos de la base de datos que sean secuencias y verifique que "sec_legajoempleados" ya no existe.
```sql
select object_name, object_type
from user_objects
where object_type = 'SEQUENCE'
and object_name = 'SEC_LEGAJOEMPLEADOS';

```