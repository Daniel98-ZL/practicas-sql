# 115. Disparador (eliminar)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario, la fecha, y el tipo de modificación que se realizó sobre la tabla "libros".

Eliminamos la tabla "libros" y la tabla "control":

```sql
drop table libros;
drop table control;
```

Creamos las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);
```

Creamos un desencadenador que se active cuando ingresamos un nuevo registro en "libros", debe almacenar en "control" el nombre del usuario que realiza el ingreso, la fecha e "insercion" en "operacion":

```sql
create or replace trigger tr_ingresar_libros
    before insert
    on libros
    for each row
begin
   insert into control values(user,sysdate,'insercion');
end tr_ingresar_libros;
/
```

Creamos un segundo disparador que se active cuando modificamos algún campo de "libros" y almacene en "control" el nombre del usuario que realiza la actualización, la fecha y en "operacion" coloque el nombre del campo actualizado:

```sql
create or replace trigger tr_actualizar_libros
    beforeupdate
    on libros
    for each row
begin
    if updating('codigo') then
       insert into control values(user,sysdate,'codigo');
    end if;
    if updating('titulo') then
       insert into control values(user,sysdate,'titulo');
    end if;
    if updating('autor') then
       insert into control values(user,sysdate,'autor');
    end if;
    if updating('editorial') then
       insert into control values(user,sysdate,'editorial');
    end if;
    if updating('precio') then
       insert into control values(user,sysdate,'precio');
    end if;
end tr_actualizar_libros;
/
```

Creamos un tercer trigger sobre "libros" que se active cuando eliminamos un registro de "libros", debe almacenar en "control" el nombre del usuario que realiza la eliminación, la fecha y "borrado" en "operacion":

```sql
create or replace trigger tr_eliminar_libros
    before delete
    on libros
    for each row
begin
   insert into control values(user,sysdate,'borrado');
end tr_eliminar_libros;
/
```

Vemos cuántos triggers están asociados a "libros"; consultamos el diccionario "user_triggers":

```sql
 
select trigger_name, triggering_event, status
  
from user_triggers
  
where table_name = 'LIBROS';
```

Hay tres.

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(101,'El aleph','Borges','Emece',28);
insert into libros values(102,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(103,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(144,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

Comprobamos que el trigge "tr_ingresar_libros" se disparó recuperando los registros de "control":

```sql
select *from control;
```

Hay 5 registros.

Actualizamos la editorial de varios libros y comprobamos que el trigger de actualización se disparó recuperando los registros de "control":

```sql
update libros set editorial='Sudamericana' where editorial='Planeta';
select *from control;
```

2 nuevos registros.

Borramos un libro de "libros" y comprobamos que el trigger de borrado se disparó recuperando los registros de "control":

```sql
delete from libros where codigo=101;
select *from control;
```

Actualizamos el autor de un libro y comprobamos que el trigger de actualización se dispara recuperando los registros de "control":

```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
select *from control;
```

Eliminamos la tabla "libros":

```sql
drop table libros;
```

Consultamos el diccionario "user_triggers" para comprobar que al eliminar "libros" se eliminaron también los triggers asociados a ella:

```sql
select trigger_name, triggering_event, status
from user_triggers
where table_name = 'LIBROS';
```

Los tres trigger asociados a "libros" han sido eliminados.

## Ejercicios propuestos

Un comercio almacena los datos de los artículos que tiene para la venta en una tabla denominada "articulos". En otra tabla denominada "ventas" almacena el código de cada artículo, la cantidad que se vende y la fecha.

1. Elimine las tablas:

```sql
drop table ventas;
drop table articulos;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table articulos(
    codigo number(4) not null,
    descripcion varchar2(40),
    precio number (6,2),
    stock number(4),
    constraint PK_articulos_codigo
    primary key (codigo)
);

create table ventas(
    codigo number(4),
    cantidad number(4),
    fecha date,
    constraint FK_ventas_articulos
    foreign key (codigo)
    references articulos(codigo)
);
```

3. Cree una secuencia llamada "sec_codigoart", estableciendo que comience en 1, sus valores estén entre 1 y 9999 y se incrementen en 1. Antes elimínela por si existe.
```sql
-- Primero eliminamos la secuencia si existe
begin
    execute immediate 'drop sequence sec_codigoart';
exception
    when others then
        null;
end;
-- Luego creamos la secuencia
create sequence sec_codigoart
    start with 1
    minvalue 1
    maxvalue 9999
    increment by 1;

```
4. Active el paquete para permitir mostrar salida en pantalla.
```sql
set serveroutput on;

```
5. Cree un trigger que coloque el siguiente valor de una secuencia para el código de "articulos" cada vez que se ingrese un nuevo artículo.
Podemos ingresar un nuevo registro en "articulos" sin incluir el código porque lo ingresará el disparador luego de calcularlo. Si al ingresar un registro en "articulos" incluimos un valor para código, será ignorado y reemplazado por el valor calculado por el disparador.
```sql
create or replace trigger tr_generar_codigoart
before insert on articulos
for each row
begin
    :new.codigo := sec_codigoart.nextval;
end;
/

```
6. Ingrese algunos registros en "articulos" sin incluir el código:

```sql
insert into articulos (descripcion, precio, stock) values ('cuaderno rayado 24h',4.5,100);
insert into articulos (descripcion, precio, stock) values ('cuaderno liso 12h',3.5,150);
insert into articulos (descripcion, precio, stock) values ('lapices color x6',8.4,60);
```

7. Recupere todos los artículos para ver cómo se almacenó el código
```sql
select * from articulos;

```
8. Ingrese algunos registros en "articulos" incluyendo el código:

```sql
insert into articulos values(160,'regla 20cm.',6.5,40);
insert into articulos values(173,'compas metal',14,35);
insert into articulos values(234,'goma lapiz',0.95,200);
```

9. Recupere todos los artículos para ver cómo se almacenó los códigos
Ignora los códigos especificados ingresando el siguiente de la secuencia.
```sql
select * from articulos;

```
10. Cuando se ingresa un registro en "ventas", se debe:

- controlar que el código del artículo exista en "articulos" (lo hacemos con la restricción "foreign key" establecida en "ventas");

- controlar que exista stock, lo cual no puede controlarse con una restricción "foreign key" porque el campo "stock" no es clave primaria en la tabla "articulos"; cree un trigger. Si existe stock, debe disminuirse en "articulos".

Cree un trigger a nivel de fila sobre la tabla "ventas" para el evento se inserción. Cada vez que se realiza un "insert" sobre "ventas", el disparador se ejecuta. El disparador controla que la cantidad que se intenta vender sea menor o igual al stock del articulo y actualiza el campo "stock" de "articulos", restando al valor anterior la cantidad vendida. Si la cantidad supera el stock, debe producirse un error, revertirse la acción y mostrar un mensaje
```sql
create or replace trigger tr_control_ventas
before insert on ventas
for each row
declare
    v_stock number;
begin
    -- Verificar que el código del artículo exista en "articulos"
    select stock into v_stock from articulos where codigo = :new.codigo;
    -- Verificar que hay suficiente stock
    if v_stock < :new.cantidad then
        raise_application_error(-20001, 'No hay suficiente stock para realizar la venta.');
    else
        -- Actualizar el stock en "articulos"
        update articulos set stock = stock - :new.cantidad where codigo = :new.codigo;
    end if;
end;
/

```
11. Ingrese un registro en "ventas" cuyo código no exista en "articulos"
Aparece un mensaje de error, porque el código no existe. El trigger se ejecutó.
```sql
insert into ventas values(9999, 10, sysdate);

```
12. Verifique que no se ha agregado ningún registro en "ventas"
```sql
select * from ventas;

```
13. Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual haya suficiente stock
Note que el trigger se disparó, aparece el texto "tr_insertar_ventas activado".
```sql
insert into ventas values(160, 5, sysdate);

```
14. Verifique que el trigger se disparó consultando la tabla "articulos" (debe haberse disminuido el stock) y se agregó un registro en "ventas"
```sql
select * from articulos;
select * from ventas;

```
15. Ingrese un registro en "ventas" cuyo código exista en "articulos" y del cual NO haya suficiente stock
Aparece el mensaje mensaje de error 20001 y el texto que muestra que se disparó el trigger.
```sql
insert into ventas values(160, 50, sysdate);

```
16. Verifique que NO se ha disminuido el stock en "articulos" ni se ha agregado un registro en "ventas"
```sql
select * from articulos;
select * from ventas;

```
17. El comercio quiere que se realicen las ventas de lunes a viernes de 8 a 18 hs. Reemplace el trigger creado anteriormente "tr_insertar_ventas" para que No permita que se realicen ventas fuera de los días y horarios especificados y muestre un mensaje de error
```sql
create or replace trigger tr_control_ventas
before insert on ventas
for each row
declare
    v_stock number;
begin
    -- Verificar el día y horario permitido para las ventas
    if to_char(sysdate, 'fmDay') not in ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday') or
       to_char(sysdate, 'HH24:MI') not between '08:00' and '18:00' then
        raise_application_error(-20001, 'No se permiten ventas fuera del horario establecido (lunes a viernes de 8 a 18 hs).');
    end if;

    -- Verificar que el código del artículo exista en "articulos"
    select stock into v_stock from articulos where codigo = :new.codigo;
    
    -- Verificar que hay suficiente stock
    if v_stock < :new.cantidad then
        raise_application_error(-20002, 'No hay suficiente stock para realizar la venta.');
    else
        -- Actualizar el stock en "articulos"
        update articulos set stock = stock - :new.cantidad where codigo = :new.codigo;
    end if;
end;
/

```
18. Ingrese un registro en "ventas", un día y horario permitido, si es necesario, modifique la fecha y la hora del sistema
```sql
insert into ventas values(160, 2, sysdate);

```
19. Verifique que se ha agregado un registro en "ventas" y se ha disminuido el stock en "articulos"
```sql
select * from articulos;
select * from ventas;

```
20. Ingrese un registro en "ventas", un día permitido fuera del horario permitido (si es necesario, modifique la fecha y hora del sistema)
Se muestra un mensaje de error.
```sql
-- Modificar la fecha y hora del sistema para simular un día permitido
alter session set nls_date_format = 'DD-MON-YYYY HH24:MI:SS';
alter session set nls_date_language = 'AMERICAN';
execute dbms_system.set_time('20-JUN-2023 10:00:00');
-- Inserción de un registro en "ventas"
insert into ventas values(160, 3, sysdate);

```
21. Ingrese un registro en "ventas", un día sábado a las 15 hs.
```sql
-- Modificar la fecha y hora del sistema para simular un día sábado a las 15:00 hs
execute dbms_system.set_time('24-JUN-2023 15:00');

```
22. El comercio quiere que los registros de la tabla "articulos" puedan ser ingresados, modificados y/o eliminados únicamente los sábados de 8 a 12 hs. Cree un trigger "tr_articulos" que No permita que se realicen inserciones, actualizaciones ni eliminaciones en "articulos" fuera del horario especificado los días sábados, mostrando un mensaje de error. Recuerde que al ingresar un registro en "ventas", se actualiza el "stock" en "articulos"; el trigger debe permitir las actualizaciones del campo "stock" en "articulos" de lunes a viernes de 8 a 18 hs. (horario de ventas)
```sql
create or replace trigger tr_control_articulos
before insert or update or delete on articulos
for each row
declare
    v_day varchar2(20);
    v_hour varchar2(5);
begin
    -- Obtener el día y la hora actual
    v_day := to_char(sysdate, 'fmDay');
    v_hour := to_char(sysdate, 'HH24:MI');

    -- Verificar si es sábado
    if v_day = 'Saturday' then
        -- Verificar si está dentro del horario permitido
        if v_hour not between '08:00' and '12:00' then
            raise_application_error(-20003, 'No se permiten operaciones en la tabla "articulos" fuera del horario establecido (sábados de 8 a 12 hs).');
        end if;
    else
        raise_application_error(-20003, 'No se permiten operaciones en la tabla "articulos" en días que no sean sábados.');
    end if;
end;
/

```
23. Ingrese un nuevo artículo un sábado a las 9 AM
Note que se activan 2 triggers.
```sql
-- Modificar la fecha y hora del sistema para simular un sábado a las 9:00 AM
execute dbms_system.set_time('24-JUN-2023 09:00:00');
-- Inserción de un nuevo artículo
insert into articulos (descripcion, precio, stock) values ('Nuevo artículo', 10.99, 50);

```
24. Elimine un artículo, un sábado a las 16 hs.
Mensaje de error.
```sql
-- Modificar la fecha y hora del sistema para simular un sábado a las 16:00 hs
execute dbms_system.set_time('24-JUN-2023 16:00:00');
-- Intento de eliminación de un artículo
delete from articulos where codigo = 160;

```
25. Actualice el precio de un artículo, un domingo
```sql
-- Modificar la fecha y hora del sistema para simular un domingo
execute dbms_system.set_time('25-JUN-2023 10:00:00');
-- Actualización del precio de un artículo
update articulos set precio = 9.99 where codigo = 160;

```
26. Actualice el precio de un artículo, un lunes en horario de ventas
Mensaje de error.
```sql
-- Modificar la fecha y hora del sistema para simular un lunes en horario de ventas
execute dbms_system.set_time('26-JUN-2023 10:00:00');
-- Intento de actualización del precio de un artículo
update articulos set precio = 8.99 where codigo = 160;

```
27. Ingrese un registro en "ventas" que modifique el "stock" en "articulos", un martes entre las 8 y 18 hs.
Note que se activan 2 triggers.
```sql
-- Modificar la fecha y hora del sistema para simular un martes entre las 8:00 y las 18:00 hs
execute dbms_system.set_time('27-JUN-2023 14:00:00');
-- Inserción de un registro en "ventas"
insert into ventas values(160, 1, sysdate);

```
28. Consulte el diccionario "user_triggers" para ver cuántos trigger están asociados a "articulos" y a "ventas" (3 triggers)
```sql
-- Triggers asociados a "articulos"
select count(*) from user_triggers where table_name = 'ARTICULOS';

-- Triggers asociados a "ventas"
select count(*) from user_triggers where table_name = 'VENTAS';

```
29. Elimine el trigger asociado a "ventas"
```sql
-- Eliminar el trigger asociado a "ventas"
drop trigger tr_control_ventas;

```
30. Elimine las tablas "ventas" y "articulos"
```sql
-- Eliminar la tabla "ventas"
drop table ventas;

-- Eliminar la tabla "articulos"
drop table articulos;

```
31. Consulte el diccionario "user_triggers" para verificar que al eliminar la tabla "articulos" se han eliminado todos los triggers asociados a ella
```sql
select count(*) from user_triggers where table_name = 'ARTICULOS';

```