# 118. Usuarios (crear)

## Práctica de laboratorio

Sabemos que para crear un usuario debemos conectarnos a la base datos como administradores (por ejemplo "system").

Necesitamos crear un usuario "ana"; antes vamos a eliminarlo por si existe (luego veremos detenidamente cómo eliminar usuarios y explicaremos la siguiente sentencia):

```sql
drop user ana cascade;
```
Creamos un usuario denominado "ana" con la contraseña "anita":

```sql
create user ana identified by anita;
```

Aparece un mensaje indicando que el usuario "ana" ha sido creado.

Necesitamos crear un usuario denominado "juan"; antes vamos a eliminarlo por si existe:

```sql
drop user juan cascade;
```

Creamos el usuario "juan" con la contraseña "juancito", le asignamos un espacio de 100 mb en "system":

```sql
create user juan identified by juancito
default tablespace system
quota 100M on system;
```

Si intentamos crear un usuario que ya existe, Oracle muestra un mensaje de error indicando tal situación.

```sql
create user juan identified by juancito;
```

Mensaje de error.

Consultamos el diccionario "dba_users" y analizamos la información que nos muestra:

```sql
select username, password,default_tablespace,created from dba_users;
```

El resultado nos muestra el nombre de usuario, si tiene o no contraseña, el espacio asignado (tablespace) y fecha de creación.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop user ana cascade;

 -- Creamos un usuario denominado "ana" con la contraseña "anita":
create user ana identified by anita;

 -- Necesitamos crear un usuario denominado "juan"; antes vamos a eliminarlo por si existe:
drop user juan cascade;

 -- Creamos el usuario "juan" con la contraseña "juancito",
 -- le asignamos un espacio de 100 mb en "system":
create user juan identified by juancito
default tablespace system
quota 100M on system;

 -- Si intentamos crear un usuario que ya existe,
 -- Oracle muestra un mensaje de error indicando tal situación.
create user juan identified by juancito;

 -- Consultamos el diccionario "dba_users" y analizamos la información que nos muestra:
select username, password,default_tablespace,created from dba_users;
```

## Ejercicios propuestos

Una escuela necesita crear 2 usuarios diferentes en su base de datos. Uno denominado "director" y otro "profesor". Luego se les concederán diferentes permisos para retringir el acceso a los diferentes objetos. Conéctese como administrador (por ejemplo "system").

1. Primero eliminamos el usuario "director", porque si existe, aparecerá un mensaje de error:

```sql
drop user director cascade;
```

2. Cree un usuario "director", con contraseña "escuela" y 100M de espacio en "system"
```sql
CREATE USER director IDENTIFIED BY escuela DEFAULT TABLESPACE system QUOTA 100M ON system;

```
3. Elimine el usuario "profesor":

```sql
drop user profesor cascade;
```

4. Cree un usuario "profesor", con contraseña "maestro" y espacio en "system"
```sql
CREATE USER profesor IDENTIFIED BY maestro DEFAULT TABLESPACE system;

```
5. Consulte el diccionario "dba_users" y analice la información que nos muestra
```sql
SELECT * FROM dba_users;

```