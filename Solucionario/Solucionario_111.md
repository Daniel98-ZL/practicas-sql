# 111. Disparador (old y new)

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las actualizaciones del precio de los libros almacenando en la tabla "control" el nombre del usuario, la fecha, el precio anterior y el nuevo.

Eliminamos las tablas:

```sql
drop table control;
drop table libros;
```
Creamos las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    codigo number(6),
    precioanterior number(6,2),
    precionuevo number(6,2)
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

Creamos un trigger a nivel de fila que se dispara "antes" que se ejecute un "update" sobre el campo "precio" de la tabla "libros". En el cuerpo del disparador se debe ingresar en la tabla "control", el nombre del usuario que realizó la actualización, la fecha, el código del libro que ha sido modificado, el precio anterior y el nuevo:

```sql
create or replace trigger tr_actualizar_precio_libros
before update of precio
on libros
    for each row
begin
   insert into control values(user,sysdate,:new.codigo,:old.precio,:new.precio);
end tr_actualizar_precio_libros;
/
```

Cuando el trigger se dispare, antes de ingresar los valores a la tabla, almacenará en "control", además del nombre del usuario y la fecha, el precio anterior del libro y el nuevo valor.

Actualizamos el precio del libro con código 100:

```sql
update libros set precio=30 where codigo=100;
```

Veamos lo que se almacenó en "control" al dispararse el trigger:

```sql
select *from control;
```

Los campos "precioanterior" y "precionuevo" de la tabla "control" almacenaron los valores de ":old.precio" y ":new.precio" respectivamente.

Actualizamos varios registros:

 update libros set precio=precio+precio*0.1 where editorial='Planeta';
Veamos lo que se almacenó en "control" al dispararse el trigger:

```sql
select *from control;
```

Los campos "precioanterior" y "precionuevo" de la tabla "control" almacenaron los valores de ":old.precio" y ":new.precio" respectivamente de cada registro afectado por la actualización.

Modificamos la editorial de un libro:

```sql
update libros set editorial='Sudamericana' where editorial='Planeta';
```

El trigger no se disparó, pues fue definido para actualizaciones del campo "precio" unicamente.

Verifiquémoslo:

```sql
select *from control;
```

Vamos a reemplazar el trigger anteriormente creado. Ahora el disparador "tr_actualizar_precio_libros" debe controlar el precio que se está actualizando, si supera los 50 pesos, se debe redondear tal valor a entero hacia abajo (empleando "floor"), es decir, se modifica el valor ingresado accediendo a ":new.precio" asignándole otro valor:

```sql
create or replace trigger tr_actualizar_precio_libros
before update of precio
on libros
    for each row
begin
    if (:new.precio>50) then
        :new.precio:=floor(:new.precio);
    end if;
       insert into control values(user,sysdate,:new.codigo,:old.precio,:new.precio);
    end tr_actualizar_precio_libros;
/
```

Vaciamos la tabla "control":

```sql
truncate table control;
```

Actualizamos el precio del libro con código 100:

```sql
update libros set precio=54.99 where codigo=100;
```

Veamos cómo se actualizó tal libro en "libros":

```sql
select *from libros where codigo=100;
```

El nuevo precio actualizado se redondeó a 54.

Veamos lo que se almacenó en "control" al dispararse el trigger:

```sql
select *from control;
```

Los campos "precioanterior" y "precionuevo" de la tabla "control" almacenaron los valores de ":old.precio" y ":new.precio" respectivamente.

Truncamos la tabla "control" nuevamente:

```sql
truncate table control;
```

Creamos un disparador para múltiples eventos, que se dispare al ejecutar "insert", "update" y "delete" sobre "libros". En el cuerpo del trigger se realiza la siguiente acción: se almacena el nombre del usuario, la fecha y los antiguos y viejos valores de "precio":

```sql
create or replace trigger tr_libros
before insert or update or delete
on libros
    for each row
begin
   insert into control values(user,sysdate,:old.codigo,:old.precio,:new.precio);
end tr_libros;
/
```

Ingresamos un registro:

```sql
insert into libros values (150,'El gato con botas','Anonimo','Emece',21);
```

Veamos lo que se almacenó en "control":

```sql
select *from control;
```

Resultado:

```sh
USUARIO      FECHA       CODIGO      PRECIOANTERIOR      PRECIONUEVO
-----------------------------------------------------------------------------------
SYSTEM       20/03/08    21                     
```

La sentencia disparadora fue una inserción, por lo tanto, los campos ":old.codigo" y ":old.precio" contienen "null", así que en "codigo" y en "precioanterior" se almacena "null"; el único campo con valor diferente de "null" es "precionuevo" correspondiente al valor de ":new.precio".

Actualizamos el campo "precio" de un libro:

```sql
update libros set precio=12 where codigo=103;
```

Veamos lo que se almacenó en "control":

```sql
select *from control;
```

Resultado:

```sh
USUARIO     FECHA       CODIGO      PRECIOANTERIOR     PRECIONUEVO
-----------------------------------------------------------------------------------
SYSTEM      20/03/08    103         28                 12
```

Analicemos: actualizamos el precio, por lo tanto, ninguno de los campos consultados contiene "null".

Actualizamos un campo diferente de "precio" de un libro:

 update libros set autor='J.L.Borges' where autor='Borges';
Veamos lo que se almacenó en "control":

```sql
select *from control;
```

Resultado:

```sh
USUARIO     FECHA       CODIGO      PRECIOANTERIOR      PRECIONUEVO
-----------------------------------------------------------------------------------
SYSTEM      20/03/08    103         12                  12
```

Actualizamos el autor, por lo tanto, los campos ":old.precio" y ":new.precio" son iguales.

Eliminamos un registro de "libros":

```sql
delete from libros where codigo=100;
```

Veamos lo que se almacenó en "control":

```sql
select *from control;
```

Resultado:

```sh
USUARIO     FECHA       CODIGO       PRECIOANTERIOR       PRECIONUEVO
-----------------------------------------------------------------------------------
SYSTEM      20/03/08    100          54
```

Analicemos: la sentencia que disparó el trigger fue un "delete", por lo tanto, el campo ":new.precio" contiene "null".

Veamos qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_LIBROS';
```

## Ejercicios propuestos

Una librería almacena los datos de sus libros en una tabla denominada "libros" y en otra denominada "ofertas", almacena los códigos y precios de los libros cuyo precio es inferior a $50.

1. Elimine las tablas:

```sql
drop table libros;
drop table ofertas;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar(20),
    precio number(6,2)
);

create table ofertas(
    codigo number(6),
    precio number(6,2)
);
```

3. Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

4. Cree un trigger a nivel de fila que se dispare al ingresar un registro en "libros"; si alguno de los libros ingresados tiene un precio menor o igual a $30 debe ingresarlo en "ofertas"
```sql
create or replace trigger tr_ofertas
after insert on libros
for each row
begin
    if :new.precio <= 30 then
        insert into ofertas values (:new.codigo, :new.precio);
    end if;
end;
/

```
5. Ingrese un libro en "libros" cuyo precio sea inferior a $30
```sql
insert into libros values(200,'Libro oferta','Autor oferta','Editorial oferta',25);

```
6. Verifique que el trigger se disparó consultando "ofertas"
```sql
select * from ofertas;

```
7. Ingrese un libro en "libros" cuyo precio supere los $30
```sql
insert into libros values(300,'Libro normal','Autor normal','Editorial normal',40);

```
8. Verifique que no se ingresó ningún registro en "ofertas"
```sql
select * from ofertas;

```
9. Cree un trigger a nivel de fila que se dispare al modificar el precio de un libro. Si tal libro existe en "ofertas" y su nuevo precio ahora es superior a $30, debe eliminarse de "ofertas"; si tal libro no existe en "ofertas" y su nuevo precio ahora es inferior a $30, debe agregarse a "ofertas"
```sql
create or replace trigger tr_modificar_ofertas
after update on libros
for each row
begin
    if :new.precio > 30 and exists (select 1 from ofertas where codigo = :new.codigo) then
        delete from ofertas where codigo = :new.codigo;
    elsif :new.precio <= 30 and not exists (select 1 from ofertas where codigo = :new.codigo) then
        insert into ofertas values (:new.codigo, :new.precio);
    end if;
end;
/

```
10. Aumente a más de $30 el precio de un libro que se encuentra en "ofertas"
```sql
update libros set precio = 35 where codigo = 200;

```
11. Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;

```
12. Disminuya a menos de $31 el precio de un libro que no se encuentra en "ofertas"
```sql
update libros set precio = 29 where codigo = 300;

```
13. Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;

```
14. Aumente el precio de un libro que no se encuentra en "ofertas"
```sql
update libros set precio = 50 where codigo = 120;

```
15. Verifique que el trigger se disparó pero no se modificó "ofertas"
```sql
select * from libros;
select * from ofertas;

```
16. Cree un trigger a nivel de fila que se dispare al borrar un registro en "libros"; si alguno de los libros eliminados está en "ofertas", también debe eliminarse de dicha tabla.
```sql
create or replace trigger tr_borrar_ofertas
after delete on libros
for each row
begin
    delete from ofertas where codigo = :old.codigo;
end;
/

```
17. Elimine un libro en "libros" que esté en "ofertas"
```sql
delete from libros where codigo = 200;

```
18. Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;

```
19. Elimine un libro en "libros" que No esté en "ofertas"
```sql
delete from libros where codigo = 300;

```
20. Verifique que el trigger se disparó consultando "libros" y "ofertas"
```sql
select * from libros;
select * from ofertas;

```
21. Cree una tabla llamada "control" que almacene el código, la fecha y el precio de un libro, antes elimínela por si existe
```sql
drop table control;
create table control(
    codigo number(6),
    fecha date,
    precio_anterior number(6,2)
);

```
22. Cree un disparador que se dispare cada vez que se actualice el precio de un libro; el trigger debe ingresar en la tabla "control", el código del libro cuyo precio se actualizó, la fecha y el precio anterior.
```sql
create or replace trigger tr_control
after update on libros
for each row
begin
    insert into control values (:new.codigo, sysdate, :old.precio);
end;
/

```
23. Actualice el precio de un libro
```sql
update libros set precio = 30 where codigo = 100;

```
24. Controle que el precio se ha modificado en "libros" y que se agregó un registro en "control"
```sql
select * from libros;
select * from control;

```
25. Modifique nuevamente el precio del libro cambiado en el punto 11
```sql
update libros set precio = 35 where codigo = 100;

```
26. Controle que el precio se ha modificado en "libros" y que se agregó un nuevo registro en "control"
```sql
select * from libros;
select * from control;

```
27. Modifique el precio de varios libros en una sola sentencia que incluya al modificado anteriormente
```sql
update libros set precio = 40 where codigo in (100, 103, 105);

```
28. Controle que el precio se ha modificado en "libros" y que se agregó un nuevo registro en "control"
```sql
select * from libros;
select * from control;

```
29. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
select * from user_triggers where trigger_name = 'TR_CONTROL';

```