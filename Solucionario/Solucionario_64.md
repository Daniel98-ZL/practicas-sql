# 64. Unión

## Practica de laboratorio

Una academia de enseñanza de idiomas da clases de inglés y frances; almacena los datos de los alumnos que estudian inglés en una tabla llamada "ingles" y los que están inscriptos en "francés" en una tabla denominada "frances".

Eliminamos las tablas:

```sql
drop table ingles;
drop table frances;
```

Creamos las tablas:

```sql
create table ingles(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table frances(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);
 ```

Ingresamos algunos registros:

```sql
insert into ingles values('20111222','Ana Acosta','Avellaneda 111');
insert into ingles values('21222333','Betina Bustos','Bulnes 222');
insert into ingles values('22333444','Carlos Caseros','Colon 333');
insert into ingles values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into ingles values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into frances values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('30111222','Fabiana Fuentes','Famatina 666');
insert into frances values('30222333','Gaston Gonzalez','Guemes 777');
```

La academia necesita el nombre y domicilio de todos los alumnos, de todos los cursos para enviarles una tarjeta de invitación para un festejo el día del alumno.

Empleamos el operador "union" para obtener dicha información de ambas tablas:

```sql
select nombre, domicilio from ingles
union
select nombre, domicilio from frances;
```

Note que existen dos alumnos (Daniel Duarte y Estela Esper) que cursan ambos idiomas, están presentes en la tabla "ingles" y "frances"; tales registros aparecen una sola vez en el resultado de "union". Si queremos que los registros duplicados aparezcan, debemos emplear "all":

```sql
select nombre, domicilio from ingles
union all
select nombre, domicilio from frances;
```

Ordenamos por nombre:

```sql
select nombre, domicilio from ingles
union all
select nombre, domicilio from frances
order by nombre;
```

Podemos agregar una columna extra a la consulta con el encabezado "curso" en la que aparezca el literal "inglés" o "francés" según si la persona cursa uno u otro idioma:

```sql
select nombre, domicilio, 'ingles' as curso from ingles
union
select nombre, domicilio,'frances' from frances
order by curso;
```

Recuerde que los encabezados de los campos son los que se especifican en el primer "select". Si queremos que el nombre tenga un encabezado "alumno" debemos especificar un alias en la primer consulta. Si ordenamos por un campo que tiene un alias, debemos especificar el alias no el nombre del campo. En la siguiente consulta realizamos una unión, colocamos un alias al campo "nombre" y ordenamos el resultado por tal alias:

```sql
select nombre as alumno, domicilio from ingles
union
select nombre, domicilio from frances
order by alumno;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table ingles;
drop table frances;

create table ingles(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

create table frances(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);

insert into ingles values('20111222','Ana Acosta','Avellaneda 111');
insert into ingles values('21222333','Betina Bustos','Bulnes 222');
insert into ingles values('22333444','Carlos Caseros','Colon 333');
insert into ingles values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into ingles values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('23444555','Daniel Duarte','Duarte Quiros 444');
insert into frances values('24555666','Estela Esper','Esmeralda 555');
insert into frances values('30111222','Fabiana Fuentes','Famatina 666');
insert into frances values('30222333','Gaston Gonzalez','Guemes 777');

select nombre, domicilio from ingles
union
select nombre, domicilio from frances;

select nombre, domicilio from ingles
union all
select nombre, domicilio from frances;

select nombre, domicilio from ingles
union all
select nombre, domicilio from frances
order by nombre;

select nombre, domicilio, 'ingles' as curso from ingles
union
select nombre, domicilio,'frances' from frances
order by curso;

select nombre as alumno, domicilio from ingles
union
select nombre, domicilio from frances
order by alumno;
```

## Ejercicios propuestos

Una clínica almacena los datos de los médicos en una tabla llamada "medicos" y los datos de los pacientes en otra denominada "pacientes".

1. Eliminamos ambas tablas:

```sql
drop table medicos;
drop table pacientes;
```

2. Creamos las tablas:

```sql
create table medicos(
    legajo number(3),
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    especialidad varchar2(30),
    primary key(legajo)
);

create table pacientes(
    documento varchar2(8) not null,
    nombre varchar2(30),
    domicilio varchar2(30),
    obrasocial varchar2(20),
    primary key(documento)
);
```

3. Ingresamos algunos registros:

```sql
insert into medicos values(1,'20111222','Ana Acosta','Avellaneda 111','clinica');
insert into medicos values(2,'21222333','Betina Bustos','Bulnes 222','clinica');
insert into medicos values(3,'22333444','Carlos Caseros','Colon 333','pediatria');
insert into medicos values(4,'23444555','Daniel Duarte','Duarte Quiros 444','oculista');
insert into medicos values(5,'24555666','Estela Esper','Esmeralda 555','alergia');
insert into pacientes values('24555666','Estela Esper','Esmeralda 555','IPAM');
insert into pacientes values('23444555','Daniel Duarte','Duarte Quiros 444','OSDOP');
insert into pacientes values('30111222','Fabiana Fuentes','Famatina 666','PAMI');
insert into pacientes values('31222333','Gaston Gonzalez','Guemes 777','PAMI');
```

4. La clínica necesita el nombre y domicilio de médicos y pacientes para enviarles una tarjeta de invitación a la inauguración de un nuevo establecimiento. Emplee el operador "union" para obtener dicha información de ambas tablas (7 registros)
Note que existen dos médicos que también están presentes en la tabla "pacientes"; tales registros aparecen una sola vez en el resultado de "union".
```sql
SELECT nombre, domicilio
FROM medicos
UNION
SELECT nombre, domicilio
FROM pacientes;

```
5. Realice la misma consulta anterior pero esta vez, incluya los registros duplicados. Emplee "union all" (9 registros)
```sql
SELECT nombre, domicilio
FROM medicos
UNION ALL
SELECT nombre, domicilio
FROM pacientes;

```
6. Realice la misma consulta anterior y esta vez ordene el resultado por nombre (9 registros)
```sql
SELECT nombre, domicilio
FROM medicos
UNION ALL
SELECT nombre, domicilio
FROM pacientes
ORDER BY nombre;

```
7. Agregue una columna extra a la consulta con el encabezado "condicion" en la que aparezca el literal "médico" o "paciente" según si la persona es uno u otro (9 registros)
```sql
SELECT nombre, domicilio, 'médico' AS condicion
FROM medicos
UNION ALL
SELECT nombre, domicilio, 'paciente' AS condicion
FROM pacientes
ORDER BY nombre;

```