# 30. Agrupar registros (group by)

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30) default 'Desconocido',
    editorial varchar2(15),
    precio number(5,2),
    cantidad number(3),
    primary key(codigo)
);
```

Ingresamos algunos registros:

```sql
insert into libros
values(100,'El aleph','Borges','Planeta',15,null);

insert into libros
values(234,'Martin Fierro','Jose Hernandez','Emece',22.20,200);

insert into libros
values(354,'Antologia poetica',default,'Planeta',null,150);

insert into libros
values(478,'Aprenda PHP','Mario Molina','Emece',18.20,null);

insert into libros
values(512,'Cervantes y el quijote','Bioy Casares- J.L. Borges','Paidos',28,100);

insert into libros
values(643,'Manual de PHP', default, 'Siglo XXI',31.80,120);

insert into libros
values(646,'Harry Potter y la piedra filosofal','J.K. Rowling',default,45.00,90);

insert into libros
values(753,'Harry Potter y la camara secreta','J.K. Rowling','Emece',null,100);

insert into libros
values(889,'Alicia en el pais de las maravillas','Lewis Carroll','Paidos',22.50,200);

insert into libros
values(893,'PHP de la A a la Z',null,null,55.90,0);
```

Queremos saber la cantidad de libros de cada editorial, utilizando la cláusula "group by":

```sql
select editorial, count(*)
from libros
group by editorial;
```

Aparece el siguiente resultado:

```sh
EDITORIAL    COUNT(*)
--------------------------
Paidos       22
Planeta      2
Emece        3
Siglo XXI    1
```

El resultado muestra los nombres de las editoriales y la cantidad de registros para cada valor del campo. Note que los valores nulos se procesan como otro grupo.

Obtenemos la cantidad libros con precio no nulo de cada editorial:

```sql
select editorial, count(precio)
from libros
group by editorial;
```

La salida es la siguiente:

```sh
EDITORIAL    COUNT(PRECIO)
------------------------------
Paidos       22
Planeta      1
Emece        2
Siglo XXI    1
```

Aparecen los nombres de las editoriales y la cantidad de registros de cada una, sin contar los que tienen precio nulo.

Para conocer el total de libros agrupados por editorial, tipeamos:

```sql
select editorial, sum(cantidad)
from libros
group by editorial;
```

Obtenemos el máximo y mínimo valor de los libros agrupados por editorial, en una sola sentencia:

```sql
select editorial, max(precio) as mayor, min(precio) as menor
from libros
group by editorial;
```

Calculamos el promedio del valor de los libros agrupados por editorial:

```sql
select editorial, avg(precio)
from libros
group by editorial;
```

Es posible limitar la consulta con "where". Vamos a contar y agrupar por editorial considerando solamente los libros cuyo precio es menor a 30 pesos:

```sql
select editorial, count(*)
from libros
where precio<30
group by editorial;
```

Note que las editoriales que no tienen libros que cumplan la condición (sus precios superan los 30 pesos), no aparecen en la salida.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30) default 'Desconocido',
    editorial varchar2(15),
    precio number(5,2),
    cantidad number(3),
    primary key(codigo)
);

insert into libros
values(100,'El aleph','Borges','Planeta',15,null);

insert into libros
values(234,'Martin Fierro','Jose Hernandez','Emece',22.20,200);

insert into libros
values(354,'Antologia poetica',default,'Planeta',null,150);

insert into libros
values(478,'Aprenda PHP','Mario Molina','Emece',18.20,null);

insert into libros
values(512,'Cervantes y el quijote','Bioy Casares- J.L. Borges','Paidos',28,100);

insert into libros
values(643,'Manual de PHP', default, 'Siglo XXI',31.80,120);

insert into libros
values(646,'Harry Potter y la piedra filosofal','J.K. Rowling',default,45.00,90);

insert into libros
values(753,'Harry Potter y la camara secreta','J.K. Rowling','Emece',null,100);

insert into libros
values(889,'Alicia en el pais de las maravillas','Lewis Carroll','Paidos',22.50,200);

insert into libros
values(893,'PHP de la A a la Z',null,null,55.90,0);

select editorial, count(*)
from libros
group by editorial;

select editorial, count(precio)
from libros
group by editorial;

select editorial, sum(cantidad)
from libros
group by editorial;

select editorial, max(precio) as mayor, min(precio) as menor
from libros
group by editorial;

select editorial, avg(precio)
from libros
group by editorial;

select editorial, count(*)
from libros
where precio<30
group by editorial;
```

## Ejercicios propuestos

## Ejercicio 01

Un comercio que tiene un stand en una feria registra en una tabla llamada "visitantes" algunos datos de las personas que visitan o compran en su stand para luego enviarle publicidad de sus productos.

1. Elimine la tabla "visitantes" y créela con la siguiente estructura:

```sql
drop table visitantes;
create table visitantes(
    nombre varchar2(30),
    edad number(2),
    sexo char(1) default 'f',
    domicilio varchar2(30),
    ciudad varchar2(20) default 'Cordoba',
    telefono varchar2(11),
    mail varchar2(30) default 'no tiene',
    montocompra number(6,2)
);
```

2. Ingrese algunos registros:

```sql
insert into visitantes
values ('Susana Molina',35,default,'Colon 123',default,null,null,59.80);

insert into visitantes
values ('Marcos Torres',29,'m',default,'Carlos Paz',default,'<marcostorres@hotmail.com>',150.50);

insert into visitantes
values ('Mariana Juarez',45,default,default,'Carlos Paz',null,default,23.90);

insert into visitantes (nombre, edad,sexo,telefono, mail)
values ('Fabian Perez',36,'m','4556677','<fabianperez@xaxamail.com>');

insert into visitantes (nombre, ciudad, montocompra)
values ('Alejandra Gonzalez','La Falda',280.50);

insert into visitantes (nombre, edad,sexo, ciudad, mail,montocompra)
values ('Gaston Perez',29,'m','Carlos Paz','<gastonperez1@gmail.com>',95.40);

insert into visitantes
values ('Liliana Torres',40,default,'Sarmiento 876',default,default,default,85);

insert into visitantes
values ('Gabriela Duarte',21,null,null,'Rio Tercero',default,'<gabrielaltorres@hotmail.com>',321.50);
```

3. Queremos saber la cantidad de visitantes de cada ciudad utilizando la cláusula "group by" (4 filas devueltas)
```sql
SELECT ciudad, COUNT(*) AS cantidad_visitantes
FROM visitantes
GROUP BY ciudad;

```
4. Queremos la cantidad visitantes con teléfono no nulo, de cada ciudad (4 filas devueltas)
```sql
SELECT ciudad, COUNT(telefono) AS cantidad_visitantes_con_telefono
FROM visitantes
WHERE telefono IS NOT NULL
GROUP BY ciudad;

```
5. Necesitamos el total del monto de las compras agrupadas por sexo (3 filas)
Note que los registros con valor nulo en el campo "sexo" se procesan como un grupo diferente.
```sql
SELECT sexo, SUM(montocompra) AS total_compras
FROM visitantes
GROUP BY sexo;

```
6. Se necesita saber el máximo y mínimo valor de compra agrupados por sexo y ciudad (6 filas)
```sql
SELECT sexo, ciudad, MAX(montocompra) AS max_valor_compra, MIN(montocompra) AS min_valor_compra
FROM visitantes
GROUP BY sexo, ciudad;

```
7. Calcule el promedio del valor de compra agrupados por ciudad (4 filas)
```sql
SELECT ciudad, AVG(montocompra) AS promedio_valor_compra
FROM visitantes
GROUP BY ciudad;

```
8. Cuente y agrupe por ciudad sin tener en cuenta los visitantes que no tienen mail (3 filas)
```sql
SELECT ciudad, COUNT(*) AS cantidad_visitantes
FROM visitantes
WHERE mail IS NOT NULL
GROUP BY ciudad;

```
## Ejercicio 02

Una empresa almacena los datos de sus empleados en una tabla "empleados".

1. Elimine la tabla y luego créela con la estructura definida a continuación:

```sql
drop table empleados;
create table empleados(
    nombre varchar2(30),
    documento char(8),
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2),
    cantidadhijos number(2),
    fechaingreso date,
    primary key(documento)
);
```

2. Ingrese algunos registros:

```sql
insert into empleados
values('Juan Perez','22333444','Colon 123','Gerencia',5000,2,'10/05/1980');

insert into empleados
values('Ana Acosta','23444555','Caseros 987','Secretaria',2000,0,'12/10/1980');

insert into empleados
values('Lucas Duarte','25666777','Sucre 235','Sistemas',4000,1,'25/05/1985');

insert into empleados
values('Pamela Gonzalez','26777888','Sarmiento 873','Secretaria',2200,3,'25/06/1990');

insert into empleados
values('Marcos Juarez','30000111','Rivadavia 801','Contaduria',3000,0,'01/05/1996');

insert into empleados
values('Yolanda Perez','35111222','Colon 180','Administracion',3200,1,'01/05/1996');

insert into empleados
values('Rodolfo Perez','35555888','Coronel Olmedo 588','Sistemas',4000,3,'01/05/1996');

insert into empleados
values('Martina Rodriguez','30141414','Sarmiento 1234','Administracion',3800,4,'01/09/2000');

insert into empleados
values('Andres Costa','28444555',default,'Secretaria',null,null,null);
```

3. Cuente la cantidad de empleados agrupados por sección (5 filas)
```sql
SELECT seccion, COUNT(*) AS cantidad_empleados
FROM empleados
GROUP BY seccion;

```
4. Calcule el promedio de hijos por sección (5 filas)
```sql
SELECT seccion, AVG(cantidadhijos) AS promedio_hijos
FROM empleados
GROUP BY seccion;

```
5. Cuente la cantidad de empleados agrupados por año de ingreso (6 filas)
```sql
SELECT EXTRACT(YEAR FROM fechaingreso) AS anio_ingreso, COUNT(*) AS cantidad_empleados
FROM empleados
GROUP BY EXTRACT(YEAR FROM fechaingreso);

```
6. Calcule el promedio de sueldo por sección de los empleados con hijos (4 filas)
```sql
SELECT seccion, AVG(sueldo) AS promedio_sueldo
FROM empleados
WHERE cantidadhijos > 0
GROUP BY seccion;

```