# 60. Restricciones foreign key deshabilitar y validar

## Practica de laboratorio

Una librería almacena la información de sus libros para la venta en dos tablas, "libros" y "editoriales".

Eliminamos ambas tablas:

```sql
drop table libros;
drop table editoriales;
```

Creamos las tablas:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    codigoeditorial number(3),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20),
    primary key (codigo)
);
```

Ingresamos algunos registros:

```sql
insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Paidos');
insert into libros values(1,'Uno',1);
insert into libros values(2,'El aleph',2);
insert into libros values(3,'Aprenda PHP',5);
```

Agregamos una restricción "foreign key" a la tabla "libros" para evitar que se ingresen códigos de editoriales inexistentes en "editoriales". Incluimos la opción "novalidate" para evitar la comprobación de la restricción en los datos existentes (note que hay un libro que tiene un código de editorial inválido):

```sql
alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo) novalidate;
```

La deshabilitación de la comprobación de la restricción no afecta a los siguientes ingresos, modificaciones y actualizaciones. Para poder ingresar, modificar o eliminar datos a una tabla sin que Oracle compruebe la restricción debemos deshabilitarla:

```sql
alter table libros
disable novalidate
constraint FK_LIBROS_CODIGOEDITORIAL;
```

Veamos si la restricción está habilitada o no:

```sql
select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';
```

En la columna "status" de la restricción "foreign key" aparece "Disabled" y en "Validated" muestra "not validated".

Ahora podemos ingresar un registro en "libros" con código de editorial inválido:

```sql
insert into libros values(4,'Ilusiones',6);
```

Habilitamos la restricción:

```sql
alter table libros
enable novalidate constraint FK_libros_codigoeditorial;
```

Veamos si la restricción está habilitada o no y si valida los datos existentes:

```sql
select constraint_name, constraint_type, status, validated
from user_constraints where table_name='LIBROS';
```

En la columna "status" aparece "Enabled" y en "Validated" "not validate".

Intentamosalterar la restricción para que se validen los datos existentes:

```sql
alter table libros
enable validate constraint FK_libros_codigoeditorial;
```

Oracle mostrará un mensaje indicando que no se pueden validar los datos existentes porque existen valores inválidos.

Truncamos la tabla yalteramos la restricción:

```sql
truncate table libros;

alter table libros
enable validate constraint FK_libros_codigoeditorial;
```

Solicitamos información sobre la restricción:

```sql
select constraint_name, constraint_type, status, validated
from user_constraints where table_name='LIBROS';
```

En la columna "status" aparece "Enabled" y en "Validated" "Validate".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    codigoeditorial number(3),
    primary key (codigo)
);

create table editoriales(
    codigo number(3),
    nombre varchar2(20),
    primary key (codigo)
);

insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Paidos');

insert into libros values(1,'Uno',1);
insert into libros values(2,'El aleph',2);
insert into libros values(3,'Aprenda PHP',5);

alter table libros
add constraint FK_libros_codigoeditorial
foreign key (codigoeditorial)
references editoriales(codigo) novalidate;

alter table libros
disable novalidate
constraint FK_LIBROS_CODIGOEDITORIAL;

select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';

insert into libros values(4,'Ilusiones',6);

alter table libros
enable novalidate constraint FK_libros_codigoeditorial;

select constraint_name, constraint_type, status, validated
from user_constraints where table_name='LIBROS';

alter table libros
enable validate constraint FK_libros_codigoeditorial;

truncate table libros;

alter table libros
enable validate constraint FK_libros_codigoeditorial;

select constraint_name, constraint_type, status, validated
from user_constraints where table_name='LIBROS';
```

## Ejercicios propuestos

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

1. Elimine las tablas "clientes" y "provincias":

```sql
drop table clientes;
drop table provincias;
```

2. Créelas con las siguientes estructuras:

```sql
create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2),
    primary key(codigo)
);

create table provincias(
    codigo number(2),
    nombre varchar2(20),
    primary key (codigo)
);
```

3. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Misiones');
insert into provincias values(4,'Rio Negro');
insert into clientes values(100,'Perez Juan','San Martin 123','Carlos Paz',1);
insert into clientes values(101,'Moreno Marcos','Colon 234','Rosario',2);
insert into clientes values(102,'Garcia Juan','Sucre 345','Cordoba',1);
insert into clientes values(103,'Lopez Susana','Caseros 998','Posadas',3);
insert into clientes values(104,'Marcelo Moreno','Peru 876','Viedma',4);
insert into clientes values(105,'Lopez Sergio','Avellaneda 333','La Plata',5);
```

4. Intente agregar una restricción "foreign key" para que los códigos de provincia de "clientes" existan en "provincias" sin especificar la opción de comprobación de datos
No se puede porque al no especificar opción para la comprobación de datos, por defecto es "validate" y hay un registro que no cumple con la restricción.
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo);

```
5. Agregue la restricción anterior pero deshabilitando la comprobación de datos existentes
```sql
ALTER TABLE clientes ADD CONSTRAINT fk_codigoprovincia FOREIGN KEY (codigoprovincia) REFERENCES provincias(codigo) DISABLE NOVALIDATE;

```
6. Vea las restricciones de "clientes"
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES';

```
7. Deshabilite la restricción "foreign key" de "clientes"
```sql
ALTER TABLE clientes DISABLE CONSTRAINT fk_codigoprovincia;

```
8. Vea las restricciones de "clientes"
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES';

```
9. Agregue un registro que no cumpla la restricción "foreign key"
Se permite porque la restricción está deshabilitada.
```sql
INSERT INTO clientes VALUES (106, 'Gonzalez Laura', 'San Martin 567', 'Cordoba', 8);

```
10. Modifique el código de provincia del cliente código 104 por 9
Oracle lo permite porque la restricción está deshabilitada.
```sql
UPDATE clientes SET codigoprovincia = 9 WHERE codigo = 104;

```
11. Habilite la restricción "foreign key"
```sql
ALTER TABLE clientes ENABLE CONSTRAINT fk_codigoprovincia;

```
12. Intente modificar un código de provincia existente por uno inexistente.
```sql
UPDATE clientes SET codigoprovincia = 7 WHERE codigo = 104;

```
13. Intentealterar la restricción "foreign key" para que valide los datos existentes
```sql
ALTER TABLE clientes ENABLE VALIDATE CONSTRAINT fk_codigoprovincia;

```
14. Elimine los registros que no cumplen la restricción y modifique la restricción a "enable" y "validate"
```sql
DELETE FROM clientes WHERE codigoprovincia = 8;
ALTER TABLE clientes ENABLE VALIDATE CONSTRAINT fk_codigoprovincia;

```
15. Obtenga información sobre la restricción "foreign key" de "clientes"
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'CLIENTES' AND constraint_name = 'FK_CODIGOPROVINCIA';

```
