# 16. Ingresar algunos campos
## Practica de laboratorio

### Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.

#### 1. Eliminamos la tabla:

```sql
drop table libros;
```

#### 2. Creamos la tabla:

```sql
create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(15)
);
```

#### 3. Si ingresamos valores para todos los campos, podemos omitir la lista de campos:

```sql
insert into libros values (1,'Uno','Richard Bach','Planeta');
```

#### 4. Podemos ingresar valores para algunos de los campos:

```sql
insert into libros (codigo, titulo, autor) values (2,'El aleph','Borges');
```

#### 5. Veamos cómo Oracle almacenó los registros:

```sql
select *from libros;
```

En el campo "editorial", para el cual no ingresamos valor, se almacenó "null".

#### 6. No podemos omitir el valor para un campo declarado "not null", como el campo "codigo":

```sql
insert into libros (titulo, autor,editorial)
values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');
```

Aparece un mensaje y la inserción no se realiza.

#### 7. Veamos cómo Oracle almacenó los registros:

```sql
select *from libros;
```

#### 8. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(15)
);

insert into libros
values (1,'Uno','Richard Bach','Planeta');

insert into libros (codigo, titulo, autor)
values (2,'El aleph','Borges');

select *from libros;

insert into libros (titulo, autor,editorial)
values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');

select *from libros;
```

## Ejercicios propuestos

### Primer problema:
#### Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".

##### 1. Elimine la tabla "cuentas":

```sql
drop table cuentas;
```

##### 2. Cree la tabla :

```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);
```

##### 3. Ingrese un registro con valores para todos sus campos, omitiendo la lista de campos.
```sql
insert into cuentas(numero, documento,nombre,saldo)
values (1234567890,'87654321','Daniel Zapana',1234.4);
```
##### 4. Ingrese un registro omitiendo algún campo que admita valores nulos.
```sql
insert into cuentas(numero, documento,nombre,saldo)
values (1234567891,'87654321','',1334.4);
```
##### 5. Verifique que en tal campo se almacenó "null"
```sql
SELECT * FROM cuentas;
```
Salida del Script

```sh
1234567890	87654321	Daniel Zapana	1234,4
1234567891	87654321		1334,4
```
##### 6. Intente ingresar un registro listando 3 campos y colocando 4 valores. Un mensaje indica que hay demasiados valores.
```sql
insert into cuentas(numero, documento,nombre)
values (1234567892,'87654321','David Sandoval',1334.6);
```
Salida del Script

Nos da un error de ORA-00913: demasiados valores.
```sh
Error que empieza en la línea: 42 del comando -
insert into cuentas(numero, documento,nombre)
values (1234567892,'87654321','David Sandoval',1334.6)
Error en la línea de comandos : 42 Columna : 13
Informe de error -
Error SQL: ORA-00913: demasiados valores
00913. 00000 -  "too many values"
*Cause:    
*Action:
```
##### 7. Intente ingresar un registro listando 3 campos y colocando 2 valores. Un mensaje indica que no hay suficientes valores.
```sql
insert into cuentas(numero, documento,nombre)
values (1234567893,'87654325');
```
Salida del Script

Nos da un error de ORA-00947: no hay suficientes valores.
```sh
Error que empieza en la línea: 45 del comando -
insert into cuentas(numero, documento,nombre)
values (1234567893,'87654325')
Error en la línea de comandos : 46 Columna : 1
Informe de error -
Error SQL: ORA-00947: no hay suficientes valores
00947. 00000 -  "not enough values"
*Cause:    
*Action:
```
##### 8. Intente ingresar un registro sin valor para un campo definido "not null".
```sql
insert into cuentas(numero, documento,nombre,saldo)
values ('','12345612','Daniel Lucano',1256.4);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."CUENTAS"."NUMERO").
```sh
Error que empieza en la línea: 48 del comando -
insert into cuentas(numero, documento,nombre,saldo)
values ('','12345612','Daniel Lucano',1256.4)
Error en la línea de comandos : 49 Columna : 9
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."CUENTAS"."NUMERO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 9. Vea los registros ingresados.
```sql
SELECT * FROM cuentas;
```
Salida del Script

```sh
1234567890	87654321	Daniel Zapana	1234,4
1234567891	87654321		1334,4
```
