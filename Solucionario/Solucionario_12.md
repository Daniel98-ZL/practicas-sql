# 12. Clave primaria (primary key)
## Ejercicos de laboratorio

### Trabajamos con la tabla "usuarios".

#### 1. Eliminamos la tabla:

```sql
drop table usuarios;
```
Salida del Script

```sh
Table USUARIOS borrado.
```
#### 2. Creamos la tabla definiendo el campo "nombre" como clave primaria:

```sql
create table usuarios(
    nombre varchar2(20),
    clave varchar2(10),
    primary key (nombre)
);
```
Salida del Script

```sh
Table USUARIOS creado.
```
#### 3. Al campo "nombre" no lo definimos "not null", pero al establecerse como clave primaria, Oracle lo convierte en "not null", veamos que en la columna "NULL" aparece "NOT NULL":

```sql
describe usuarios;
```
Salida del Script

```sh
Nombre ¿Nulo?   Tipo         
------ -------- ------------ 
NOMBRE NOT NULL VARCHAR2(20) 
CLAVE           VARCHAR2(10)
```
#### 4. Ingresamos algunos registros:

```sql
insert into usuarios (nombre, clave) values ('juanperez','Boca');
insert into usuarios (nombre, clave) values ('raulgarcia','River');
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.
```
#### 5. Recordemos que cuando un campo es clave primaria, sus valores no se repiten. Intentamos ingresar un valor de clave primaria existente:

```sql
insert into usuarios (nombre, clave) values ('juanperez','payaso');
```
Salida del Script

Nos da un error de ORA-00001: restricción única (DANIEL.SYS_C008328) violada.
```sh
Error que empieza en la línea: 14 del comando :
insert into usuarios (nombre, clave) values ('juanperez','payaso')
Informe de error -
ORA-00001: restricción única (DANIEL.SYS_C008328) violada
```
Aparece un mensaje de error y la sentencia no se ejecuta.

#### 6. Cuando un campo es clave primaria, sus valores no pueden ser nulos. Intentamos ingresar el valor "null" en el campo clave primaria:

```sql
insert into usuarios (nombre, clave) values (null,'payaso');
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."USUARIOS"."NOMBRE").
```sh
Error que empieza en la línea: 16 del comando -
insert into usuarios (nombre, clave) values (null,'payaso')
Error en la línea de comandos : 16 Columna : 46
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."USUARIOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
Aparece un mensaje de error y la sentencia no se ejecuta.

#### 7. Si realizamos alguna actualización, Oracle controla que los valores para el campo establecido como clave primaria no estén repetidos en la tabla. Intentemos actualizar el nombre de un usuario colocando un nombre existente:

```sql
update usuarios set nombre='juanperez' where nombre='raulgarcia';
```
Salida del Script

Nos da un error de ORA-00001: restricción única (DANIEL.SYS_C008328) violada.
```sh
Error que empieza en la línea: 18 del comando :
update usuarios set nombre='juanperez' where nombre='raulgarcia'
Informe de error -
ORA-00001: restricción única (DANIEL.SYS_C008328) violada
```
Aparece un mensaje indicando que se viola la clave primaria y la actualización no se realiza.

#### 8. Corroboramos que la tabla "usuarios" tiene establecido el campo "nombre" como clave primaria realizando la siguiente consulta (Recuerde colocar el nombre de la tabla en mayúsculas, sino Oracle considerará que no existe la tabla):

```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='USUARIOS';
```

Aparece la siguiente tabla:

```sh
TABLE_NAME                     COLUMN_NAME
------------------------------------------

USUARIOS                       NOMBRE
```
Salida del Script

```sh
USUARIOS	NOMBRE
```
Indicando que la tabla "usuarios" tiene establecido el campo "nombre" como clave primaria.

#### 9. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table usuarios;

create table usuarios(
    nombre varchar2(20),
    clave varchar2(10),
    primary key (nombre)
);

describe usuarios;

insert into usuarios (nombre, clave) values ('juanperez','Boca');
insert into usuarios (nombre, clave) values ('raulgarcia','River');

insert into usuarios (nombre, clave) values ('juanperez','payaso');

insert into usuarios (nombre, clave) values (null,'payaso');

update usuarios set nombre='juanperez' where nombre='raulgarcia';

select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='USUARIOS';
```

## Ejercicios propuestos

### Ejercicio 01

#### Trabaje con la tabla "libros" de una librería.

##### 1. Elimine la tabla:

```sql
drop table libros;
```
Salida del Script

```sh
Table LIBROS borrado.
```
##### 2. Créela con los siguientes campos, estableciendo como clave primaria el campo "codigo":

```sql
create table libros(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    autor varchar2(20),
    editorial varchar2(15),
    primary key (codigo)
);
```
Salida del Script

```sh
Table LIBROS creado.
```
##### 3. Ingrese los siguientes registros:

```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
insert into libros (codigo,titulo,autor,editorial) values (2,'Martin Fierro','Jose Hernandez','Planeta');
insert into libros (codigo,titulo,autor,editorial) values (3,'Aprenda PHP','Mario Molina','Nuevo Siglo');
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.

1 fila insertadas.
```
##### 4. Ingrese un registro con código repetido (aparece un mensaje de error)
```sql
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece');
```
Salida del Script

Nos da un error de ORA-00001: restricción única (DANIEL.SYS_C008331) violada.
```sh
Error que empieza en la línea: 40 del comando :
insert into libros (codigo,titulo,autor,editorial) values (1,'El aleph','Borges','Emece')
Informe de error -
ORA-00001: restricción única (DANIEL.SYS_C008331) violada
```
##### 5. Intente ingresar el valor "null" en el campo "codigo"
```sql
insert into libros (codigo,titulo,autor,editorial) values (null,'Pulgar','Borges','Emece');
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."LIBROS"."CODIGO").
```sh
Error que empieza en la línea: 42 del comando -
insert into libros (codigo,titulo,autor,editorial) values (null,'Pulgar','Borges','Emece')
Error en la línea de comandos : 42 Columna : 60
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."LIBROS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 6. Intente actualizar el código del libro "Martin Fierro" a "1" (mensaje de error)
```sql
update libros set codigo=1 where titulo='Martin Fierro';
```
Salida del Script

Nos da un error de ORA-00001: restricción única (DANIEL.SYS_C008331) violada.
```sh
Error que empieza en la línea: 44 del comando :
update libros set codigo=1 where titulo='Martin Fierro'
Informe de error -
ORA-00001: restricción única (DANIEL.SYS_C008331) violada
```
##### 7. Actualice el código del libro "Martin Fierro" a "10"
```sql
update libros set codigo=10 where titulo='Martin Fierro';
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 8. Vea qué campo de la tabla "LIBROS" fue establecido como clave primaria
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='LIBROS';
```
Salida del Script

```sh
LIBROS	CODIGO
```
##### 9. Vea qué campo de la tabla "libros" (en minúsculas) fue establecido como clave primaria
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='libros';
```
Salida del Script

```sh
la tabla sale vacia.
```
La tabla aparece vacía porque Oracle no encuentra la tabla "libros", ya que almacena los nombres de las tablas con mayúsculas.

### Ejercicio 02

#### Un instituto de enseñanza almacena los datos de sus estudiantes en una tabla llamada "alumnos".

##### 1. Elimine la tabla "alumnos":

```sql
drop table alumnos;
```
Salida del Script

Esta tabla no lo teniamos creado por eso nos da el siguiente error ORA-00942: la tabla o vista no existe.
```sh
Error que empieza en la línea: 60 del comando :
drop table alumnos
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
##### 2. Cree la tabla con la siguiente estructura intentando establecer 2 campos como clave primaria, el campo "documento" y "legajo":

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
);
```
Salida del Script

Nos da un error de ORA-02260: la tabla sólo puede tener una clave primaria.
```sh
Error que empieza en la línea: 62 del comando :
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(codigo),
    primary key(documento)
)
Informe de error -
ORA-02260: la tabla sólo puede tener una clave primaria
02260. 00000 -  "table can have only one primary key"
*Cause:    Self-evident.
*Action:   Remove the extra primary key.

```
Un mensaje indica la tabla solamente puede tener UNA clave primaria.

##### 3. Cree la tabla estableciendo como clave primaria el campo "documento":

```sql
create table alumnos(
    legajo varchar2(4) not null,
    documento varchar2(8),
    nombre varchar2(30),
    domicilio varchar2(30),
    primary key(documento)
);
```
Salida del Script

```sh
Table ALUMNOS creado.
```
##### 4. Verifique que el campo "documento" no admite valores nulos
```sql
describe alumnos;
```
Salida del Script

```sh
Nombre    ¿Nulo?   Tipo         
--------- -------- ------------ 
LEGAJO    NOT NULL VARCHAR2(4)  
DOCUMENTO NOT NULL VARCHAR2(8)  
NOMBRE             VARCHAR2(30) 
DOMICILIO          VARCHAR2(30)
```
##### 5. Ingrese los siguientes registros:

```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A233','22345345','Perez Mariana','Colon 234');
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');
```
Salida del Script

```sh
1 fila insertadas.

1 fila insertadas.
```
##### 6. Intente ingresar un alumno con número de documento existente (no lo permite)
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348');
```
Salida del Script

Nos da un error de ORA-00001: restricción única (DANIEL.SYS_C008333) violada.
```sh
Error que empieza en la línea: 84 del comando :
insert into alumnos (legajo,documento,nombre,domicilio) values('A567','23545345','Morales Marcos','Avellaneda 348')
Informe de error -
ORA-00001: restricción única (DANIEL.SYS_C008333) violada
```
##### 7. Intente ingresar un alumno con documento nulo (no lo permite)
```sql
insert into alumnos (legajo,documento,nombre,domicilio) values('A567',null,'Morales Marcos','Avellaneda 348');
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."ALUMNOS"."DOCUMENTO").
```sh
Error que empieza en la línea: 86 del comando -
insert into alumnos (legajo,documento,nombre,domicilio) values('A567',null,'Morales Marcos','Avellaneda 348')
Error en la línea de comandos : 86 Columna : 71
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."ALUMNOS"."DOCUMENTO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 8. Vea el campo clave primaria de "ALUMNOS".
```sql
select uc.table_name, column_name from user_cons_columns ucc
    join user_constraints uc
    on ucc.constraint_name=uc.constraint_name
    where uc.constraint_type='P' and
    uc.table_name='ALUMNOS';
```
Salida del Script

```sh
ALUMNOS	DOCUMENTO
```
