# 69. Eliminar campos (alter table - drop)

## Practica de laboratorio

Trabajamos con la tablas "libros" y "editoriales" de una librería.
Eliminamos las tablas:

```sql
drop table libros;
drop table editoriales;
```

Creamos las tablas:

```sql
create table editoriales(
  codigo number(3),
  nombre varchar2(30),
  primary key(codigo)
 );

create table libros(
  titulo varchar2(30),
  editorial number(3),
  autor varchar2(30),
  precio number(6,2),
  constraint FK_libros_editorial
   foreign key(editorial)
   references editoriales(codigo)
 );
```

Eliminamos un campo de la tabla "libros":

```sql
alter table libros
drop column precio;
```

Vemos la estructura de la tabla "libros":

```sql
describe libros;
```

El campo "precio" ya no existe.

Recuerde que no pueden eliminarse los campos referenciados por una "foreign key". Intentamos eliminar el campo "codigo" de "editoriales":

```sql
alter table editoriales
drop column codigo;
```

Un mensaje indica que la sentencia no fue ejecutada.

Eliminamos el campo "editorial" de "libros":

```sql
alter table libros
drop column editorial;
```

Verificamos que el campo no existe:

```sql
describe libros;
```

El campo se ha eliminado y junto con él la restricción "foreign key":

```sql
select *from user_constraints
where table_name='LIBROS';
```

Ahora si podemos eliminar el campo "codigo" de "editoriales", pues la restricción "foreign key" que hacía referencia a ella ya no existe:

```sql
alter table editoriales
drop column codigo;
```

El campo "codigo" de "editoriales" se ha eliminado y junto con él la restricción "primary key":

```sql
select *from user_constraints
where table_name='EDITORIALES';
```

Agregamos un índice compuesto sobre "titulo" y "autor" de "libros":

```sql
create unique index I_libros_titulo
on libros(titulo,autor);
```

Veamos si existe tal índice:

```sql
select index_name,column_name,column_position
from user_ind_columns
where table_name='LIBROS';
```

Recuerde que si elimina un campo indizado, su índice también se elimina. Eliminamos el campo "autor" de "libros":

```sql
alter table libros
drop column autor;
```

Veamos si existe el índice compuesto creado anteriormente sobre los campos "titulo" y "autor" de "libros":

```sql
select index_name,column_name,column_position
from user_ind_columns
where table_name='LIBROS';
```

Ya no existe.

La tabla ahora solamente consta de un campo, por lo tanto, no puede eliminarse, pues la tabla no puede quedar vacía de campos:

```sql
alter table libros
drop column titulo;
```

Mensaje de error.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table editoriales(
    codigo number(3),
    nombre varchar2(30),
    primary key(codigo)
);

create table libros(
    titulo varchar2(30),
    editorial number(3),
    autor varchar2(30),
    precio number(6,2),
    constraint FK_libros_editorial
    foreign key(editorial)
    references editoriales(codigo)
);

 -- Eliminamos un campo de la tabla "libros":
alter table libros
drop column precio;

 -- Vemos la estructura de la tabla "libros":
describe libros;

 -- Intentamos eliminar el campo "codigo" de "editoriales":
alter table editoriales
drop column codigo;
 -- Un mensaje indica que la sentencia no fue ejecutada.

 -- Eliminamos el campo "editorial" de "libros":
alter table libros
drop column editorial;

describe libros;
 -- El campo se ha eliminado y junto con él la restricción "foreign key":

select *from user_constraints
where table_name='LIBROS';

 -- Ahora si podemos eliminar el campo "codigo" de "editoriales", pues la
 -- restricción "foreign key" que hacía referencia a ella ya no existe:
alter table editoriales
drop column codigo;

select *from user_constraints
where table_name='EDITORIALES';

-- Agregamos un índice compuesto sobre "titulo" y "autor" de "libros":
create unique index I_libros_titulo
on libros(titulo,autor);

 --Veamos si existe tal índice:
select index_name,column_name,column_position
from user_ind_columns
where table_name='LIBROS';

 --Recuerde que si elimina un campo indizado, su índice también se elimina.
 -- Eliminamos el campo "autor" de "libros":
alter table libros
drop column autor;

 --Veamos si existe el índice compuesto creado anteriormente sobre los
 -- campos "titulo" y "autor" de "libros":
select index_name,column_name,column_position
from user_ind_columns
where table_name='LIBROS';

 -- La tabla ahora solamente consta de un campo, por lo tanto, no puede eliminarse,
 -- pues la tabla no puede quedar vacía de campos:
alter table libros
drop column titulo;
 -- Mensaje de error.
```

## Ejercicios propuestos

Trabaje con una tabla llamada "empleados" y "secciones".
1. Elimine las tablas y créelas:

```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20),
    nombre varchar2(20) not null,
    domicilio varchar2(30),
    seccion number(2),
    sueldo number(8,2),
    constraint CK_empleados_sueldo
    check (sueldo>=0) disable,
    fechaingreso date,
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
 );
```

2. Ingrese algunos registros en ambas tablas:

```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Juan','Colon 123',8,505.50,'10/10/1980');
insert into empleados values('Gonzalez','Juana','Avellaneda 222',9,600,'01/05/1990');
insert into empleados values('Perez','Luis','Caseros 987',10,800,'12/09/2000');
```

3. Elimine el campo "domicilio" y luego verifique la eliminación
```sql
ALTER TABLE empleados
DROP COLUMN domicilio;

-- Verificar la eliminación del campo
DESCRIBE empleados;

```
4. Vea las restricciones de "empleados" (1 restricción "foreign key" y 2 "check")
```sql
-- Consultar las restricciones de la tabla "empleados"
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
5. Intente eliminar el campo "codigo" de "secciones"
```sql
-- Esta consulta arrojará un error porque el campo "codigo" es clave primaria
ALTER TABLE secciones
DROP COLUMN codigo;

```
6. Elimine la restricción "foreign key" de "empleados", luego elimine el campo "codigo" de "secciones" y verifique la eliminación
```sql
-- Eliminar la restricción "foreign key" de "empleados"
ALTER TABLE empleados
DROP CONSTRAINT FK_empleados_seccion;

-- Eliminar el campo "codigo" de "secciones"
ALTER TABLE secciones
DROP COLUMN codigo;

-- Verificar la eliminación del campo
DESCRIBE secciones;

```
7. Verifique que al eliminar el campo "codigo" de "secciones" se ha eliminado la "primary key" de "secciones"
```sql
-- Consultar las restricciones de la tabla "secciones"
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'SECCIONES';

```
8. Elimine el campo "sueldo" y verifique que la restricción sobre tal campo se ha eliminado
```sql
ALTER TABLE empleados
DROP COLUMN sueldo;

-- Verificar la eliminación de la restricción
SELECT constraint_name, constraint_type
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
9. Cree un índice no único por el campo "apellido" y verifique su existencia consultando "user_indexes"
```sql
-- Crear un índice no único en "empleados" por el campo "apellido"
CREATE INDEX idx_apellido ON empleados(apellido);

-- Verificar la existencia del índice
SELECT index_name, table_name
FROM user_indexes
WHERE table_name = 'EMPLEADOS';

```
10. Elimine el campo "apellido" y verifique que el índice se ha eliminado
```sql
-- Eliminar el campo "apellido" de "empleados"
ALTER TABLE empleados
DROP COLUMN apellido;

-- Verificar la eliminación del índice
SELECT index_name, table_name
FROM user_indexes
WHERE table_name = 'EMPLEADOS';

```
11. Elimine 2 campos de "empleados" y vea la estructura de la tabla
```sql
-- Eliminar los campos "fechaingreso" y "nombre" de "empleados"
ALTER TABLE empleados
DROP COLUMN fechaingreso,
DROP COLUMN nombre;

-- Verificar la estructura de la tabla
DESCRIBE empleados;

```
12. Intente eliminar el único campo de "empleados"
```sql
-- Esta consulta arrojará un error porque no es posible eliminar el único campo de una tabla
ALTER TABLE empleados
DROP COLUMN domicilio;

```