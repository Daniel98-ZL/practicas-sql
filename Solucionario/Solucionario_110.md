# 110. Disparador de múltiples eventos

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario, la fecha, y el tipo de modificación que se realizó sobre la tabla "libros".

Eliminamos la tabla "libros" y la tabla "control":

```sql
drop table libros;
drop table control;
```

Creamos la tabla con la siguiente estructura:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);
```

Creamos la tabla "control":

```sql
create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Creamos un disparador a nivel de sentencia, que se dispare cada vez que se ingrese, actualice o elimine un registro de la tabla "libros". El trigger ingresa en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó la modificación y el tipo de operación que se realizó:

- si se realizó una inserción (insert), se almacena "inserción";

- si se realizó una actualización (update), se almacena "actualización" y

- si se realizó una eliminación (delete) se almacena "borrado".

```sql
create or replace trigger tr_cambios_libros
    before insert or update or delete
    on libros
        for each row
begin
    if inserting then
        insert into control values (user, sysdate,'inserción');
        end if;
        if updating then
            insert into control values (user, sysdate,'actualización');
        end if;
            if deleting then
                insert into control values (user, sysdate,'borrado');
        end if;
    end tr_cambios_libros;
/
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
 select *from user_triggers where trigger_name ='TR_CAMBIOS_LIBROS';
```

obtenemos la siguiente información:

- trigger_name: nombre del disparador;

- trigger_type: momento y nivel, en este caso es un desencadenador "before" y a nivel de fila (each row);

- triggering_event: evento que lo dispara, en este caso, "insert or update or delete";

- base_object_type: a qué objeto está asociado, puede ser una tabla o una vista, en este caso, una tabla (table);

- table_name: nombre de la tabla al que está asociado (libros);

Ingresamos un registro en "libros":

```sql
insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',23);
```

Veamos si el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

Vemos que se ingresó un registro que muestra que el usuario "system", el día y hora actual realizó una inserción sobre "libros".

Actualizamos algunos registros de "libros":

```sql
update libros set precio=precio+precio*0.1 where editorial='Planeta';
```

Veamos cuántas veces el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

Vemos que se ingresaron 3 nuevos registros que muestran que el usuario "system", el día y hora actual actualizó tres registros de "libros". Si el trigger se hubiese definido a nivel de sentencia, el "update" anterior se hubiese disparado una sola vez.

Eliminamos un registro de "libros":

```sql
delete from libros where codigo=145;
```

Veamos si el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

Vemos que se eliminó 1 registro.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table control;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_cambios_libros
    before insert or update or delete
    on libros
        for each row
begin
    if inserting then
        insert into control values (user, sysdate,'inserción');
        end if;
        if updating then
            insert into control values (user, sysdate,'actualización');
        end if;
        if deleting then
            insert into control values (user, sysdate,'borrado');
        end if;
    end tr_cambios_libros;
/

select *from user_triggers where trigger_name ='TR_CAMBIOS_LIBROS';

insert into libros values(150,'El experto en laberintos','Gaskin','Planeta',23);

select *from control;

update libros set precio=precio+precio*0.1 where editorial='Planeta';

select *from control;

delete from libros where codigo=145;

select *from control;
```

## Ejercicios propuestos

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica el precio o la editorial de un libro.

1. Elimine las tablas:


```sql
drop table control;
drop table libros;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);
```

3. Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

4. El gerente permite:

- ingresar o borrar libros de la tabla "libros" unicamente los sábados de 8 a 12 hs.

- actualizar los precios de los libros de lunes a viernes de 8 a 18 hs. y sábados entre la 8 y 12 hs.

Cree un disparador para los tres eventos que controle la hora en que se realizan las operaciones sobre "libros". Si se intenta eliminar, ingresar o actualizar registros de "libros" fuera de los días y horarios permitidos, debe aparecer un mensaje de error. Si la operación de ingreso, borrado o actualización de registros se realiza, se debe almacenar en "control", el nombre del usuario, la fecha y el tipo de operación ejecutada
```sql
-- Crear el disparador en la tabla "libros"
create or replace trigger tr_control_horario
before insert or update or delete on libros
for each row
declare
    dia_semana varchar2(20);
    hora_actual varchar2(8);
    usuario_actual varchar2(30);
begin
    -- Obtener el día de la semana actual
    select to_char(sysdate, 'Day') into dia_semana from dual;

    -- Obtener la hora actual
    select to_char(sysdate, 'HH24:MI:SS') into hora_actual from dual;

    -- Obtener el nombre de usuario actual (puedes reemplazarlo con el nombre real del usuario)
    usuario_actual := 'Usuario1';

    -- Verificar si la operación se realiza dentro de los días y horarios permitidos
    if (dia_semana = 'Saturday' and hora_actual >= '08:00:00' and hora_actual <= '12:00:00') or
       (dia_semana in ('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday') and hora_actual >= '08:00:00' and hora_actual <= '18:00:00') then

        -- Operación permitida
        if inserting then
            -- Registro de inserción en "control"
            insert into control values (usuario_actual, sysdate, 'Inserción');
        elsif updating then
            -- Registro de actualización en "control"
            insert into control values (usuario_actual, sysdate, 'Actualización');
        elsif deleting then
            -- Registro de borrado en "control"
            insert into control values (usuario_actual, sysdate, 'Borrado');
        end if;

    else
        -- Operación fuera de los días y horarios permitidos, mostrar mensaje de error
        raise_application_error(-20001, 'No se permite realizar la operación fuera del horario permitido');
    end if;
end;
/

```
5. Cambie la fecha y hora del sistema a "domingo 19 hs.". Intente ingresar un libro
Mensaje de error.
```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
alter session set NLS_DATE_LANGUAGE = 'AMERICAN';
alter session set NLS_TERRITORY = 'AMERICA';

-- Establecer la fecha y hora del sistema a "domingo 19 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Intentar ingresar un libro
insert into libros values(150, 'El Principito', 'Antoine de Saint-Exupéry', 'Planeta', 20);

```
6. Cambie la fecha y hora del sistema a "lunes 10 hs.". Intente ingresar un libro.
Mensaje de error.
```sql
-- Establecer la fecha y hora del sistema a "lunes 10 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Intentar ingresar un libro
insert into libros values(150, 'El Principito', 'Antoine de Saint-Exupéry', 'Planeta', 20);

```
7. Cambie la fecha y hora del sistema a "sabado 8 hs.". Ingrese un libro
```sql
-- Establecer la fecha y hora del sistema a "sábado 8 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Ingresar un libro
insert into libros values(150, 'El Principito', 'Antoine de Saint-Exupéry', 'Planeta', 20);

```
8. Realice un "select" sobre "libros" y sobre "control" para verificar que se han cargado los datos correspondientes/
Aparece el nuevo libro en "libros" y una fila de "ingreso" en "control".
```sql
-- Verificar los datos en "libros"
select * from libros;

-- Verificar los datos en "control"
select * from control;

```
9. Cambie la fecha y hora del sistema a "domingo 18 hs.". Intente modificar el precio de un libro.
Mensaje de error.
```sql
-- Establecer la fecha y hora del sistema a "domingo 18 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Intentar modificar el precio de un libro
update libros set precio = 30 where codigo = 150;

```
10. Cambie la fecha y hora del sistema a "sabado 15 hs.". Intente modificar el precio de un libro.
Mensaje de error.
```sql
-- Establecer la fecha y hora del sistema a "sábado 15 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Intentar modificar el precio de un libro
update libros set precio = 30 where codigo = 150;

```
11. Cambie la fecha y hora del sistema a "sabado 9 hs.". Actualice el precio de un libro
```sql
-- Establecer la fecha y hora del sistema a "sábado 9 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Actualizar el precio de un libro
update libros set precio = 30 where codigo = 150;

```
12. Realice un "select" sobre "libros" y sobre "control" para verificar que se han cargado los datos correspondientes.
Aparece el nuevo precio en "libros" y una nueva fila de "actualización" en "control".
```sql
-- Verificar los datos en "libros"
select * from libros;

-- Verificar los datos en "control"
select * from control;

```
13. Cambie la fecha y hora del sistema a "martes 11:30 hs.". Actualice el precio de un libro
```sql
-- Establecer la fecha y hora del sistema a "martes 11:30 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Actualizar el precio de un libro
update libros set precio = 40 where codigo = 150;

```
14. Realice un "select" sobre "libros" y sobre "control" para verificar que se han cargado los datos correspondientes.
Aparece el nuevo precio en "libros" y una nueva fila de "actualización" en "control".
```sql
-- Verificar los datos en "libros"
select * from libros;

-- Verificar los datos en "control"
select * from control;

```
15. Cambie la fecha y hora del sistema a "domingo 18:30 hs.". Intente borrar un libro.
Mensaje de error.
```sql
-- Establecer la fecha y hora del sistema a "domingo 18:30 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Intentar borrar un libro
delete from libros where codigo = 150;

```
16. Cambie la fecha y hora del sistema a "miercoles 15 hs.". Intente borrar un libro.
Mensaje de error.
```sql
-- Establecer la fecha y hora del sistema a "miércoles 15 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Intentar borrar un libro
delete from libros where codigo = 150;

```
17. Cambie la fecha y hora del sistema a "sábado 11:55 hs.". Borre un libro
```sql
-- Establecer la fecha y hora del sistema a "sábado 11:55 hs."
-- Esto puede variar dependiendo del sistema de base de datos que estés utilizando.

-- Borrar un libro
delete from libros where codigo = 150;

```
18. Realice un "select" sobre "libros" y sobre "control" para verificar que se han cargado los datos correspondientes.
Se ha eliminado el registro en "libros" y se ha cargado una nueva fila de "borrado" en "control".
```sql
-- Verificar los datos en "libros"
select * from libros;

-- Verificar los datos en "control"
select * from control;

```