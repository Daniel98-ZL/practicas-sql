# 55. Otros tipos de combinaciones

## Practica de laboratorio

Una librería almacena la información de sus libros para la venta en dos tablas, "libros" y "editoriales".

Eliminamos ambas tablas y las creamos:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigoeditorial number(3),
    nombre varchar2(20)
);
```

Ingresamos algunos registros en ambas tablas:

```sql
insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Siglo XXI');
insert into editoriales values(null,'Norma');

insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',1);
insert into libros values(102,'Aprenda PHP','Mario Molina',2);
insert into libros values(103,'Java en 10 minutos',null,4);
insert into libros values(104,'El anillo del hechicero','Carol Gaskin',null);
```

Realizamos un natural join entre las dos tablas:

```sql
select titulo,nombre as editorial
from libros
natural join editoriales;
```

En el ejemplo anterior la tabla "libros" combina su campo "codigoeditorial" con el campo "codigoeditorial" de "editoriales".

Realizamos una combinación empleando la cláusula "using":

```sql
select titulo,nombre as editorial
from libros
join editoriales
using (codigoeditorial);
```

En el ejemplo anterior la tabla "libros" combina su campo "codigoeditorial" con el campo "codigoeditorial" de "editoriales".

Realizamos una combinación izquierda y luego un "join" con el modificador "(+)"; ambas consultas retornan el mismo resultado:

```sql
select titulo,nombre as editorial
from libros l
left join editoriales e
on l.codigoeditorial = e.codigoeditorial;

select titulo,nombre as editorial
from libros l
join editoriales e
on l.codigoeditorial = e.codigoeditorial(+);
```

Ambas mostrarán el título y nombre de la editorial; los libros cuyo código de editorial no esté presente en "editoriales" aparecerán con el valor "null" en la columna "editorial".

Realizamos una combinación derecha y luego obtenemos el mismo resultado empleando "join" y el modificador "(+)":

```sql
select titulo,nombre as editorial
from editoriales e
right join libros l
on e.codigoeditorial = l.codigoeditorial;

select titulo,nombre as editorial
from editoriales e
join libros l
on e.codigoeditorial(+) = l.codigoeditorial;
```

Ambas mostrarán el título y nombre de la editorial; las editoriales que no encuentran coincidencia en "libros", aparecen con el valor "null" en la columna "titulo".

Si intentamos emplear el modificador en campos de distintas tablas Oracle mostrará un mensaje de error:

```sql
select titulo,nombre as editorial
from libros l
join editoriales e
on l.codigoeditorial(+)= e.codigoeditorial(+);
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    codigoeditorial number(3)
);

create table editoriales(
    codigoeditorial number(3),
    nombre varchar2(20)
);

insert into editoriales values(1,'Planeta');
insert into editoriales values(2,'Emece');
insert into editoriales values(3,'Siglo XXI');
insert into editoriales values(null,'Norma');
insert into libros values(100,'El aleph','Borges',1);
insert into libros values(101,'Martin Fierro','Jose Hernandez',1);
insert into libros values(102,'Aprenda PHP','Mario Molina',2);
insert into libros values(103,'Java en 10 minutos',null,4);
insert into libros values(104,'El anillo del hechicero','Carol Gaskin',null);

select titulo,nombre as editorial
from libros
natural join editoriales;

select titulo,nombre as editorial
from libros
join editoriales
using (codigoeditorial);

select titulo,nombre as editorial
from libros l
left join editoriales e
on l.codigoeditorial = e.codigoeditorial;

select titulo,nombre as editorial
from libros l
join editoriales e
on l.codigoeditorial = e.codigoeditorial(+);

select titulo,nombre as editorial
from editoriales e
right join libros l
on e.codigoeditorial = l.codigoeditorial;

select titulo,nombre as editorial
from editoriales e
join libros l
on e.codigoeditorial(+) = l.codigoeditorial;

select titulo,nombre as editorial
from libros l
join editoriales e
on l.codigoeditorial(+)= e.codigoeditorial(+);
```

## Ejercicios propuestos

Una empresa tiene registrados sus clientes en una tabla llamada "clientes", también tiene una tabla "provincias" donde registra los nombres de las provincias.

1. Elimine las tablas "clientes" y "provincias" y créelas:

```sql
drop table clientes;
drop table provincias;

create table clientes (
    codigo number(5),
    nombre varchar2(30),
    domicilio varchar2(30),
    ciudad varchar2(20),
    codigoprovincia number(2)
);

create table provincias(
    codigoprovincia number(2),
    nombre varchar2(20)
);
```

2. Ingrese algunos registros para ambas tablas:

```sql
insert into provincias values(1,'Cordoba');
insert into provincias values(2,'Santa Fe');
insert into provincias values(3,'Corrientes');
insert into provincias values(null,'Salta');
insert into clientes values (100,'Lopez Marcos','Colon 111','Córdoba',1);
insert into clientes values (101,'Perez Ana','San Martin 222','Cruz del Eje',1);
insert into clientes values (102,'Garcia Juan','Rivadavia 333','Villa Maria',1);
insert into clientes values (103,'Perez Luis','Sarmiento 444','Rosario',2);
insert into clientes values (104,'Gomez Ines','San Martin 666','Santa Fe',2);
insert into clientes values (105,'Torres Fabiola','Alem 777','La Plata',4);
insert into clientes values (106,'Garcia Luis','Sucre 475','Santa Rosa',null);
```

3. Muestre todos los datos de los clientes, incluido el nombre de la provincia empleando un "left join" (7 filas)
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c
LEFT JOIN provincias p ON c.codigoprovincia = p.codigoprovincia;

```
4. Obtenga la misma salida que la consulta anterior pero empleando un "join" con el modificador (+)
Note que en los puntos 3 y 4, los registros "Garcia Luis" y "Torres Fabiola" aparecen aunque no encuentran coincidencia en "provincias", mostrando "null" en la columna "provincia".
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c, provincias p
WHERE c.codigoprovincia = p.codigoprovincia(+);

```
5. Muestre todos los datos de los clientes, incluido el nombre de la provincia empleando un "right join" para que las provincias de las cuales no hay clientes también aparezcan en la consulta (7 filas)
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c
RIGHT JOIN provincias p ON c.codigoprovincia = p.codigoprovincia;

```
6. Obtenga la misma salida que la consulta anterior pero empleando un "join" con el modificador (+)
Note que en los puntos 5 y 6, las provincias "Salta" y "Corrientes" aparecen aunque no encuentran coincidencia en "clientes", mostrando "null" en todos los campos de tal tabla.
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c, provincias p
WHERE c.codigoprovincia(+) = p.codigoprovincia;

```
7. Intente colocar en una consulta "join", el modificador "(+)" en ambos campos del enlace (mensaje de error)
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c, provincias p
WHERE c.codigoprovincia(+) = p.codigoprovincia(+);

```
8. Intente realizar un natural join entre ambas tablas mostrando el nombre del cliente, la ciudad y nombre de la provincia (las tablas tienen 2 campos con igual nombre "codigoprovincia" y "nombre"; mensaje de error)
```sql
SELECT *
FROM clientes NATURAL JOIN provincias;

```
9. Realice una combinación entre ambas tablas empleando la cláusula "using" (5 filas)
```sql
SELECT c.*, p.nombre AS provincia
FROM clientes c
JOIN provincias p USING (codigoprovincia);

```