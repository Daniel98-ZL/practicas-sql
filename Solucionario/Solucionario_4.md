# 4. Recuperación de algunos campos (select)
## Ejercicios laboratorio

### Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.
#### 1. Eliminamos la tabla, si existe:
```sql
drop table libros;
```
Salida del Script
```sh
Table LIBROS borrado.
```
#### 2. Creamos la tabla libros:
```sql
create table libros(
  titulo varchar(20),
  autor varchar(30),
  editorial varchar(15),
  precio float,
  cantidad integer
);

```
Salida del Script

```sh
Table LIBROS creado.
```
#### 3. Veamos la estructura de la tabla:

```sql
describe libros;
```
Salida del Script

```sh
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 
PRECIO           FLOAT(126)   
CANTIDAD         NUMBER(38)   

```

#### 4. Ingresamos algunos registros:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',45.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta',25,200);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Planeta',15.8,200);
```
Salida del Script

Nos da un error para el segundo registro el cual nos muestra ORA-12899: el valor es demasiado grande para la columna "DANIEL"."LIBROS"."TITULO" (real: 35, máximo: 20).

```sh
1 fila insertadas.


Error que empieza en la línea: 14 del comando -
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta',25,200)
Error en la línea de comandos : 14 Columna : 69
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "DANIEL"."LIBROS"."TITULO" (real: 35, máximo: 20)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

1 fila insertadas.

```

#### 5. Para ver todos los campos de una tabla tipeamos:

```sql
select *from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	45,5	100
Matematica estas ahi	Paenza	Planeta	15,8	200
```

#### 6. Para ver solamente el título, autor y editorial de todos los libros especificamos los nombres de los campos separados por comas:

```sql
select titulo,autor,editorial from libros;
```
Salida del Script
```sh
El aleph	Borges	Emece
Matematica estas ahi	Paenza	Planeta
```

#### 7. La siguiente sentencia nos mostrará los títulos y precios de todos los libros:

```sql
select titulo,precio from libros;
```
Salida del Script
```sh
El aleph	45,5
Matematica estas ahi	15,8
```

#### 8. Para ver solamente la editorial y la cantidad de libros tipeamos:

```sql
select editorial,cantidad from libros;
```
Salida del Script

```sh
Emece	100
Planeta	200
```
## Ejercicios propuestos

### Ejercicio 01

#### Un videoclub que alquila películas en video almacena la información de sus películas en alquiler en una tabla llamada "peliculas".

##### 1. Elimine la tabla, si existe:

```sql
drop table peliculas;
```
Salida del Script

```sh
Table PELICULAS borrado.
```

##### 2. Cree la tabla:

```sql
create table peliculas(
  titulo varchar(20),
  actor varchar(20),
  duracion integer,
  cantidad integer
);
```
Salida del Script

```sh
Table PELICULAS creado.
```

##### 3. Vea la estructura de la tabla:

```sql
describe peliculas;
```
Salida del Script

```sh
Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
TITULO          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(38)   
CANTIDAD        NUMBER(38)   

```

##### 4. Ingrese los siguientes registros:

```sql
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,2);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,3);
 insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',90,2);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```

##### 5. Realice un "select" mostrando solamente el título y actor de todas las películas:

```sql
select titulo,actor from peliculas;
```
Salida del Script

```sh
Mision imposible	Tom Cruise
Mision imposible 2	Tom Cruise
Mujer bonita	Julia R.
Elsa y Fred	China Zorrilla
```
##### 6. Muestre el título y duración de todas las peliculas.

```sql
select titulo,duracion from peliculas;
```
Salida del Script

```sh
Mision imposible	120
Mision imposible 2	180
Mujer bonita	90
Elsa y Fred	90
```

##### 7. Muestre el título y la cantidad de copias.

```sql
select titulo,cantidad from peliculas;
```
Salida del Script

```sh
Mision imposible	3
Mision imposible 2	2
Mujer bonita	3
Elsa y Fred	2
```
### Ejercicio 02

#### A. Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

##### 1. Elimine la tabla, si existe:

```sql
drop table empleados;
```
Salida del Script

```sh
Table EMPLEADOS borrado.
```
##### 2. Cree la tabla:

```sql
 create table empleados(
  nombre varchar(20),
  documento varchar(8), 
  sexo varchar(1),
  domicilio varchar(30),
  sueldobasico float
 );
```
Salida del Script

```sh
Table EMPLEADOS creado.
```

##### 3. Vea la estructura de la tabla:

```sql
describe empleados;
```
Salida del Script

```sh
Nombre       ¿Nulo? Tipo         
------------ ------ ------------ 
NOMBRE              VARCHAR2(20) 
DOCUMENTO           VARCHAR2(8)  
SEXO                VARCHAR2(1)  
DOMICILIO           VARCHAR2(30) 
SUELDOBASICO        FLOAT(126)   

```

##### 4. Ingrese algunos registros:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22345678','m','Sarmiento 123',300);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24345678','f','Colon 134',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Marcos Torres','27345678','m','Urquiza 479',800);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```

##### 5. Muestre todos los datos de los empleados.

```sql
select * from empleados;
```
Salida del Script

```sh
Juan Perez	22345678	m	Sarmiento 123	300
Ana Acosta	24345678	f	Colon 134	500
Marcos Torres	27345678	m	Urquiza 479	800
```

##### 6. Muestre el nombre y documento de los empleados.

```sql
select nombre,documento from empleados;
```
Salida del Script

```sh
Juan Perez	22345678
Ana Acosta	24345678
Marcos Torres	27345678
```

##### 7. Realice un "select" mostrando el nombre, documento y sueldo básico de todos los empleados.

```sql
select nombre,documento,sueldobasico from empleados;
```
Salida del Script

```sh
Juan Perez	22345678	300
Ana Acosta	24345678	500
Marcos Torres	27345678	800
```


#### B) Un comercio que vende artículos de computación registra la información de sus productos en la tabla llamada "articulos".

##### 1. Elimine la tabla si existe:

```sql
drop table articulos;
```
Salida del Script

```sh
Table ARTICULOS borrado.
```

##### 2. Cree la tabla "articulos" con los campos necesarios para almacenar los siguientes datos:

* código del artículo: entero,
* nombre del artículo: 20 caracteres de longitud,
* descripción: 30 caracteres de longitud,
* precio: float.

```sql
create table articulos(
  codigo INTEGER,
  nombre varchar(20), 
  descripcion varchar(30),
  precio float
 );
```
Salida del Script

```sh
Table ARTICULOS creado.
```
##### 3. Vea la estructura de la tabla (describe).

```sql
describe articulos;
```
Salida del Script

```sh
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(38)   
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             FLOAT(126)   
```

##### 4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio) values (1,'impresora','Epson Stylus C45',400.80);
insert into articulos (codigo, nombre, descripcion, precio) values (2,'impresora','Epson Stylus C85',500);
insert into articulos (codigo, nombre, descripcion, precio) values (3,'monitor','Samsung 14',800);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 5. Muestre todos los campos de todos los registros.

```sql
select * from articulos;
```
Salida del Script

```sh
1	impresora	Epson Stylus C45	400,8
2	impresora	Epson Stylus C85	500
3	monitor	Samsung 14	800
```
##### 6. Muestre sólo el nombre, descripción y precio.

```sql
select nombre,descripcion,precio from articulos;
```
Salida del Script

```sh
impresora	Epson Stylus C45	400,8
impresora	Epson Stylus C85	500
monitor	Samsung 14	800
```


