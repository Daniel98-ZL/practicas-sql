# 40. Restricciones: validación y estados (validate - novalidate - enable - disable)

## Practica de laboratorario

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla:

```sql
drop table libros;
```

La creamos e ingresamos algunos registros:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros values (1,'Uno','Bach','Planeta',22);
insert into libros values (2,'El quijote','Cervantes','Emece',15);
insert into libros values (2,'Aprenda PHP','Mario Molina','Siglo XXI',-40);
```

Intentamos agregar una restricción "primary key" para asegurar que los códigos no se repitan, pero como ya tenemos almacenado registros que infringen la restricción, Oracle nos mostrará un mensaje de error:

```sql
alter table libros
add constraint PK_libros_codigo
primary key (codigo);
```

Vamos a especificar que no haya comprobación de datos existentes agregando "disable" y "novalidate":

```sql
alter table libros
add constraint PK_libros_codigo
primary key (codigo) disable novalidate;
```

Veamos lo que nos informa "user_constraints":

```sql
select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';
```

Aparece la siguiente tabla:

```sh
CONSTRAINT_NAME     CONSTRAINT_TYPE     STATUS      VALIDATED
--------------------------------------------------------------------------
PK_LIBROS_CODIGO    P                   DISABLED    NOT VALIDATED
```

La columna "status" nos informa que está deshabilitada (Disabled).

Si ingresamos un registro con código existente, Oracle lo permite, porque la restricción está en estado "disabled":

```sql
insert into libros values (2,'Momo','Michael Ende','Alfaragua',25);
```

Intentamos habilitar la restricción sin verificar los datos ya almacenados:

```sql
alter table libros
enable novalidate constraint PK_libros_codigo;
```

No lo permite, aun cuando especificamos que no valide los datos existentes, Oracle realiza la verificación igualmente.

Eliminamos los registros con clave duplicada:

```sql
delete libros where titulo='El quijote';
delete libros where titulo='Momo';
```

Ahora Oracle permite habilitar la restricción:

```sql
alter table libros
enable novalidate constraint PK_libros_codigo;
```

Si intentamos actualizar un registro repitiendo la clave primaria, Oracle no lo permite:

```sql
insert into libros values (2,'Momo','Michael Ende','Alfaragua',25);
```

Veamos lo que nos informa "user_constraints":

```sql
select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';
```

Aparece la siguiente tabla:

```sh
CONSTRAINT_NAME     CONSTRAINT_TYPE     STATUS      VALIDATED
--------------------------------------------------------------------------
PK_LIBROS_CODIGO    P                   ENABLED     NOT VALIDATED
```

Intentamos agregamos una restricción "check" que no permita valores negativos para el precio:

```sql
alter table libros
add constraint CK_libros_precio
check(precio>=0);
```

Oracle no lo permite porque, por defecto, la opción es "validate" y existen precios que violan la restricción que intentamos establecer.

Agregamos la restricción especificando que no valide los datos almacenados:

```sql
alter table libros
add constraint CK_libros_precio
check(precio>=0) novalidate;
```

Veamos el estado de la restricción de control:

```sql
select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';
```

El resultado es el siguiente:

```sh
CONSTRAINT_TYPE     STATUS      VALIDATED
-----------------------------------------------------
C                   ENABLED     NOT VALIDATED
```

Si intentamos ingresar un valor negativo para el precio, aparecerá un mensaje de error, porque la restricción de control creada está habilitada:

```sql
insert into libros values (3,'Momo','Michael Ende','Alfaragua',-25);
```

Deshabilitamos la restricción "CK_libros_precio":

```sql
alter table libros
disable constraint CK_libros_precio;
```

Veamos el estado actual:

```sql
select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';
```

Nos muestra que está deshabilitada y no valida los datos existentes.

Ahora si podemos ingresar el registro:

```sql
insert into libros values (3,'Momo','Michael Ende','Alfaragua',-25);
```

Habilitamos la restricción para futuros ingresos pero no para los existentes:

```sql
alter table libros
enable novalidate constraint CK_libros_precio;
```

Note que Oracle lo permite, no valida los datos existentes, pero si fuera otro tipo de restricción, no lo permitiría.

Consultamos "user_constraints":

```sql
select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';
```

Nos muestra que está habilitada y no valida los datos existentes.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros values (1,'Uno','Bach','Planeta',22);
insert into libros values (2,'El quijote','Cervantes','Emece',15);
insert into libros values (2,'Aprenda PHP','Mario Molina','Siglo XXI',-40);

alter table libros
add constraint PK_libros_codigo
primary key (codigo);

alter table libros
add constraint PK_libros_codigo
primary key (codigo) disable novalidate;

select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';

insert into libros values (2,'Momo','Michael Ende','Alfaragua',25);

alter table libros
enable novalidate constraint PK_libros_codigo;

delete libros where titulo='El quijote';
delete libros where titulo='Momo';

alter table libros
enable novalidate constraint PK_libros_codigo;

insert into libros values (2,'Momo','Michael Ende','Alfaragua',25);

select constraint_name, constraint_type, status, validated
from user_constraints
where table_name='LIBROS';

alter table libros
add constraint CK_libros_precio
check(precio>=0);

alter table libros
add constraint CK_libros_precio
check(precio>=0) novalidate;

select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and
constraint_name='CK_LIBROS_PRECIO';

insert into libros values (3,'Momo','Michael Ende','Alfaragua',-25);

alter table libros
disable constraint CK_libros_precio;

select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';

insert into libros values (3,'Momo','Michael Ende','Alfaragua',-25);

alter table libros
enable novalidate constraint CK_libros_precio;

select constraint_type, status, validated
from user_constraints
where table_name='LIBROS' and constraint_name='CK_LIBROS_PRECIO';
```

## Ejercicios propuestos

Una empresa tiene registrados datos de sus empleados en una tabla llamada "empleados".

1. Elimine la tabla:

```sql
drop table empleados;
```

2. Créela con la siguiente estructura e ingrese los registros siguientes:

```sql
create table empleados (
    codigo number(6),
    documento char(8),
    nombre varchar2(30),
    seccion varchar2(20),
    sueldo number(6,2)
);

insert into empleados
values (1,'22222222','Alberto Acosta','Sistemas',-10);

insert into empleados
values (2,'33333333','Beatriz Benitez','Recursos',3000);

insert into empleados
values (3,'34444444','Carlos Caseres','Contaduria',4000);
```

3. Intente agregar una restricción "check" para asegurarse que no se ingresen valores negativos para el sueldo sin especificar validación ni estado:

```sql
alter table empleados
add constraint CK_empleados_sueldo_positivo
check (sueldo>=0);
```

No se permite porque hay un valor negativo almacenado y por defecto la opción es "validate".

4. Vuelva a intentarlo agregando la opción "novalidate".
```sql
ALTER TABLE empleados
ADD CONSTRAINT CK_empleados_sueldo_positivo
CHECK (sueldo >= 0) NOVALIDATE;

```
5. Intente ingresar un valor negativo para sueldo.
```sql
INSERT INTO empleados
VALUES (4, '45555555', 'David Duarte', 'Ventas', -2000);

```
6. Deshabilite la restricción e ingrese el registro anterior.
```sql
ALTER TABLE empleados
DISABLE CONSTRAINT CK_empleados_sueldo_positivo;

INSERT INTO empleados
VALUES (4, '45555555', 'David Duarte', 'Ventas', -2000);

```
7. Intente establecer una restricción "check" para "seccion" que permita solamente los valores "Sistemas", "Administracion" y "Contaduría" sin especificar validación:

```sql
alter table empleados
add constraint CK_empleados_seccion_lista
check (seccion in ('Sistemas','Administracion','Contaduria'));
```

No lo permite porque existe un valor fuera de la lista.

8. Establezca la restricción anterior evitando que se controlen los datos existentes.
```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT CK_empleados_seccion_lista;

```
9. Vea si las restricciones de la tabla están o no habilitadas y validadas.
Muestra 2 filas, una por cada restricción; ambas son de control, ninguna valida los datos existentes, "CK_empleados_sueldo_positivo" está deshabilitada, la otra habilitada.
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
10. Habilite la restricción deshabilitada.
Note que existe un sueldo que infringe la condición.
```sql
ALTER TABLE empleados
ENABLE VALIDATE CONSTRAINT CK_empleados_sueldo_positivo;

```
11. Intente modificar la sección del empleado "Carlos Caseres" a "Recursos"
No lo permite.
```sql
UPDATE empleados
SET seccion = 'Recursos'
WHERE codigo = 3;

```
12. Deshabilite la restricción para poder realizar la actualización del punto precedente.
```sql
ALTER TABLE empleados
DISABLE CONSTRAINT CK_empleados_seccion_lista;

```
13. Agregue una restricción "primary key" para el campo "codigo" deshabilitada.
```sql
ALTER TABLE empleados
ADD CONSTRAINT PK_empleados_codigo PRIMARY KEY (codigo) DISABLE;

```
14. Ingrese un registro con código existente.
```sql
INSERT INTO empleados
VALUES (3, '56666666', 'Elena Espinoza', 'Recursos', 5000);

```
15. Intente habilitar la restricción.
No se permite porque aun cuando se especifica que no lo haga, Oracle verifica los datos existentes, y existe un código repetido.
```sql
ALTER TABLE empleados
ENABLE CONSTRAINT PK_empleados_codigo;

```
16. Modifique el registro con clave primaria repetida.
```sql
UPDATE empleados
SET codigo = 4
WHERE codigo = 3;

```
17. Habilite la restricción "primary key"
```sql
ALTER TABLE empleados
ENABLE CONSTRAINT PK_empleados_codigo;

```
18. Agregue una restricción "unique" para el campo "documento"
```sql
ALTER TABLE empleados
ADD CONSTRAINT UQ_empleados_documento UNIQUE (documento);

```
19. Vea todas las restricciones de la tabla "empleados"
Muestra las 4 restricciones: 2 de control (1 habilitada y la otra no, no validan datos existentes), 1 "primary key" (habilitada y no valida datos existentes) y 1 única (habilitada y valida los datos anteriores).
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
20. Deshabilite todas las restricciones de "empleados"
```sql
ALTER TABLE empleados
DISABLE ALL CONSTRAINTS;

```
21. Ingrese un registro que viole todas las restricciones.
```sql
INSERT INTO empleados
VALUES (5, NULL, 'Francisco Fernandez', 'Marketing', -500);

```
22. Habilite la restricción "CK_empleados_sueldo_positivo" sin validar los datos existentes.
```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT CK_empleados_sueldo_positivo;

```
23. Habilite la restricción "CK_empleados_seccion_lista" sin validar los datos existentes.
```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT CK_empleados_seccion_lista;

```
24. Intente habilitar la restricción "PK_empleados_codigo" sin validar los datos existentes.
```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT PK_empleados_codigo;

```
25. Intente habilitar la restricción "UQ_empleados_documento" sin validar los datos existentes.
```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT UQ_empleados_documento;

```
26. Elimine el registro que infringe con las restricciones "primary key" y "unique".
```sql
DELETE FROM empleados
WHERE codigo = 5;

```
27. Habilite las restricciones "PK_empleados_codigo" y "UQ_empleados_documento" sin validar los datos existentes.
```sql
ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT PK_empleados_codigo;

ALTER TABLE empleados
ENABLE NOVALIDATE CONSTRAINT UQ_empleados_documento;

```
28. Consulte el catálogo "user_constraints" y analice la información.
```sql
SELECT constraint_name, constraint_type, status, validated
FROM user_constraints
WHERE table_name = 'EMPLEADOS';

```
