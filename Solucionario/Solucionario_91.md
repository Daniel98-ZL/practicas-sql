# 91. Procedimientos Almacenados (crear- ejecutar)

## Practica de laboratorio

Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".

1. Eliminamos la tabla y la creamos:

```sql
drop table empleados;

create table empleados(
    documento char(8),
    nombre varchar2(20),
    apellido varchar2(20),
    sueldo number(6,2),
    cantidadhijos number(2,0),
    fechaingreso date,
    primary key(documento)
);
```

2. Ingrese algunos registros:

```sql
insert into empleados values('22222222','Juan','Perez',200,2,'10/10/1980');
insert into empleados values('22333333','Luis','Lopez',250,0,'01/02/1990');
insert into empleados values('22444444','Marta','Perez',350,1,'02/05/1995');
insert into empleados values('22555555','Susana','Garcia',400,2,'15/12/2018');
insert into empleados values('22666666','Jose Maria','Morales',500,3,'25/08/2015');
```

3. Cree (o reemplace) el procedimiento almacenado llamado "pa_aumentarsueldo" que aumente los sueldos inferiores al promedio en un 20%
```sql
CREATE OR REPLACE PROCEDURE pa_aumentarsueldo IS
    avg_sueldo NUMBER;
BEGIN
    SELECT AVG(sueldo) INTO avg_sueldo FROM empleados;

    UPDATE empleados
    SET sueldo = sueldo * 1.2
    WHERE sueldo < avg_sueldo;
END;

```
4. Ejecute el procedimiento creado anteriormente
```sql
BEGIN
    pa_aumentarsueldo;
END;

```
5. Verifique que los sueldos han aumentado
```sql
SELECT * FROM empleados;

```
6. Ejecute el procedimiento nuevamente
```sql
BEGIN
    pa_aumentarsueldo;
END;

```
7. Verifique que los sueldos han aumentado
```sql
SELECT * FROM empleados;

```
8. Elimine la tabla "empleados_antiguos"
```sql
DROP TABLE empleados_antiguos;

```
9. Cree la tabla "empleados_antiguos"

```sql
create table empleados_antiguos(
    documento char(8),
    nombre varchar2(40)
);
```

10. Cree (o reemplace) un procedimiento almacenado que ingrese en la tabla "empleados_antiguos" el documento, nombre y apellido (concatenados) de todos los empleados de la tabla "empleados" que ingresaron a la empresa hace más de 10 años
```sql
CREATE OR REPLACE PROCEDURE pa_ingresos_antiguos IS
BEGIN
    INSERT INTO empleados_antiguos(documento, nombre)
    SELECT documento, nombre || ' ' || apellido
    FROM empleados
    WHERE fechaingreso < ADD_MONTHS(TRUNC(SYSDATE, 'YEAR'), -120);
END;

```
11. Ejecute el procedimiento creado anteriormente
```sql
BEGIN
    pa_ingresos_antiguos;
END;

```
12. Verifique que la tabla "empleados_antiguos" ahora tiene registros (3 registros)
```sql
SELECT * FROM empleados_antiguos;

```