# 33. Clave primaria compuesta

## Practica de laboratorio

Una playa de estacionamiento almacena cada día los datos de los vehículos que ingresan en la tabla llamada "vehiculos".

Seteamos el formato de "date" para que nos muestre únicamente la hora y los minutos, ya que en esta playa, se almacenan los datos de los vehículos diariamente:

```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
```

Eliminamos la tabla:

```sql
drop table vehiculos;
```

Creamos la tabla estableciendo dos campos como clave primaria:

```sql
create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date,
    horasalida date,
    primary key(patente,horallegada)
);
```

Ingresamos algunos registros:

```sql
insert into vehiculos values('AIC124','a','8:05','12:30');
insert into vehiculos values('CAA258','a','8:05',null);
insert into vehiculos values('DSE367','m','8:30','18:00');
insert into vehiculos values('FGT458','a','9:00',null);
insert into vehiculos values('AIC124','a','16:00',null);
insert into vehiculos values('LOI587','m','18:05','19:55');
```

Si intentamos ingresar un registro con clave primaria repetida:

```sql
insert into vehiculos values('LOI587','m','18:05',null);
```

Aparece un mensaje de error y la inserción no se realiza.

Si ingresamos un registro repitiendo el valor de uno de los campos que forman parte de la clave, si lo acepta:

```sql
insert into vehiculos values('LOI587','m','21:30',null);
```

Recuperamos todos los registros:

```sql
select *from vehiculos;
```

Note que cada registro es único, dos de ellos tienen la misma patente, pero diferente hora de llegada.

Si intentamos actualizar un registro repitiendo la clave primaria:

```sql
 update vehiculos set horallegada='8:05'
  where patente='AIC124' and horallegada='16:00';
```

Aparece un mensaje de error y la actualización no se realiza.

Recordemos que los campos que forman parte de la clave primaria no aceptan valores nulos, aunque no se haya aclarado en la definición de la tabla:

```sql
insert into vehiculos values('HUO690','m',null,null);
```

Si mostramos la estructura de la tabla:

```sql
describe vehiculos;
```

Vemos que los campos que forman parte de la clave primaria (patente y horallegada) tienen "NOT NULL" en la columna "Null", es decir, no admiten valores nulos.

Para ver la clave primaria de una tabla podemos realizar la siguiente consulta:

```sql
select uc.table_name, column_name, position from user_cons_columns ucc
join user_constraints uc
on ucc.constraint_name=uc.constraint_name
where uc.constraint_type='P' and
uc.table_name='VEHICULOS';
```

Nos retorna la siguiente información:

```sh
TABLE_NAME   COLUMN_NAME   POSITION
----------------------------------------
VEHICULOS    PATENTE       1
VEHICULOS    HORALLEGADA   2
```

Los dos campos son clave primaria, "POSITION" indica el orden en que fueron definidos los campos.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'HH24:MI';

drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date,
    horasalida date,
    primary key(patente,horallegada)
);

insert into vehiculos values('AIC124','a','8:05','12:30');
insert into vehiculos values('CAA258','a','8:05',null);
insert into vehiculos values('DSE367','m','8:30','18:00');
insert into vehiculos values('FGT458','a','9:00',null);
insert into vehiculos values('AIC124','a','16:00',null);
insert into vehiculos values('LOI587','m','18:05','19:55');
insert into vehiculos values('LOI587','m','18:05',null);
insert into vehiculos values('LOI587','m','21:30',null);

select *from vehiculos;

update vehiculos set horallegada='8:05'
where patente='AIC124' and horallegada='16:00';

insert into vehiculos values('HUO690','m',null,null);

describe vehiculos;

select uc.table_name, column_name, position from user_cons_columns ucc
join user_constraints uc
on ucc.constraint_name=uc.constraint_name
where uc.constraint_type='P' and
uc.table_name='VEHICULOS';
```

## Ejercicios propuestos

## Ejercicio 01

Un consultorio médico en el cual trabajan 3 médicos registra las consultas de los pacientes en una tabla llamada "consultas".

1. Elimine la tabla:

```sql
drop table consultas;
```
2. La tabla contiene los siguientes datos:

- fechayhora: date not null, fecha y hora de la consulta,
- medico: varchar2(30), not null, nombre del médico (Perez,Lopez,Duarte),
- documento: char(8) not null, documento del paciente,
- paciente: varchar2(30), nombre del paciente,
- obrasocial: varchar2(30), nombre de la obra social (IPAM,PAMI, etc.).
  
3. Setee el formato de "date" para que nos muestre día, mes, año, hora y minutos:

```sql
AL.TER SESSION SET NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

4. Un médico sólo puede atender a un paciente en una fecha y hora determinada. En una fecha y hora determinada, varios médicos atienden a distintos pacientes. Cree la tabla definiendo una clave primaria compuesta:

```sql
create table consultas(
    fechayhora date not null,
    medico varchar2(30) not null,
    documento char(8) not null,
    paciente varchar2(30),
    obrasocial varchar2(30),
    primary key(fechayhora,medico)
);
```

5. Ingrese varias consultas para un mismo médico en distintas horas el mismo día:

```sql
insert into consultas
values ('05/11/2006 8:00','Lopez','12222222','Acosta Betina','PAMI');

insert into consultas
values ('05/11/2006 8:30','Lopez','23333333','Fuentes Carlos','PAMI');
```

6. Ingrese varias consultas para diferentes médicos en la misma fecha y hora:

```sql
insert into consultas
values ('05/11/2006 8:00','Perez','34444444','Garcia Marisa','IPAM');

insert into consultas
values ('05/11/2006 8:00','Duarte','45555555','Pereyra Luis','PAMI');
```

7. Intente ingresar una consulta para un mismo médico en la misma hora el mismo día (mensaje de error)
```sql
INSERT INTO consultas
VALUES (TO_DATE('05/11/2006 8:00', 'DD/MM/YYYY HH24:MI'), 'Lopez', '56666666', 'Rodriguez Ana', 'PAMI');
-- Devuelve un mensaje de error indicando que la clave primaria ya existe

```
8. Intente cambiar la hora de la consulta de "Acosta Betina" por una no disponible ("8:30") (error)
```sql
UPDATE consultas
SET fechayhora = TO_DATE('05/11/2006 8:30', 'DD/MM/YYYY HH24:MI')
WHERE medico = 'Lopez' AND paciente = 'Acosta Betina';
-- Devuelve un mensaje de error indicando que la clave primaria ya existe

```
9. Cambie la hora de la consulta de "Acosta Betina" por una disponible ("9:30")
```sql
UPDATE consultas
SET fechayhora = TO_DATE('05/11/2006 9:30', 'DD/MM/YYYY HH24:MI')
WHERE medico = 'Lopez' AND paciente = 'Acosta Betina';

```
10. Ingrese una consulta para el día "06/11/2006" a las 10 hs. para el doctor "Perez"
```sql
INSERT INTO consultas (fechayhora, medico, documento, paciente, obrasocial)
VALUES (TO_DATE('06/11/2006 10:00', 'DD/MM/YYYY HH24:MI'), 'Perez', '66666666', 'Gómez Ana', 'OSDE');

```
11. Recupere todos los datos de las consultas de "Lopez" (3 registros)
```sql
SELECT *
FROM consultas
WHERE medico = 'Lopez';

```
12. Recupere todos los datos de las consultas programadas para el "05/11/2006 8:00" (2 registros)
```sql
SELECT *
FROM consultas
WHERE fechayhora = TO_DATE('05/11/2006 8:00', 'DD/MM/YYYY HH24:MI');

```
13. Muestre día y mes de todas las consultas de "Lopez"
```sql
SELECT TO_CHAR(fechayhora, 'DD/MM') AS dia_mes
FROM consultas
WHERE medico = 'Lopez';

```
## Ejercicio 02

Un club dicta clases de distintos deportes. En una tabla llamada "inscriptos" almacena la información necesaria.

1. Elimine la tabla "inscriptos":

```sql
drop table inscriptos;
```

2. La tabla contiene los siguientes campos:

- documento del socio alumno: char(8) not null
- nombre del socio: varchar2(30),
- nombre del deporte (tenis, futbol, natación, basquet): varchar2(15) not null,
- año de inscripcion: date,
- matrícula: si la matrícula ha sido o no pagada ('s' o 'n').
  
3. Necesitamos una clave primaria que identifique cada registro. Un socio puede inscribirse en varios deportes en distintos años. Un socio no puede inscribirse en el mismo deporte el mismo año. Varios socios se inscriben en un mismo deporte en distintos años. Cree la tabla con una clave compuesta:

```sql
create table inscriptos(
    documento char(8) not null,
    nombre varchar2(30),
    deporte varchar2(15) not null,
    año date,
    matricula char(1),
    primary key(documento,deporte,año)
);
```

4. Setee el formato de "date" para que nos muestre solamente el año (no necesitamos las otras partes de la fecha ni la hora)
```sql
ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY';

```
5. Inscriba a varios alumnos en el mismo deporte en el mismo año:

```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2005','s');

insert into inscriptos
values ('23333333','Marta Garcia','tenis','2005','s');

insert into inscriptos
values ('34444444','Luis Perez','tenis','2005','n');
```

6. Inscriba a un mismo alumno en varios deportes en el mismo año:

```sql
insert into inscriptos
values ('12222222','Juan Perez','futbol','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','natacion','2005','s');

insert into inscriptos
values ('12222222','Juan Perez','basquet','2005','n');
```

7. Ingrese un registro con el mismo documento de socio en el mismo deporte en distintos años:

```sql
insert into inscriptos
values ('12222222','Juan Perez','tenis','2006','s');

insert into inscriptos
values ('12222222','Juan Perez','tenis','2007','s');
```

8. Intente inscribir a un socio alumno en un deporte en el cual ya esté inscripto en un año en el cual ya se haya inscripto (mensaje de error)
```sql
INSERT INTO inscriptos
VALUES ('12222222', 'Juan Perez', 'tenis', '2005', 's');
-- Devuelve un mensaje de error indicando que la clave primaria ya existe

```
9. Intente actualizar un registro para que la clave primaria se repita (error)
```sql
UPDATE inscriptos
SET documento = '23333333'
WHERE documento = '12222222' AND deporte = 'tenis' AND año = '2005';
-- Devuelve un mensaje de error indicando que la clave primaria ya existe

```
10. Muestre los nombres y años de los inscriptos en "tenis" (5 registros)
```sql
SELECT nombre, año
FROM inscriptos
WHERE deporte = 'tenis';

```
11. Muestre los nombres y deportes de los inscriptos en el año 2005 (6 registros)
```sql
SELECT nombre, deporte
FROM inscriptos
WHERE año = '2005';

```
12. Muestre el deporte y año de todas las inscripciones del socio documento "12222222" (6 registros)
```sql
SELECT deporte, año
FROM inscriptos
WHERE documento = '12222222';

```