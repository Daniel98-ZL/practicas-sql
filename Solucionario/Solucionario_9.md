# 9. Comentarios

## Ejercicios de laboratorio

```sql
 select titulo, autor 
 /*mostramos títulos y
 nombres de los autores*/
 from libros;
```
Salida del Script

```sh
El aleph	Borges
Martin Fierro	Jose Hernandez
Aprenda PHP	Mario Molina
Cervantes y el quijote	Borges
Matematica estas ahi	Adrian Paenza
```
Todo lo que está entre los símbolos "/*" y "*/" no se ejecuta.
