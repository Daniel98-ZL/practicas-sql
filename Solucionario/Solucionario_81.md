# 81. Vistas (create view)

## Practicas de laboratorio

Una empresa almacena la información de sus empleados en dos tablas llamadas "empleados" y "secciones".

Eliminamos las tablas:

```sql
drop table empleados;
drop table secciones;
```

Creamos las tablas:

```sql
create table secciones(
    codigo number(2),
    nombre varchar2(20),
    sueldo number(5,2)
    constraint CK_secciones_sueldo check (sueldo>=0),
    constraint PK_secciones primary key (codigo)    );

create table empleados(
    legajo number(5),
    documento char(8),
    sexo char(1)
    constraint CK_empleados_sexo check (sexo in ('f','m')),
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    seccion number(2) not null,
    cantidadhijos number(2)
    constraint CK_empleados_hijos check (cantidadhijos>=0),
    estadocivil char(10)
    constraint CK_empleados_estadocivil check (estadocivil in ('casado','divorciado','soltero','viudo')),
    fechaingreso date,
    constraint PK_empleados primary key (legajo),
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo),
    constraint UQ_empleados_documento
    unique(documento)
);
```

Ingresamos algunos registros:

```sql
insert into secciones values(1,'Administracion',300);
insert into secciones values(2,'Contaduría',400);
insert into secciones values(3,'Sistemas',500);
insert into empleados values(100,'22222222','f','Lopez','Ana','Colon 123',1,2,'casado','10/10/1990');
insert into empleados values(102,'23333333','m','Lopez','Luis','Sucre 235',1,0,'soltero','02/10/1990');
insert into empleados values(103,'24444444','m','Garcia','Marcos','Sarmiento 1234',2,3,'divorciado','12/07/1998');
insert into empleados values(104,'25555555','m','Gomez','Pablo','Bulnes 321',3,2,'casado','10/09/1998');
insert into empleados values(105,'26666666','f','Perez','Laura','Peru 1254',3,3,'casado','05/09/2000');
```

Eliminamos la vista "vista_empleados". Aún no hemos aprendido a eliminar vistas, lo veremos próximamente:

```sql
drop view vista_empleados;
```

Creamos la vista "vista_empleados", que es resultado de una combinación en la cual se muestran 5 campos:

```sql
create view vista_empleados as
select (apellido||' '||e.nombre) as nombre, sexo, s.nombre as seccion, cantidadhijos
from empleados e
join secciones s
on codigo=seccion;
```

Vemos la información de la vista:

```sql
select *from vista_empleados;
```

Realizamos una consulta a la vista como si se tratara de una tabla:

```sql
select seccion,count(*) as cantidad
from vista_empleados
group by seccion;
```

Eliminamos la vista "vista_empleados_ingreso":

```sql
drop view vista_empleados_ingreso;
```

Creamos otra vista de "empleados" denominada "vista_empleados_ingreso" que almacena la cantidad de empleados por año:

```sql
create view vista_empleados_ingreso (fecha,cantidad) as
select extract(year from fechaingreso),count(*)
from empleados
group by extract(year from fechaingreso);
```

Vemos la información:

```sql
select *from vista_empleados_ingreso;
```

Hemos aprendido que los registros resultantes de una vista no se almacena en la base de datos, sino la definición de la vista, por lo tanto, al modificar las tablas referenciadas por la vista, el resultado de la vista cambia.

Modificamos una fecha en la tabla "empleados" y luego consultamos la vista para verificar que está actualizada:

```sql
 update empleados set fechaingreso='10/09/2000' where fechaingreso='10/09/1998';

select *from vista_empleados_ingreso;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar2(20),
    sueldo number(5,2)
    constraint CK_secciones_sueldo check (sueldo>=0),
    constraint PK_secciones primary key (codigo)
);

create table empleados(
    legajo number(5),
    documento char(8),
    sexo char(1)
    constraint CK_empleados_sexo check (sexo in ('f','m')),
    apellido varchar2(20),
    nombre varchar2(20),
    domicilio varchar2(30),
    seccion number(2) not null,
    cantidadhijos number(2)
    constraint CK_empleados_hijos check (cantidadhijos>=0),
    estadocivil char(10)
    constraint CK_empleados_estadocivil check (estadocivil in ('casado','divorciado','soltero','viudo')),
    fechaingreso date,
    constraint PK_empleados primary key (legajo),
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo),
    constraint UQ_empleados_documento
    unique(documento)
);

insert into secciones values(1,'Administracion',300);
insert into secciones values(2,'Contaduría',400);
insert into secciones values(3,'Sistemas',500);
insert into empleados values(100,'22222222','f','Lopez','Ana','Colon 123',1,2,'casado','10/10/1990');
insert into empleados values(102,'23333333','m','Lopez','Luis','Sucre 235',1,0,'soltero','02/10/1990');
insert into empleados values(103,'24444444','m','Garcia','Marcos','Sarmiento 1234',2,3,'divorciado','12/07/1998');
insert into empleados values(104,'25555555','m','Gomez','Pablo','Bulnes 321',3,2,'casado','10/09/1998');
insert into empleados values(105,'26666666','f','Perez','Laura','Peru 1254',3,3,'casado','05/09/2000');

 -- Eliminamos la vista "vista_empleados" por si ya existe:
drop view vista_empleados;

 -- Creamos la vista "vista_empleados", que es resultado de una combinación en la cual se muestran 5 campos:
create view vista_empleados as
select (apellido||' '||e.nombre) as nombre, sexo, s.nombre as seccion, cantidadhijos
from empleados e
join secciones s
on codigo=seccion;

 -- Vemos la información de la vista:
select * from vista_empleados;

 -- Realizamos una consulta a la vista como si se tratara de una tabla:
select seccion,count(*) as cantidad
from vista_empleados
group by seccion;

 -- Eliminamos la vista "vista_empleados_ingreso":
drop view vista_empleados_ingreso;

 -- Creamos otra vista de "empleados" denominada "vista_empleados_ingreso"
 -- que almacena la cantidad de empleados por año:
create view vista_empleados_ingreso (fecha,cantidad) as
select extract(year from fechaingreso),count(*)
from empleados
group by extract(year from fechaingreso);

 -- Vemos la información:

select * from vista_empleados_ingreso;

 -- Modificamos una fecha en la tabla "empleados"
 -- y luego consultamos la vista para verificar que está actualizada:
update empleados set fechaingreso='10/09/2000' where fechaingreso='10/09/1998';

select * from vista_empleados_ingreso;
```

## Ejercicios propuestos

Un club dicta cursos de distintos deportes. Almacena la información en varias tablas. El director no quiere que los empleados de administración conozcan la estructura de las tablas ni algunos datos de los profesores y socios, por ello se crean vistas a las cuales tendrán acceso.

1. Elimine las tablas y créelas nuevamente:

```sql
drop table inscriptos;
drop table cursos;
drop table socios;
drop table profesores;

create table socios(
    documento char(8) not null,
    nombre varchar2(40),
    domicilio varchar2(30),
    primary key (documento)
);

create table profesores(
    documento char(8) not null,
    nombre varchar2(40),
    domicilio varchar2(30),
    primary key (documento)
);

create table cursos(
    numero number(2),
    deporte varchar2(20),
    dia varchar2(15),
    documentoprofesor char(8),
    constraint CK_inscriptos_dia
    check (dia in('lunes','martes','miercoles','jueves','viernes','sabado')),
    constraint FK_documentoprofesor
    foreign key (documentoprofesor)
    references profesores(documento),
    primary key (numero)
);

create table inscriptos(
    documentosocio char(8) not null,
    numero number(2) not null,
    matricula char(1),
    constraint CK_inscriptos_matricula check (matricula in('s','n')),
    constraint FK_documentosocio
    foreign key (documentosocio)
    references socios(documento),
    constraint FK_numerocurso
    foreign key (numero)
    references cursos(numero),
    primary key (documentosocio,numero)
); 

2. Ingrese algunos registros para todas las tablas:

insert into socios values('30000000','Fabian Fuentes','Caseros 987');
insert into socios values('31111111','Gaston Garcia','Guemes 65');
insert into socios values('32222222','Hector Huerta','Sucre 534');
insert into socios values('33333333','Ines Irala','Bulnes 345');
insert into profesores values('22222222','Ana Acosta','Avellaneda 231');
insert into profesores values('23333333','Carlos Caseres','Colon 245');
insert into profesores values('24444444','Daniel Duarte','Sarmiento 987');
insert into profesores values('25555555','Esteban Lopez','Sucre 1204');
insert into cursos values(1,'tenis','lunes','22222222');
insert into cursos values(2,'tenis','martes','22222222');
insert into cursos values(3,'natacion','miercoles','22222222');
insert into cursos values(4,'natacion','jueves','23333333');
insert into cursos values(5,'natacion','viernes','23333333');
insert into cursos values(6,'futbol','sabado','24444444');
insert into cursos values(7,'futbol','lunes','24444444');
insert into cursos values(8,'basquet','martes','24444444');
insert into inscriptos values('30000000',1,'s');
insert into inscriptos values('30000000',3,'n');
insert into inscriptos values('30000000',6,null);
insert into inscriptos values('31111111',1,'s');
insert into inscriptos values('31111111',4,'s');
insert into inscriptos values('32222222',8,'s');
 ```

3. Elimine la vista "vista_club":

```sql
drop view vista_club;
```

4. Cree una vista en la que aparezca el nombre del socio, el deporte, el día, el nombre del profesor y el estado de la matrícula (deben incluirse los socios que no están inscriptos en ningún deporte, los cursos para los cuales no hay inscriptos y los profesores que no tienen designado deporte también)
```sql
CREATE VIEW vista_club AS
SELECT s.nombre AS "Nombre Socio", c.deporte, c.dia, p.nombre AS "Nombre Profesor", i.matricula AS "Estado Matrícula"
FROM socios s
LEFT JOIN inscriptos i ON s.documento = i.documentosocio
LEFT JOIN cursos c ON i.numero = c.numero
LEFT JOIN profesores p ON c.documentoprofesor = p.documento;

```
5. Muestre la información contenida en la vista (11 registros)
```sql
SELECT * FROM vista_club;

```
6. Realice una consulta a la vista donde muestre la cantidad de socios inscriptos en cada deporte (agrupe por deporte y día) ordenados por cantidad
```sql
SELECT deporte, dia, COUNT("Nombre Socio") AS "Cantidad de Socios Inscriptos"
FROM vista_club
GROUP BY deporte, dia
ORDER BY COUNT("Nombre Socio");

```
7. Muestre (consultando la vista) los cursos (deporte y día) para los cuales no hay inscriptos (3 registros)
```sql
SELECT DISTINCT deporte, dia
FROM vista_club
WHERE "Nombre Socio" IS NULL;

```
8. Muestre los nombres de los socios que no se han inscripto en ningún curso (consultando la vista) (1 registro)
```sql
SELECT "Nombre Socio"
FROM vista_club
WHERE deporte IS NULL;

```
9. Muestre (consultando la vista) los profesores que no tienen asignado ningún deporte aún (1 registro)
```sql
SELECT DISTINCT "Nombre Profesor"
FROM vista_club
WHERE deporte IS NULL;

```
10. Muestre (consultando la vista) el nombre de los socios que deben matrículas (1 registro)
```sql
SELECT "Nombre Socio"
FROM vista_club
WHERE "Estado Matrícula" = 'n';

```
11. Consulte la vista y muestre los nombres de los profesores y los días en que asisten al club para dictar sus clases (9 registros)
```sql
SELECT DISTINCT "Nombre Profesor", dia
FROM vista_club;

```
12. Muestre la misma información anterior pero ordenada por día
```sql
SELECT DISTINCT "Nombre Profesor", dia
FROM vista_club
ORDER BY dia;

```
13. Muestre todos los socios que son compañeros en tenis los lunes (2 registros)
```sql
SELECT "Nombre Socio"
FROM vista_club
WHERE deporte = 'tenis' AND dia = 'lunes';

```
14. Intente crear una vista denominada "vista_inscriptos" que muestre la cantidad de inscriptos por curso, incluyendo el número del curso, el nombre del deporte y el día
```sql
CREATE VIEW vista_inscriptos AS
SELECT c.numero AS "Número Curso", c.deporte, c.dia, COUNT(i.documentosocio) AS "Cantidad de Inscriptos"
FROM cursos c
LEFT JOIN inscriptos i ON c.numero = i.numero
GROUP BY c.numero, c.deporte, c.dia;

```
15. Elimine la vista "vista_inscriptos" y créela para que muestre la cantidad de inscriptos por curso, incluyendo el número del curso, el nombre del deporte y el día
```sql
SELECT c.numero AS "Número Curso", c.deporte, c.dia, COUNT(i.documentosocio) AS "Cantidad de Inscriptos"
FROM cursos c
LEFT JOIN inscriptos i ON c.numero = i.numero
GROUP BY c.numero, c.deporte, c.dia;

```
16. Consulte la vista (9 registros)
```sql
SELECT c.numero AS "Número Curso", c.deporte, c.dia, COUNT(i.documentosocio) AS "Cantidad de Inscriptos"
FROM cursos c
LEFT JOIN inscriptos i ON c.numero = i.numero
GROUP BY c.numero, c.deporte, c.dia;

```