# 109. Disparador de actualización - lista de campos (update trigger)

El trigger de actualización (a nivel de sentencia o de fila) permite incluir una lista de campos. Si se incluye el nombre de un campo (o varios) luego de "update", el trigger se disparará únicamente cuando alguno de esos campos (incluidos en la lista) es actualizado. Si se omite la lista de campos, el trigger se dispara cuando cualquier campo de la tabla asociada es modificado, es decir, por defecto toma todos los campos de la tabla.

La lista de campos solamente puede especificarse en disparadores de actualización, nunca en disparadores de inserción o borrado.

Sintaxis general:

```sql
create or replace trigger NOMBREDISPARADOR
    MOMENTO update of CAMPOS
    on TABLA
    NIVEL--statement o for each row
begin
    CUERPODEL DISPARADOR;
end NOMBREDISPARADOR;
/
```

"CAMPOS" son los campos de la tabla asociada que activarán el trigger si son modificados. Pueden incluirse más de uno, en tal caso, se separan con comas.

Creamos un desencadenador a nivel de fila que se dispara cada vez que se actualiza el campo "precio" de la tabla "libros":

```sql
create or replace trigger tr_actualizar_precio_libros
    before update of precio
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_actualizar_precio_libros;
/
```

Si realizamos un "update" sobre el campo "precio" de "libros", el trigger se dispara. Pero si realizamos un "update" sobre cualquier otro campo, el trigger no se dispara, ya que está definido solamente para el campo "precio".

## Practica de laboratorio

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica el "precio" de un libro.

Eliminamos las tablas:

```sql
drop table control;
drop table libros;
```

Creamos las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

Establecemos el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

Creamos un desencadenador a nivel de fila que se dispare cada vez que se actualiza el campo "precio"; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "precio" de "libros":

```sql
create or replace trigger tr_actualizar_precio_libros
    before update of precio
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_actualizar_precio_libros;
/
```

Veamos qué nos informa el diccionario "user_triggers" respecto del trigger anteriormente creado:

```sql
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_PRECIO_LIBROS';
```

Aumentamos en un 10% el precio de todos los libros de editorial "Nuevo siglo':

```sql
update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';
```

Veamos cuántas veces se disparó el trigger consultando la tabla "control":

```sql
select *from control;
```

El trigger se disparó 2 veces, una vez por cada registro modificado en "libros". Si el trigger hubiese sido creado a nivel de sentencia, el "update" anterior hubiese disparado el trigger 1 sola vez aún cuando se modifican 2 filas.

Modificamos otro campo, diferente de "precio":

```sql
update libros set autor='Lewis Carroll' where autor='Carroll';
```

Veamos si el trigger se disparó consultando la tabla "control":

```sql
select *from control;
```

El trigger no se disparó (no hay nuevas filas en "control"), pues está definido solamente sobre el campo "precio".

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table control;
drop table libros;

create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);

insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);

alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';

create or replace trigger tr_actualizar_precio_libros
    before update of precio
    on libros
    for each row
begin
    insert into control values(user,sysdate);
end tr_actualizar_precio_libros;
/
 
select *from user_triggers where trigger_name ='TR_ACTUALIZAR_PRECIO_LIBROS';

update libros set precio=precio+precio*0.1 where editorial='Nuevo siglo';

select *from control;
 
update libros set autor='Lewis Carroll' where autor='Carroll';

select *from control;
```

## Ejercicios propuestos

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario y la fecha, cada vez que se modifica el precio o la editorial de un libro.

1. Elimine las tablas:

```sql
drop table control;
drop table libros;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

3. Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

4. Establezca el formato de fecha para que muestre "DD/MM/YYYY HH24:MI":

```sql
alter session set NLS_DATE_FORMAT = 'DD/MM/YYYY HH24:MI';
```

5. Cree un desencadenador a nivel de sentencia que se dispare cada vez que se actualicen los campos "precio" y "editorial" ; el trigger debe ingresar en la tabla "control", el nombre del usuario, la fecha y la hora en la cual se realizó un "update" sobre "precio" o "editorial" de "libros"
```sql
create or replace trigger actualizar_libros_trigger
after update of precio, editorial on libros
for each row
begin
    insert into control(usuario, fecha)
    values(user, sysdate);
end;
/

```
6. Vea qué informa el diccionario "user_triggers" respecto del trigger anteriormente creado
```sql
SELECT trigger_name, trigger_type, triggering_event, table_owner, table_name
FROM user_triggers
WHERE table_name = 'LIBROS';

```
7. Aumente en un 10% el precio de todos los libros de editorial "Nuevo siglo'
```sql
update libros
set precio = precio * 1.1
where editorial = 'Nuevo siglo';

```
8. Vea cuántas veces se disparó el trigger consultando la tabla "control"
El trigger se disparó 1 vez.
```sql
select * from control;

```
9. Cambie la editorial, de "Planeta" a "Sudamericana"
```sql
update libros
set editorial = 'Sudamericana'
where editorial = 'Planeta';

```
10. Veamos si el trigger se disparó consultando la tabla "control"
El trigger se disparó.
```sql
select * from control;

```
11. Modifique un campo diferente de los que activan el trigger
```sql
update libros
set titulo = 'Nuevo Título'
where codigo = 100;

```
12. Verifique que el cambio se realizó
```sql
select * from libros where codigo = 100;

```
13. Verifique que el trigger no se disparó
El trigger no se disparó (no hay nuevas filas en "control"), pues está definido únicamente sobre los campos "precio" y "editorial".
```sql
select * from control;

```