# 122. Modelado de base de datos

Desarrolle el modelado de una base de datos para la gestión de un almacén, através de un modelo lógico en 
Data Modeler y genere el diagrama de entidad relacion y el script sql.

```sql
-- Creación de la tabla "Producto"
CREATE TABLE Producto (
    ID_Producto INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Descripcion VARCHAR(255),
    Precio DECIMAL(10,2),
    Cantidad_Stock INT
);

-- Creación de la tabla "Proveedor"
CREATE TABLE Proveedor (
    ID_Proveedor INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Direccion VARCHAR(255),
    Numero_Contacto VARCHAR(20)
);

-- Creación de la tabla "Cliente"
CREATE TABLE Cliente (
    ID_Cliente INT PRIMARY KEY,
    Nombre VARCHAR(100),
    Direccion VARCHAR(255),
    Numero_Contacto VARCHAR(20)
);

-- Creación de la tabla "Pedido"
CREATE TABLE Pedido (
    ID_Pedido INT PRIMARY KEY,
    Fecha_Pedido DATE,
    Estado VARCHAR(50),
    Total DECIMAL(10,2),
    ID_Cliente INT,
    FOREIGN KEY (ID_Cliente) REFERENCES Cliente(ID_Cliente)
);

-- Creación de la tabla "Detalle_Pedido"
CREATE TABLE Detalle_Pedido (
    ID_Detalle INT PRIMARY KEY,
    ID_Pedido INT,
    ID_Producto INT,
    Cantidad INT,
    Precio_Unitario DECIMAL(10,2),
    FOREIGN KEY (ID_Pedido) REFERENCES Pedido(ID_Pedido),
    FOREIGN KEY (ID_Producto) REFERENCES Producto(ID_Producto)
);

```
```sh
Table PRODUCTO creado.


Table PROVEEDOR creado.


Table CLIENTE creado.


Table PEDIDO creado.


Table DETALLE_PEDIDO creado.

```
## Diagrama de entidad relacion
![Diagrama](C:/Users/zapan/Desktop/1.png)
