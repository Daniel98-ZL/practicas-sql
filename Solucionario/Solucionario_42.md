# 42. Restricciones: eliminación (alter table - drop constraint)

## Practica de laboratorio

Trabajamos con la tabla "libros" de una librería.
Eliminamos la tabla:

```sql
drop table libros;
```

La creamos estableciendo el campo código como clave primaria:

```sql
create table libros(
    codigo number(5) not null,
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(6,2)
);
```

Definimos una restricción "primary key" para nuestra tabla "libros" para asegurarnos que cada libro tendrá un código diferente y único:

```sql
alter table libros
add constraint PK_libros_codigo
primary key(codigo);
```

Definimos una restricción "check" para asegurarnos que el precio no será negativo:

```sql
alter table libros
add constraint CK_libros_precio
check (precio>=0);
```

Definimos una restricción "unique" para los campos "titulo", "autor" y "editorial":

```sql
alter table libros
add constraint UQ_libros
unique(titulo,autor,editorial);
```

Vemos las restricciones:

```sql
select *from user_constraints where table_name='LIBROS';
```

Aparecen 4 restricciones:

* 1 "check" que controla que el precio sea positivo;

* 1 "check" , que se creó al definir "not null" el campo "codigo", el nombre le fue dado por Oracle;

* 1 "primary key" y

* 1 "unique".

Eliminamos la restricción "PK_libros_codigo":

```sql
alter table libros
drop constraint PK_LIBROS_CODIGO;
```

Eliminamos la restricción de control "CK_libros_precio":

```sql
alter table libros
drop constraint CK_LIBROS_PRECIO;
```

Vemos si se eliminaron:

```sql
select *from user_constraints where table_name='LIBROS';
```

Aparecen 2 restricciones.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(6,2)
);

alter table libros
add constraint PK_libros_codigo
primary key(codigo);

alter table libros
add constraint CK_libros_precio
check (precio>=0);

alter table libros
add constraint UQ_libros
unique(titulo,autor,editorial);

select *from user_constraints where table_name='LIBROS';

alter table libros
drop constraint PK_LIBROS_CODIGO;

alter table libros
drop constraint CK_LIBROS_PRECIO;

select *from user_constraints where table_name='LIBROS';
```

## Ejercicios propuestos

Una playa de estacionamiento almacena cada día los datos de los vehículos que ingresan en la tabla llamada "vehiculos".

1. Setee el formato de "date" para que nos muestre hora y minutos:

```sql
alter SESSION SET NLS_DATE_FORMAT = 'HH24:MI';
```

2. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table vehiculos;

create table vehiculos(
    patente char(6) not null,
    tipo char(1),--'a'=auto, 'm'=moto
    horallegada date not null,
    horasalida date
);
```

3. Establezca una restricción "check" que admita solamente los valores "a" y "m" para el campo "tipo":

```sql
alter table vehiculos
add constraint CK_vehiculos_tipo
check (tipo in ('a','m'));
```

4. Agregue una restricción "primary key" que incluya los campos "patente" y "horallegada"
```sql
ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
5. Ingrese un vehículo.
```sql
INSERT INTO vehiculos VALUES ('ABC123', 'a', SYSDATE, NULL);

```
6. Intente ingresar un registro repitiendo la clave primaria.
```sql
INSERT INTO vehiculos VALUES ('ABC123', 'a', SYSDATE, NULL);
-- Muestra un error debido a la violación de la clave primaria

```
7. Ingrese un registro repitiendo la patente pero no la hora de llegada.
```sql
INSERT INTO vehiculos VALUES ('ABC123', 'm', SYSDATE, NULL);
-- Se permite ingresar el registro ya que la hora de llegada es diferente

```
8. Ingrese un registro repitiendo la hora de llegada pero no la patente.
```sql
INSERT INTO vehiculos VALUES ('XYZ789', 'a', SYSDATE, NULL);
-- Se permite ingresar el registro ya que la patente es diferente

```
9. Vea todas las restricciones para la tabla "vehiculos"
aparecen 4 filas, 3 correspondientes a restricciones "check" y 1 a "primary key". Dos de las restricciones de control tienen nombres dados por Oracle.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
10. Elimine la restricción "primary key"
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT PK_vehiculos;

```
11. Vea si se ha eliminado.
Ahora aparecen 3 restricciones.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
12. Elimine la restricción de control que establece que el campo "patente" no sea nulo (busque el nombre consultando "user_constraints").
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT <nombre_restriccion>; -- Reemplaza <nombre_restriccion> con el nombre de la restricción de control

```
13. Vea si se han eliminado.
```sql
SELECT constraint_name, constraint_type, search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS';

```
14. Vuelva a establecer la restricción "primary key" eliminada.
```sql
ALTER TABLE vehiculos
ADD CONSTRAINT PK_vehiculos PRIMARY KEY (patente, horallegada);

```
15. La playa quiere incluir, para el campo "tipo", además de los valores permitidos "a" (auto) y "m" (moto), el caracter "c" (camión). No puede modificar la restricción, debe eliminarla y luego redefinirla con los 3 valores.
```sql
ALTER TABLE vehiculos
DROP CONSTRAINT CK_vehiculos_tipo;

ALTER TABLE vehiculos
ADD CONSTRAINT CK_vehiculos_tipo
CHECK (tipo IN ('a', 'm', 'c'));

```
16. Consulte "user_constraints" para ver si la condición de chequeo de la restricción "CK_vehiculos_tipo" se ha modificado.
```sql
SELECT search_condition
FROM user_constraints
WHERE table_name = 'VEHICULOS' AND constraint_name = 'CK_vehiculos_tipo';

```