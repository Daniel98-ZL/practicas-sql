# 18. Operadores aritméticos y de concatenación (columnas calculadas)
## Practica de laborarorio

### Trabajamos con la tabla "libros" de una librería.

#### 1. Eliminamos la tabla:

```sql
drop table libros;
```

#### 2. Creamos la tabla:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    precio number(6,2),
    cantidad number(4) default 0,
    primary key (codigo)
);
```

#### 3. Ingresamos algunos registros:

```sql
insert into libros (codigo,titulo,autor,editorial,precio)
values(1,'El aleph','Borges','Emece',25);

insert into libros
values(2,'Java en 10 minutos','Mario Molina','Siglo XXI',50.40,100);

insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
values(3,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',15,50);
```

#### 4. Queremos saber el monto total en dinero de cada libro:

```sql
select titulo, precio, cantidad, precio*cantidad
from libros;
```

#### 5. Queremos saber el precio de cada libro con un 10% de descuento:

```sql
select titulo, precio, precio-(precio*0.1)
from libros;
```

#### 6. Actualizamos los precios con un 10% de descuento y vemos el resultado:

```sql
update libros set precio=precio-(precio*0.1);
select*from libros;
```

#### 7. Queremos una columna con el título y autor de cada libro:

```sql
select titulo||'-'||autor
from libros;
```

#### 8. Mostramos el título y precio de cada libro concatenados:

```sql
select titulo||' $'||precio
from libros;
```

#### 9. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5),
    titulo varchar2(40) not null,
    autor varchar2(20) default 'Desconocido',
    editorial varchar2(20),
    precio number(6,2),
    cantidad number(4) default 0,
    primary key (codigo)
);

insert into libros (codigo,titulo,autor,editorial,precio)
values(1,'El aleph','Borges','Emece',25);

insert into libros
values(2,'Java en 10 minutos','Mario Molina','Siglo XXI',50.40,100);

insert into libros (codigo,titulo,autor,editorial,precio,cantidad)
values(3,'Alicia en el pais de las maravillas','Lewis Carroll','Emece',15,50);

select titulo, precio, cantidad, precio*cantidad
from libros;

select titulo, precio, precio-(precio*0.1)
from libros;

update libros set precio=precio-(precio*0.1);
select*from libros;

select titulo||'-'||autor
from libros;

select titulo||' $'||precio
from libros;
```

## Ejercicios propuestos

### Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

#### 1. Elimine la tabla:

```sql
drop table articulos;
```

#### 2. Cree la tabla:

```sql
create table articulos(
    codigo number(4),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(8,2),
    cantidad number(3) default 0,
    primary key (codigo)
);
```

#### 3. Ingrese algunos registros:

```sql
insert into articulos
values (101,'impresora','Epson Stylus C45',400.80,20);

insert into articulos
values (203,'impresora','Epson Stylus C85',500,30);

insert into articulos
values (205,'monitor','Samsung 14',800,10);

insert into articulos
values (300,'teclado','ingles Biswal',100,50);
```

#### 4. El comercio quiere aumentar los precios de todos sus artículos en un 15%. Actualice todos los precios empleando operadores aritméticos.
```sql
update articulos set precio=precio+(precio*0.15);
```
#### 5. Vea el resultado.
```sql
SELECT * FROM articulos
```
Salida del Script
```sh
101	impresora	Epson Stylus C45	460,92	20
203	impresora	Epson Stylus C85	575	30
205	monitor	Samsung 14	920	10
300	teclado	ingles Biswal	115	50
```
#### 6. Muestre todos los artículos, concatenando el nombre y la descripción de cada uno de ellos separados por coma.
```sql
select nombre||', '||descripcion
from articulos;
```
Salida del Script

```sh
impresora, Epson Stylus C45
impresora, Epson Stylus C85
monitor, Samsung 14
teclado, ingles Biswal
```
#### 7. Reste a la cantidad de todas las impresoras, el valor 5, empleando el operador aritmético menos ("-")
```sql
update articulos set cantidad=cantidad-5 WHERE nombre='impresora';
```
#### 8. Recupere todos los datos de las impresoras para verificar que la actualización se realizó.
```sql
SELECT * FROM articulos WHERE nombre='impresora';
```
Salida del Script
```sh
101	impresora	Epson Stylus C45	460,92	15
203	impresora	Epson Stylus C85	575	25
```
#### 9. Muestre todos los artículos concatenado los campos para que aparezcan de la siguiente manera "Cod. 101: impresora Epson Stylus C45 $460,92 (15)"
```sql
select 'Cod. '||codigo||': '||nombre||' '||descripcion||' $'||precio||' ('||cantidad||')'
from articulos;
```
Salida del Script
```sh
Cod. 101: impresora Epson Stylus C45 $460,92 (15)
Cod. 203: impresora Epson Stylus C85 $575 (25)
Cod. 205: monitor Samsung 14 $920 (10)
Cod. 300: teclado ingles Biswal $115 (50)
```
