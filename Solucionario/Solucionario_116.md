# 116. Errores definidos por el usuario en trigger

## Práctica de laboratorio

Una librería almacena en "libros" los datos de sus libros para la venta. En una tabla denominada "control" almacena el nombre del usuario y la fecha, cada vez que se actualiza la tabla "libros".

Eliminamos las tablas "libros" y "control":

```sql
drop table libros;
drop table control;
```

Creamos las tablas:

```sql
create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
```

Ingresamos algunos registros en "libros":

```sql
insert into libros values (101,'Uno','Richard Bach','Planeta',25); 
insert into libros values (102,'Matematica estas ahi','Paenza','Nuevo siglo',12); 
insert into libros values (103,'El aleph','Borges','Emece',28);
insert into libros values (104,'Aprenda PHP','Molina','Nuevo siglo',55); 
insert into libros values (105,'El experto en laberintos','Gaskin','Planeta',23); 
```

Creamos un trigger de actualización a nivel de fila sobre la tabla "libros" que se dispare antes que se ejecute una actualización. Ante cualquier modificación de los registros de "libros", se debe ingresar en la tabla "control", el nombre del usuario que realizó la actualización y la fecha. Pero, controlamos que NO se permita modificar el campo "codigo", en caso de suceder, la acción no debe realizarse y debe mostrarse un mensaje de error indicándolo:

```sql
create or replace trigger tr_actualizar_libros
before update
on libros
for each row
begin
    if updating('codigo') then
        raise_application_error(-20001,'No se puede modificar el código de los libros');
    else
       insert into control values(user,sysdate);
    end if;
end;
/
```

Aumentamos el precio de todos los libros de editorial "Planeta":

```sql
update libros set precio=precio+precio*0.1  where editorial='Planeta';
```

Controlamos que los precios se han modificado y el trigger se ha disparado almacenando en "control" 2 registros:

```sql
select *from libros;
select *from control;
```

Intentamos modificar el código de un libro:

```sql
update libros set codigo=109 where codigo=101;
```

Note que muestra el mensaje de error definido por nosotros. El trigger se disparó y se ejecutó "raise_application_error", por lo tanto, el código no se modificó. Controlamos que el código no se modificó:

```sql
select *from libros;
```

Reemplazamos el trigger creado anteriormente para que ahora se dispare DESPUES (after) de cualquier modificación de los registros de "libros"; debe realizar lo mismo que el anterior, ingresar en la tabla "control", el nombre del usuario que realizó la actualización y la fecha. Pero, controlando que NO se permita modificar el campo "codigo", en caso de suceder, la acción no debe revertirse y debe mostrarse un mensaje de error indicándolo:

```sql
create or replace trigger tr_actualizar_libros
after update
on libros
for each row
begin
    if updating('codigo') then
        raise_application_error(-20001,'No se puede modificar el código de los libros');
    else
       insert into control values(user,sysdate);
    end if;
end;
/
```

Intentamos modificar el código de un libro:

```sql
update libros set codigo=109 where codigo=101;
```

Note que muestra el mensaje de error definido por nosotros. El trigger fue definido "after", es decir, se disparó luego de ejecutarse la actualización, pero también se ejecutó "raise_application_error", por lo tanto, la sentencia "update" se deshizo.

Controlamos que el código no se modificó:

```sql
select *from libros;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table control;

create table libros(
    codigo number(5),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date
);
 
insert into libros values (101,'Uno','Richard Bach','Planeta',25); 
insert into libros values (102,'Matematica estas ahi','Paenza','Nuevo siglo',12); 
insert into libros values (103,'El aleph','Borges','Emece',28);
insert into libros values (104,'Aprenda PHP','Molina','Nuevo siglo',55); 
insert into libros values (105,'El experto en laberintos','Gaskin','Planeta',23); 

create or replace trigger tr_actualizar_libros
before update
on libros
for each row
begin
    if updating('codigo') then
        raise_application_error(-20001,'No se puede modificar el código de los libros');
    else
        insert into control values(user,sysdate);
    end if;
end;
/
 
 -- Aumentamos el precio de todos los libros de editorial "Planeta":
update libros set precio=precio+precio*0.1  where editorial='Planeta';
 -- Controlamos que los precios se han modificado y el trigger se ha disparado
 -- almacenando en "control" 2 registros:

select *from libros;
select *from control;

 -- Intentamos modificar el código de un libro:
update libros set codigo=109 where codigo=101;
 -- Note que muestra el mensaje de error definido por nosotros.
 
 -- Controlamos que el código no se modificó:
select *from libros;

 -- Reemplazamos el trigger creado anteriormente para que ahora se dispare DESPUES (after)
 -- de cualquier modificación de los registros de "libros";
create or replace trigger tr_actualizar_libros
after update
on libros
for each row
begin
    if updating('codigo') then
        raise_application_error(-20001,'No se puede modificar el código de los libros');
    else
        insert into control values(user,sysdate);
    end if;
end;
/
 
 -- Intentamos modificar el código de un libro:
update libros set codigo=109 where codigo=101;

 -- Controlamos que el código no se modificó:
select *from libros;
```

## Ejercicios propuestos

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados" y en otra tabla llamada "control", guarda el nombre del usuario, la fecha y el tipo de operación que se realiza en la tabla "empleados".

1. Elimine las tablas "empleados" y "control":

```sql
drop table empleados;
drop table control;
```

2. Cree las tablas:

```sql
create table empleados(
    documento char(8),
    apellido varchar2(30),
    nombre varchar2(30),
    domicilio varchar2(30),
    seccion varchar2(20),
    sueldo number(8,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(30)
);
```

3. Ingrese algunos registros:

```sql
insert into empleados values('22222222','Acosta','Ana','Avellaneda 11','Secretaria',1800);
insert into empleados values('23333333','Bustos','Betina','Bulnes 22','Gerencia',5000);
insert into empleados values('24444444','Caseres','Carlos','Colon 333','Contaduria',3000);
```

4. Cree un trigger de inserción sobre "empleados" que guarde en "control" el nombre del usuario que ingresa datos, la fecha y "insercion", en el campo "operacion". Pero, si el sueldo que se intenta ingresar supera los $5000, debe mostrarse un mensaje de error y deshacer la transacción
```sql
create or replace trigger tr_insertar_empleados
before insert on empleados
for each row
declare
    v_usuario varchar2(30) := sys_context('userenv', 'session_user');
begin
    if :new.sueldo > 5000 then
        raise_application_error(-20001, 'No se puede ingresar un sueldo mayor a $5000');
    else
        insert into control(usuario, fecha, operacion)
        values (v_usuario, sysdate, 'insercion');
    end if;
end;
/

```
5. Cree un trigger de borrado sobre "empleados" que guarde en "control" los datos requeridos (en "operacion" debe almacenar "borrado". Si se intenta eliminar un empleado de la sección "gerencia", debe aparecer un mensaje de error y deshacer la operación
```sql
create or replace trigger tr_borrar_empleados
before delete on empleados
for each row
declare
    v_usuario varchar2(30) := sys_context('userenv', 'session_user');
begin
    if :old.seccion = 'Gerencia' then
        raise_application_error(-20001, 'No se puede eliminar un empleado de la sección "Gerencia"');
    else
        insert into control(usuario, fecha, operacion)
        values (v_usuario, sysdate, 'borrado');
    end if;
end;
/

```
6. Cree un trigger de actualización. Ante cualquier modificación de los registros de "empleados", se debe ingresar en la tabla "control", el nombre del usuario que realizó la actualización, la fecha y "actualizacion". Pero, controlamos que NO se permita modificar el campo "documento", en caso de suceder, la acción no debe realizarse y debe mostrarse un mensaje de error indicándolo
```sql
create or replace trigger tr_actualizar_empleados
before update on empleados
for each row
declare
    v_usuario varchar2(30) := sys_context('userenv', 'session_user');
begin
    if updating('documento') then
        raise_application_error(-20001, 'No se puede modificar el campo "documento"');
    else
        insert into control(usuario, fecha, operacion)
        values (v_usuario, sysdate, 'actualizacion');
    end if;
end;
/

```
7. Intente ingresar un empleado con sueldo superior a $5000:

```sql
insert into empleados values('25555555','Duarte','Dario','Dominicana 444','Secretaria',5800);
```

Note que muestra el mensaje de error definido por usted.

8. Ingrese un empleado con valores permitidos:

```sql
insert into empleados values('25555555','Duarte','Dario','Dominicana 444','Secretaria',2800);
```

9. Intente borrar un empleado de "gerencia"
Aparece un mensaje de error.
```sql
delete from empleados where seccion = 'Gerencia';

```
10. Elimine un empleado que no sea de "Gerencia"
```sql
delete from empleados where seccion <> 'Gerencia';

```
11. Intente modificar el documento de un empleado
Mensaje de error.
```sql
update empleados set documento = '26666666' where documento = '25555555';

```
12. Modifique un campo diferente de "documento"
```sql
update empleados set sueldo = 3200 where documento = '25555555';

```
13. Vea que se ha almacenado hasta el momento en "control"
Debe haber 3 registros, de inserción, de borrado y actualización.
```sql
SELECT * FROM control;

```