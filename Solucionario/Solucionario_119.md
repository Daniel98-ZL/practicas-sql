# 119. 	Permiso de conexión

## Práctica de laboratorio

Una escuela necesita crear 3 usuarios diferentes en su base de datos. Uno denominado "director", otro "profesor" y un tercero "alumno".

Conéctese como administrador (por ejemplo "system").

1. Elimine el usuario "director", porque si existe, aparecerá un mensaje de error:

```sql
drop user director cascade;
```

2. Cree un usuario "director", con contraseña "dire" y 100M de espacio en "system"
```sql
CREATE USER director IDENTIFIED BY dire DEFAULT TABLESPACE system QUOTA 100M ON system;

```
3. Elimine el usuario "profesor":

```sql
drop user profesor cascade;
```

4. Cree un usuario "profesor", con contraseña "profe" y espacio en "system"
```sql
CREATE USER profesor IDENTIFIED BY profe DEFAULT TABLESPACE system;

```
5. Elimine el usuario "alumno" y luego créelo con contraseña "alu" y espacio en "system"
```sql
DROP USER alumno CASCADE;

CREATE USER alumno IDENTIFIED BY alu DEFAULT TABLESPACE system;

```
6. Consulte el diccionario "dba_users" y analice la información que nos muestra
Deben aparecer los tres usuarios creados anteriormente.
```sql
SELECT username, account_status, default_tablespace, temporary_tablespace
FROM dba_users
WHERE username IN ('director', 'profesor', 'alumno');

```
7. Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a nuestros tres usuarios
Nos muestra que estos usuarios no tienen ningún privilegio concedido.
```sql
SELECT grantee, privilege
FROM dba_sys_privs
WHERE grantee IN ('DIRECTOR', 'PROFESOR', 'ALUMNO');

```
8. Conceda a "director" permiso para conectarse
```sql
GRANT CREATE SESSION TO director;

```
9. Conceda a "profesor" permiso para conectarse
```sql
GRANT CREATE SESSION TO profesor;

```
10. Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a nuestros 3 usuarios
```sql
SELECT grantee, privilege
FROM dba_sys_privs
WHERE grantee IN ('DIRECTOR', 'PROFESOR', 'ALUMNO');

```
11. Abra una nueva conexión para "director". Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (director)
```sql
CONNECT director/dire@<nombre_del_host>:<puerto>/<nombre_de_la_instancia>

```
12. En la conexión de "director" consulte sus privilegios
```sql
SELECT * FROM session_privs;

```
13. Obtenga el nombre del usuario conectado
```sql
SELECT USER FROM dual;

```
14. Vuelva a la conexión "system" (la otra solapa) y compruebe el usuario actual
```sql
SELECT USER FROM dual;

```
15. Intente abrir una nueva conexión para el usuario inexistente. Debe aparecer un mensaje de error y denegarse la conexión. Cancele.
```sql
CONNECT usuario_inexistente/contraseña@<nombre_del_host>:<puerto>/<nombre_de_la_instancia>

```
16. Intente abrir una nueva conexión para el usuario "profesor" colocando una contraseña incorrecta. Debe aparecer un mensaje de error y denegarse la conexión. Cancele.
```sql
CONNECT profesor/contraseña_incorrecta@<nombre_del_host>:<puerto>/<nombre_de_la_instancia>

```
17. Abra una nueva conexión para "profesor" colocando los datos correctos. Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (profesor).
```sql
CONNECT profesor@<nombre_del_host>:<puerto>/<nombre_de_la_instancia>

```
18. Intentemos abrir una nueva conexión para el usuario "alumno", el cual no tiene permiso. Un mensaje de error indica que el usuario "alumno" no tiene permiso "create session" por lo cual se deniega la conexión. Cancele.
```sql
SELECT COUNT(*) AS "Permiso de Conexión"
FROM dba_sys_privs
WHERE privilege = 'CREATE SESSION'
AND grantee = 'ALUMNO';

```
19. Conceda a "alumno" permiso de conexión
```sql
GRANT CREATE SESSION TO alumno;

```
20. Consulte el diccionario "dba_sys_privs" para encontrar los privilegios concedidos a "alumno"
```sql
SELECT privilege
FROM dba_sys_privs
WHERE grantee = 'ALUMNO';

```
21. Abra una nueva conexión para "ALUMNO". Se debe abrir una nueva solapa (nueva conexión) con el nombre del usuario (profesor)
```sql
SELECT USER AS "Usuario Conectado" FROM dual;

```
22. Consulte el diccionario "user_sys_privs"
```sql
SELECT privilege
FROM user_sys_privs
WHERE grantee = 'ALUMNO';

```
23. Compruebe que está en la sesión de "alumno"
```sql
SELECT USER FROM dual;

```