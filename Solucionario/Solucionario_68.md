# 68. Modificar campos (alter table - modify)

## Practica de laboratorio

Trabajamos con la tablas "libros" y "editoriales" de una librería.
Eliminamos las tablas:

```sql
drop table libros;
drop table editoriales;
```

Creamos las tablas:

```sql
create table editoriales(
    codigo number(3),
    nombre varchar2(30),
    primary key(codigo)
);

create table libros(
    titulo varchar2(40),
    editorial number(3),
    autor varchar2(30),
    precio number(4),
    constraint FK_libros_editorial
    foreign key(editorial)
    references editoriales(codigo)
);
```

Modificamos el campo precio para que tome valores de 6 dígitos incluyendo 2 decimales y acepte valores nulos:

```sql
alter table libros
modify precio number(6,2);
```

Verificamos el cambio viendo la estructura de la tabla:

```sql
describe libros;
```

Ingresamos algunos registros:

```sql
insert into editoriales values(1, 'Emece');
insert into libros values('Uno',1,'Richard Bach',24.6);
```

Intentamos modificar el campo "precio" a "varchar(8)":

```sql
alter table libros modify  precio varchar(8);
```

No lo permite, porque existe un registro con un valor numérico en tal campo.

Actualizamos el registro de "libros" con precio no nulo a nulo:

```sql
update libros set precio= null;
```

Ahora si podemos cambiar el tipo de dato de "precio", los registros existentes contienen "null" en tal campo:

```sql
alter table libros modify  precio varchar(8);
```

Verificamos el cambio:

```sql
describe libros;
```

Intentamos modificar el campo "codigo" de "editoriales" a "char(3)":

```sql
alter table editoriales modify  codigo char(3);
```

No lo permite porque tal campo es referenciado por una clave externa.

Modificamos un atributo del campo "codigo" de "editoriales":

```sql
alter table editoriales modify  codigo number(4);
```

Oracle permite el cambio pues no afecta a la restricción.

Intentamos redefinir "precio" para que no acepte valores nulos:

```sql
alter table libros
modify  precio not null;
```

No lo permite porque existe un registro con valor nulo en "precio".

Eliminamos el registro y modificamos el campo "precio" a "no nulo":

```sql
delete from libros;

alter table libros
modify precio not null;
```

Intentamos redefinir como no nulo el campo "codigo" de "editoriales":

```sql
alter table editoriales
modify codigo not null;
```

No aparece mensaje de error, pero si verificamos la estructura de la tabla veremos que continua siendo "not null", ya que es clave primaria:

```sql
describe editoriales;
```

Redefinimos el campo "precio" como number(6,2), con un valor por defecto 0:

```sql
alter table libros
modify precio number(6,2) default 0;
```

Oracle permite modificar el campo "precio" a "char(8)". Si luego ingresamos un registro sin valor para "precio", guardará el valor por defecto (0) convertido a cadena ('0'):

```sql
alter table libros
modify precio char(8) default 0;

insert into libros values('El aleph',1,'Borges',default);

select *from libros;
```

Redefinimos el valor por defecto del campo "precio" (que ahora es de tipo char) a "cero":

```sql
alter table libros
modify precio default 'cero';
```

Oracle no permite modificar el campo "precio" a "number(8,2)" porque si luego ingresamos un registro sin valor para tal campo, el valor por defecto ('cero') no podrá convertirse a número:

```sql
alter table libros
modify precio number(8,2);
```

Mensaje de error.

Modificamos el valor por defecto para que luego pueda ser convertido:

```sql
alter table libros
modify precio default '0';
```

Vaciamos la tabla:

```sql
truncate table libros;
```

Oracle permite modificar el campo "precio" a "number(8,2)" porque si luego ingresamos un registro sin valor para tal campo, el valor por defecto ('0') podrá convertirse a número (0):

```sql
alter table libros
modify precio number(8,2);
```

Oracle permite modificar el campo "precio" a "char(8)". Si luego ingresamos un registro sin valor para "precio", guardará el valor por defecto (0) convertido a cadena ('0'):

```sql
alter table libros
modify precio char(8) default 0;

insert into libros values('El aleph',1,'Borges',default);

select *from libros;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;
drop table editoriales;

create table editoriales(
    codigo number(3),
    nombre varchar2(30),
    primary key(codigo)
);

create table libros(
    titulo varchar2(40),
    editorial number(3),
    autor varchar2(30),
    precio number(4),
    constraint FK_libros_editorial
    foreign key(editorial)
    references editoriales(codigo)
);

 -- Modificamos el campo precio para que tome valores
 -- de 6 dígitos incluyendo 2 decimales y acepte valores nulos:
alter table libros
modify precio number(6,2);

 -- Verificamos el cambio viendo la estructura de la tabla:
describe libros;

 -- Ingresamos algunos registros:
insert into editoriales values(1, 'Emece');
insert into libros values('Uno',1,'Richard Bach',24.6);

 -- Intentamos modificar el campo "precio" a "varchar(8)":
alter table libros modify  precio varchar(8);
 -- No lo permite, porque existe un registro con un valor numérico en tal campo.

 -- Actualizamos el registro de "libros" con precio no nulo a nulo:
update libros set precio= null;

 -- Ahora si podemos cambiar el tipo de dato de "precio",
 -- los registros existentes contienen "null" en tal campo:
alter table libros modify  precio varchar(8);

 -- Verificamos el cambio:
describe libros;

 -- Intentamos modificar el campo "codigo" de "editoriales" a "char(3)":
alter table editoriales modify  codigo char(3);
 -- No lo permite porque tal campo es referenciado por una clave externa.

 -- Modificamos un atributo del campo "codigo" de "editoriales":
alter table editoriales modify  codigo number(4);
 -- Oracle permite el cambio pues no afecta a la restricción.

 -- Intentamos redefinir "precio" para que no acepte valores nulos:
alter table libros
modify  precio not null;
 -- No lo permite porque existe un registro con valor nulo en "precio".

 -- Eliminamos el registro y modificamos el campo "precio" a "no nulo":
delete from libros;
alter table libros
modify precio not null;

 -- Intentamos redefinir como no nulo el campo "codigo" de "editoriales":
alter table editoriales
modify codigo not null;

 --No aparece mensaje de error, pero si verificamos la estructura de la tabla
 -- veremos que continua siendo "not null", ya que es clave primaria:
describe editoriales;

 -- Redefinimos el campo "precio" como number(6,2), con un valor por defecto 0:
alter table libros
modify precio number(6,2) default 0;
 -- Oracle permite modificar el campo "precio" a "char(8)".

alter table libros
modify precio char(8) default 0;

insert into libros values('El aleph',1,'Borges',default);

select *from libros;

 -- Redefinimos el valor por defecto del campo "precio"
 -- (que ahora es de tipo char) a "cero":
alter table libros
modify precio default 'cero';

 -- Oracle no permite modificar el campo "precio" a "number(8,2)"
 -- porque si luego ingresamos un registro sin valor para tal campo,
 -- el valor por defecto ('cero') no podrá convertirse a número:
alter table libros
modify precio number(8,2);
 -- Mensaje de error.

 -- Modificamos el valor por defecto para que luego pueda ser convertido:
alter table libros
modify precio default '0';

 -- Vaciamos la tabla:
truncate table libros;

 -- Oracle permite modificar el campo "precio" a "number(8,2)" porque si luego
 -- ingresamos un registro sin valor para tal campo,
 -- el valor por defecto ('0') podrá convertirse a número (0):
alter table libros
modify precio number(8,2);

 -- Oracle permite modificar el campo "precio" a "char(8)".
 -- Si luego ingresamos un registro sin valor para "precio",
 -- guardará el valor por defecto (0) convertido a cadena ('0'):
alter table libros
modify precio char(8) default 0;

insert into libros values('El aleph',1,'Borges',default);

select * from libros;
```

## Ejercicios propuestos

Trabaje con una tabla llamada "empleados" y "secciones".

1. Elimine las tablas y créelas:

```sql
drop table empleados;
drop table secciones;

create table secciones(
    codigo number(2),
    nombre varchar(20),
    primary key (codigo)
);

create table empleados(
    apellido varchar2(20) not null,
    nombre varchar2(20),
    domicilio varchar2(30),
    seccion number(2),
    fechaingreso date,
    telefono number(7),
    constraint FK_empleados_seccion
    foreign key (seccion)
    references secciones(codigo)
    on delete set null
);
```

2. Modifique el tipo de dato del campo "telefono" a varchar(11)
Oracle lo permite porque la tabla está vacía; si no lo estuviese, no lo permitiría.
```sql
ALTER TABLE empleados
MODIFY telefono varchar(11);

```
3. Ingrese algunos registros en ambas tablas

```sql
insert into secciones values(8,'Secretaria');
insert into secciones values(9,'Contaduria');
insert into secciones values(10,'Sistemas');
insert into empleados values('Lopez','Luciano','Colon 123',8,'10/10/1980','4819977');
insert into empleados values('Gonzalez',null,'Avellaneda 222',9,'01/05/1990','4515566');
```

4. Modifique el campo "nombre" de "empleados" para que permita cadenas variables de 10 caracteres (menor longitud) y luego verifique el cambio.
Oracle lo permite, porque los valores en tal campo son menores a 10.
```sql
ALTER TABLE empleados
MODIFY nombre varchar2(10);

```
5. Intente modificar el campo "nombre" de "empleados" para que permita cadenas variables de 6 caracteres (menor longitud)
Oracle no lo permite, porque un valor en tal campo consta de 7 caracteres.
```sql
-- Esta consulta arrojará un error porque un valor en el campo "nombre" consta de 7 caracteres
ALTER TABLE empleados
MODIFY nombre varchar2(6);

```
6. Elimine el registro correspondiente a "Lopez Luciano"
```sql
DELETE FROM empleados
WHERE apellido = 'Lopez' AND nombre = 'Luciano';

```
7. Modifique el campo "nombre" de "empleados" para que permita cadenas variables de 6 caracteres
Oracle lo permite, la tabla no está vacía pero los registros contienen valor nulo en el campo "nombre".
```sql
ALTER TABLE empleados
MODIFY nombre varchar2(6);

```
8. Intente cambiar el tipo de dato del campo "codigo" de "secciones" a char(2)
Oracle no lo permite porque tal campo es referenciado por una clave externa.
```sql
-- Esta consulta arrojará un error porque el campo "codigo" es referenciado por una clave externa
ALTER TABLE secciones
MODIFY codigo char(2);

```
9. Cambie la longitud del campo "codigo" de "secciones" a 3.
Oracle lo permite porque el cambio no afecta la restricción "foreign key" que referencia el campo "codigo".
```sql
-- Esta consulta arrojará un error porque el campo "codigo" es referenciado por una clave externa
ALTER TABLE secciones
MODIFY codigo char(2);

```
10. Intente modificar el campo "nombre" de "empleados" para que no admita valores nulos.
Mensaje de error, la tabla contiene valores nulos en tal campo.
```sql

ALTER TABLE empleados
MODIFY nombre varchar2(20) not null;

```
11. Modifique el valor nulo por uno válido del campo "nombre" de "empleados" y luego realice la modificación del punto anterior.
```sql
UPDATE empleados
SET nombre = 'NuevoNombre'
WHERE nombre IS NULL;

ALTER TABLE empleados
MODIFY nombre varchar2(20) not null;

```
12. Verifique que "nombre" ya no admite valores nulos.
```sql

DESCRIBE empleados;

```