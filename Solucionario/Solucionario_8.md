# 8. Actualizar registros (update)

## Ejercicios de laboratorio

### Trabajamos con la tabla "usuarios".
#### 1. Eliminamos la tabla:

```sql
drop table usuarios;
```
Salida del Script

```sh
Table USUARIOS borrado.
```
#### 2. Creamos la tabla:

```sql
create table usuarios(
    nombre varchar2(20),
    clave varchar2(10)
);
```
Salida del Script

```sh
Table USUARIOS creado.
```
#### 3. Ingresamos algunos registros:

```sql
insert into usuarios (nombre,clave) values ('Marcelo','River');
insert into usuarios (nombre,clave) values ('Susana','chapita');
insert into usuarios (nombre,clave) values ('Carlosfuentes','Boca');
insert into usuarios (nombre,clave) values ('Federicolopez','Boca');
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
#### 4. Cambiaremos los valores de todas las claves, por la cadena "RealMadrid":

```sql
update usuarios set clave='RealMadrid';
```
Salida del Script

```sh
4 filas actualizadas.
```
Un mensaje indica que se actualizaron 4 registros.

#### 5. El cambio afectó a todos los registros, veámoslo:

```sql
select *from usuarios;
```
Salida del Script

```sh
Marcelo	RealMadrid
Susana	RealMadrid
Carlosfuentes	RealMadrid
Federicolopez	RealMadrid
```
#### 6. Necesitamos cambiar el valor de la clave del usuario llamado "Federicolopez" por "Boca":

```sql
update usuarios set clave='Boca' where nombre='Federicolopez';
```
Salida del Script

```sh
1 fila actualizadas.
```
#### 7. Verifiquemos que la actualización se realizó:

```sql
select *from usuarios;
```
Salida del Script

```sh
Marcelo	RealMadrid
Susana	RealMadrid
Carlosfuentes	RealMadrid
Federicolopez	Boca
```
#### 8. Vimos que si Oracle no encuentra registros que cumplan con la condición del "where", un mensaje indica que ningún registro se modifica:

```sql
update usuarios set clave='payaso' where nombre='JuanaJuarez';
```
Salida del Script

```sh
0 filas actualizadas.
```
#### 9. Para actualizar varios campos en una sola instrucción empleamos:

```sql
update usuarios set nombre='Marceloduarte', clave='Marce' where nombre='Marcelo';
```
Salida del Script

```sh
1 fila actualizadas.
```
#### 10. Verifiquemos que la actualización se realizó:

```sql
select *from usuarios;
```
Salida del Script

```sh
Marceloduarte	Marce
Susana	RealMadrid
Carlosfuentes	RealMadrid
Federicolopez	Boca
```
#### 11. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table usuarios;

create table usuarios(
    nombre varchar2(20),
    clave varchar2(10)
);

insert into usuarios (nombre,clave) values ('Marcelo','River');
insert into usuarios (nombre,clave) values ('Susana','chapita');
insert into usuarios (nombre,clave) values ('Carlosfuentes','Boca');
insert into usuarios (nombre,clave) values ('Federicolopez','Boca');

update usuarios set clave='RealMadrid';

select *from usuarios;

update usuarios set clave='Boca' where nombre='Federicolopez';

select *from usuarios;

update usuarios set clave='payaso' where nombre='JuanaJuarez';

update usuarios set nombre='Marceloduarte', clave='Marce' where nombre='Marcelo';

select *from usuarios;
```

## Ejercicios propuestos

### Ejercicio 01

#### Trabaje con la tabla "agenda" que almacena los datos de sus amigos.

##### 1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table agenda;
```
Salida del Script
```sh
Table AGENDA borrado.
```
```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);
```
Salida del Script

```sh
Table AGENDA creado.
```

##### 2. Ingrese los siguientes registros:

```sql
insert into agenda (apellido,nombre,domicilio,telefono) values ('Acosta','Alberto','Colon 123','4234567');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Juarez','Juan','Avellaneda 135','4458787');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Maria','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Lopez','Jose','Urquiza 333','4545454');
insert into agenda (apellido,nombre,domicilio,telefono) values ('Suarez','Susana','Gral. Paz 1234','4123456');
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 3. Modifique el registro cuyo nombre sea "Juan" por "Juan Jose" (1 registro actualizado)

```sql
update agenda set nombre='Juan Jose' where nombre='Juan';
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 4. Actualice los registros cuyo número telefónico sea igual a "4545454" por "4445566" (2 registros)

```sql
update agenda set telefono='4445566' where telefono='4545454';
```
Salida del Script

```sh
2 filas actualizadas.
```
##### 5. Actualice los registros que tengan en el campo "nombre" el valor "Juan" por "Juan Jose" (ningún registro afectado porque ninguno cumple con la condición del "where")

```sql
update agenda set nombre='Juan Jose' where nombre='Juan';
```
Salida del Script

```sh
0 filas actualizadas.
```
### Ejercicio 02

#### Trabaje con la tabla "libros" de una librería.

##### 1. Elimine la tabla y créela con los siguientes campos: titulo (cadena de 30 caracteres de longitud), autor (cadena de 20), editorial (cadena de 15) y precio (entero no mayor a 999.99):

```sql
drop table libros;
```
Salida del Script

```sh
Table LIBROS borrado.
```
```sql
create table libros (
    titulo varchar2(30),
    autor varchar2(20),
    editorial varchar2(15),
    precio number(5,2)
);
```
Salida del Script

```sh
Table LIBROS creado.
```
##### 2. Ingrese los siguientes registros:

```sql
insert into libros (titulo, autor, editorial, precio) values ('El aleph','Borges','Emece',25.00);
insert into libros (titulo, autor, editorial, precio) values ('Martin Fierro','Jose Hernandez','Planeta',35.50);
insert into libros (titulo, autor, editorial, precio) values ('Aprenda PHP','Mario Molina','Emece',45.50);
insert into libros (titulo, autor, editorial, precio) values ('Cervantes y el quijote','Borges','Emece',25);
insert into libros (titulo, autor, editorial, precio) values ('Matematica estas ahi','Paenza','Siglo XXI',15);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 3. Muestre todos los registros (5 registros)

```sql
select * from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	25
Martin Fierro	Jose Hernandez	Planeta	35,5
Aprenda PHP	Mario Molina	Emece	45,5
Cervantes y el quijote	Borges	Emece	25
Matematica estas ahi	Paenza	Siglo XXI	15
```
##### 4. Modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (1 registro)

```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 5. Nuevamente, modifique los registros cuyo autor sea igual a "Paenza", por "Adrian Paenza" (ningún registro afectado porque ninguno cumple la condición)

```sql
update libros set autor='Adrian Paenza' where autor='Paenza';
```
Salida del Script

```sh
0 filas actualizadas.
```
##### 6. Actualice el precio del libro de "Mario Molina" a 27 pesos (1 registro)

```sql
update libros set precio=27 where autor='Mario Molina';
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 7. Actualice el valor del campo "editorial" por "Emece S.A.", para todos los registros cuya editorial sea igual a "Emece" (3 registros)

```sql
update libros set editorial='Emece S.A.' where editorial='Emece';
```
Salida del Script

```sh
3 filas actualizadas.
```
