# 10. Valores nulos (null)

## Ejercicios de laboratorio

### Trabajamos con la tabla "libros" de una librería.

#### 1. Eliminamos la tabla "libros":

```sql
drop table libros;
```
Salida del Script

```sh
Table LIBROS borrado.
```
#### 2. Creamos la tabla especificando que los campos "titulo" y "autor" no admitan valores nulos:

```sql
create table libros(
    titulo varchar2(30) not null,
    autor varchar2(30) not null,
    editorial varchar2(15) null,
    precio number(5,2)
);
```
Salida del Script

```sh
Table LIBROS creado.
```
Los campos "editorial" y "precio" si permiten valores nulos; el primero, porque lo especificamos colocando "null" en la definición del campo, el segundo lo asume por defecto.

#### 3. Agregamos un registro a la tabla con valor nulo para el campo "precio":

```sql
insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);
```
Salida del Script

```sh
1 fila insertadas.
```
#### 4. Veamos cómo se almacenó el registro:

```sql
select *from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	
```
No aparece ningún valor en la columna "precio".

#### 5. Ingresamos otro registro, con valor nulo para el campo "editorial", campo que admite valores "null":

```sql
insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,0);
```
Salida del Script

```sh
1 fila insertadas.
```
#### 6. Veamos cómo se almacenó el registro:

```sql
select *from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	
Alicia en el pais	Lewis Carroll		0
```
No aparece ningún valor en la columna "editorial".

#### 7. Ingresamos otro registro, con valor nulo para los dos campos que lo admiten:

```sql
insert into libros (titulo,autor,editorial,precio) values('Aprenda PHP','Mario Molina',null,null);
```
Salida del Script

```sh
1 fila insertadas.
```
#### 8. Veamos cómo se almacenó el registro:

```sql
select *from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	
Alicia en el pais	Lewis Carroll		0
Aprenda PHP	Mario Molina		
```
No aparece ningún valor en ambas columnas.

#### 9. Veamos lo que sucede si intentamos ingresar el valor "null" en campos que no lo admiten, como "titulo":

```sql
insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."LIBROS"."TITULO")
```sh
Error que empieza en la línea: 22 del comando -
insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25)
Error en la línea de comandos : 22 Columna : 59
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."LIBROS"."TITULO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
Aparece un mensaje indicando que no se puede realizar una inserción "null" y la sentencia no se ejecuta.

#### 10. Para ver cuáles campos admiten valores nulos y cuáles no, vemos la estructura de la tabla:

```sql
describe libros;
```
Salida del Script

```sh
Nombre    ¿Nulo?   Tipo         
--------- -------- ------------ 
TITULO    NOT NULL VARCHAR2(30) 
AUTOR     NOT NULL VARCHAR2(30) 
EDITORIAL          VARCHAR2(15) 
PRECIO             NUMBER(5,2)  
```
Nos muestra, en la columna "Null", que los campos "titulo" y "autor" están definidos "not null", es decir, no permiten valores nulos, los otros dos campos si los admiten.

#### 11. Dijimos que la cadena vacía es interpretada como valor "null". Vamos a ingresar un registro con cadena vacía para el campo "editorial":

```sql
insert into libros (titulo,autor,editorial,precio) values('Uno','Richard Bach','',18.50);
```
Salida del Script

```sh
1 fila insertadas.
```
#### 12. Veamos cómo se almacenó el registro:

```sql
select *from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	
Alicia en el pais	Lewis Carroll		0
Aprenda PHP	Mario Molina		
Uno	Richard Bach		18,5
```
No aparece ningún valor en la columna "editorial" del libro "Uno", almacenó "null".

#### 13. Intentamos ingresar una cadena vacía en el campo "titulo":

```sql
insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."LIBROS"."TITULO").
```sh
Error que empieza en la línea: 30 del comando -
insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22)
Error en la línea de comandos : 30 Columna : 59
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."LIBROS"."TITULO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
Mensaje de error indicando que el campo no admite valores nulos.

#### 14. Dijimos que una cadena de espacios NO es igual a una cadena vacía o valor "null". Vamos a ingresar un registro y en el campo "editorial" guardaremos una cadena de 3 espacios:

```sql
insert into libros (titulo,autor,editorial,precio)mvalues('Don quijote','Cervantes','   ',20);
```
Salida del Script

```sh
1 fila insertadas.
```
#### 15. Veamos cómo se almacenó el registro:

```sql
select *from libros;
```
Salida del Script

```sh
El aleph	Borges	Emece	
Alicia en el pais	Lewis Carroll		0
Aprenda PHP	Mario Molina		
Uno	Richard Bach		18,5
Don quijote	Cervantes	   	20
```
Se muestra la cadena de espacios.

#### 16. Recuperamos los registros que contengan en el campo "editorial" una cadena de 3 espacios:

```sql
select *from libros where editorial='   ';
```
Salida del Script

```sh
Don quijote	Cervantes	   	20
```
#### 17. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(30) not null,
    autor varchar2(30) not null,
    editorial varchar2(15) null,
    precio number(5,2)
);

insert into libros (titulo,autor,editorial,precio) values('El aleph','Borges','Emece',null);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('Alicia en el pais','Lewis Carroll',null,0);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('Aprenda PHP','Mario Molina',null,null);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values(null,'Borges','Siglo XXI',25);

describe libros;

insert into libros (titulo,autor,editorial,precio) values('Uno','Richard Bach','',18.50);

select *from libros;

insert into libros (titulo,autor,editorial,precio) values('','Richard Bach','Planeta',22);

insert into libros (titulo,autor,editorial,precio) values('Don quijote','Cervantes','   ',20);

select *from libros;

select *from libros where editorial='   ';
```

## Ejercicios propuestos

### Ejercicio 01

#### Una farmacia guarda información referente a sus medicamentos en una tabla llamada "medicamentos".

##### 1. Elimine la tabla y créela con la siguiente estructura:

```sql
drop table medicamentos;
```
Salida del Script

Nos da un error de ORA-00942: la tabla o vista no existe.

```sh
Error que empieza en la línea: 38 del comando :
drop table medicamentos
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
```sql
create table medicamentos(
    codigo number(5) not null,
    nombre varchar2(20) not null,
    laboratorio varchar2(20),
    precio number(5,2),
    cantidad number(3,0) not null
);
```
Salida del Script

```sh
Table MEDICAMENTOS creado.
```
##### 2. Visualice la estructura de la tabla "medicamentos" note que los campos "codigo", "nombre" y "cantidad", en la columna "Null" muestra "NOT NULL".

```sql
DESCRIBE medicamentos;
```
Salida del Script

```sh
Nombre      ¿Nulo?   Tipo         
----------- -------- ------------ 
CODIGO      NOT NULL NUMBER(5)    
NOMBRE      NOT NULL VARCHAR2(20) 
LABORATORIO          VARCHAR2(20) 
PRECIO               NUMBER(5,2)  
CANTIDAD    NOT NULL NUMBER(3)
```
##### 3. Ingrese algunos registros con valores "null" para los campos que lo admitan:

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(1,'Sertal gotas',null,null,100); 
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(2,'Sertal compuesto',null,8.90,150);
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(3,'Buscapina','Roche',null,200);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 4. Vea todos los registros.

```sql
SELECT * FROM medicamentos;
```
Salida del Script

```sh
1	Sertal gotas			100
2	Sertal compuesto		8,9	150
3	Buscapina	Roche		200
```
##### 5. Ingrese un registro con valor "0" para el precio y cadena vacía para el laboratorio.

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Aspirina','   ',0,200);
```
Salida del Script

```sh
1 fila insertadas.
```
##### 6. Intente ingresar un registro con cadena vacía para el nombre (mensaje de error)

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(5,'','farma',0,200);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."NOMBRE").
```sh
Error que empieza en la línea: 58 del comando -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(5,'','farma',0,200)
Error en la línea de comandos : 58 Columna : 79
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."NOMBRE")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 7. Intente ingresar un registro con valor nulo para un campo que no lo admite (aparece un mensaje de error)

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(null,'','farma',0,200);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."CODIGO").
```sh
Error que empieza en la línea: 60 del comando -
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(null,'','farma',0,200)
Error en la línea de comandos : 60 Columna : 77
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."MEDICAMENTOS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 8. Ingrese un registro con una cadena de 1 espacio para el laboratorio.

```sql
insert into medicamentos (codigo,nombre,laboratorio,precio,cantidad) values(4,'Aspirina',' ',10,200);
```
Salida del Script

```sh
1 fila insertadas.
```
##### 9. Recupere los registros cuyo laboratorio contenga 1 espacio (1 registro)

```sql
select * from medicamentos where laboratorio=' ';
```
Salida del Script

```sh
4	Aspirina	 	10	200
```
##### 10. Recupere los registros cuyo laboratorio sea distinto de ' '(cadena de 1 espacio) (1 registro)

```sql
select * from medicamentos where laboratorio<>' ';
```
Salida del Script

```sh
3	Buscapina	Roche		200
4	Aspirina	   	0	200
5	   	farma	0	200
5	   	farma	0	200
```
### Ejercicio 02

#### Trabaje con la tabla que almacena los datos sobre películas, llamada "peliculas".

##### 1. Elimine la tabla:

```sql
DROP TABLE peliculas;
```
Salida del Script

```sh
Table PELICULAS borrado.
```
##### 2. Créela con la siguiente estructura:

```sql
create table peliculas(
    codigo number(4) not null,
    titulo varchar2(40) not null,
    actor varchar2(20),
    duracion number(3)
);
```
Salida del Script

```sh
Table PELICULAS creado.
```
##### 3. Visualice la estructura de la tabla. note que el campo "codigo" y "titulo", en la columna "Null" muestran "NOT NULL".

```sql
DESCRIBE peliculas;
```
Salida del Script

```sh
Nombre   ¿Nulo?   Tipo         
-------- -------- ------------ 
CODIGO   NOT NULL NUMBER(4)    
TITULO   NOT NULL VARCHAR2(40) 
ACTOR             VARCHAR2(20) 
DURACION          NUMBER(3)   
```
##### 4. Ingrese los siguientes registros:

```sql
insert into peliculas (codigo,titulo,actor,duracion) values(1,'Mision imposible','Tom Cruise',120);
insert into peliculas (codigo,titulo,actor,duracion) values(2,'Harry Potter y la piedra filosofal',null,180);
insert into peliculas (codigo,titulo,actor,duracion) values(3,'Harry Potter y la camara secreta','Daniel R.',null);
insert into peliculas (codigo,titulo,actor,duracion) values(0,'Mision imposible 2','',150);
insert into peliculas (codigo,titulo,actor,duracion) values(4,'Titanic','L. Di Caprio',220);
insert into peliculas (codigo,titulo,actor,duracion) values(5,'Mujer bonita','R. Gere.J. Roberts',0);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 5. Recupere todos los registros para ver cómo Oracle los almacenó.

```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal		180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2		150
4	Titanic	L. Di Caprio	220
5	Mujer bonita	R. Gere.J. Roberts	0
```
##### 6. Intente ingresar un registro con valor nulo para campos que no lo admiten (aparece un mensaje de error)

```sql
insert into peliculas (codigo,titulo,actor,duracion) values('','Mujer bonita','R. Gere.J. Roberts',0);
```
Salida del Script

Nos da un error de ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."PELICULAS"."CODIGO").
```sh
Error que empieza en la línea: 88 del comando -
insert into peliculas (codigo,titulo,actor,duracion) values('','Mujer bonita','R. Gere.J. Roberts',0)
Error en la línea de comandos : 88 Columna : 61
Informe de error -
Error SQL: ORA-01400: no se puede realizar una inserción NULL en ("DANIEL"."PELICULAS"."CODIGO")
01400. 00000 -  "cannot insert NULL into (%s)"
*Cause:    An attempt was made to insert NULL into previously listed objects.
*Action:   These objects cannot accept NULL values.
```
##### 7. Muestre todos los registros.

```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal		180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2		150
4	Titanic	L. Di Caprio	220
5	Mujer bonita	R. Gere.J. Roberts	0
```
##### 8. Actualice la película en cuyo campo "duracion" hay 0 por "null" (1 registro)

```sql
update peliculas set duracion=null where duracion=0;
```
Salida del Script

```sh
1 fila actualizadas.
```
##### 9. Recupere todos los registros.

```sql
SELECT * FROM peliculas;
```
Salida del Script

```sh
1	Mision imposible	Tom Cruise	120
2	Harry Potter y la piedra filosofal		180
3	Harry Potter y la camara secreta	Daniel R.	
0	Mision imposible 2		150
4	Titanic	L. Di Caprio	220
5	Mujer bonita	R. Gere.J. Roberts	
```

