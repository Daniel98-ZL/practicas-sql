# 6. Operadores relacionales
## Ejercicios de laboratorio

### Trabajamos con la tabla "libros" de una librería.
#### 1. Eliminamos la tabla "libros":

```sql
drop table libros;
```
Salida Script

```sh
Table LIBROS borrado.
```

#### 2. La creamos con la siguiente estructura:

```sql
create table libros(
    titulo varchar2(30),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);
```
Salida del Script

```sh
Table LIBROS creado.
```

#### 3. Agregamos registros a la tabla:

```sql
insert into libros (titulo,autor,editorial,precio) values ('El aleph','Borges','Emece',24.50);
insert into libros (titulo,autor,editorial,precio) values ('Martin Fierro','Jose Hernandez','Emece',16.00);
insert into libros (titulo,autor,editorial,precio) values ('Aprenda PHP','Mario Molina','Emece',35.40);
insert into libros (titulo,autor,editorial,precio) values ('Cervantes y el quijote','Borges','Paidos',50.90);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```

#### 4. Seleccionamos los registros cuyo autor sea diferente de 'Borges':

```sql
select *from libros where autor<>'Borges';
```
Salida del Script

```sh
Martin Fierro	Jose Hernandez	Emece	16
Aprenda PHP	Mario Molina	Emece	35,4
```

#### 5. Seleccionamos los registros cuyo precio supere los 20 pesos, sólo el título y precio:

```sql
select titulo,precio from libros where precio>20;
```
Salida del Script

```sh
El aleph	24,5
Aprenda PHP	35,4
Cervantes y el quijote	50,9
```
#### 6. Recuperamos aquellos libros cuyo precio es menor o igual a 30:

```sql
select *from libros where precio<=30;
```
Salida del Script

```sh
El aleph	Borges	Emece	24,5
Martin Fierro	Jose Hernandez	Emece	16
```

#### 7. Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(30),
    autor varchar2(30),
    editorial varchar2(15),
    precio number(5,2)
);

insert into libros (titulo,autor,editorial,precio) values ('El aleph','Borges','Emece',24.50);
insert into libros (titulo,autor,editorial,precio) values ('Martin Fierro','Jose Hernandez','Emece',16.00);
insert into libros (titulo,autor,editorial,precio) values ('Aprenda PHP','Mario Molina','Emece',35.40);
insert into libros (titulo,autor,editorial,precio) values ('Cervantes y el quijote','Borges','Paidos',50.90);

select *from libros where autor<>'Borges';

select titulo,precio from libros where precio>20;

select *from libros where precio<=30;
```

## Ejercicios propuesto

### Ejercicio 01

#### Un comercio que vende artículos de computación registra los datos de sus artículos en una tabla con ese nombre.

##### 1. Elimine "articulos"

```sql
drop table articulos;
```
Salida del Script

```sh
Table ARTICULOS borrado.
```
##### 2. Cree la tabla, con la siguiente estructura:

```sql
create table articulos(
    codigo number(5),
    nombre varchar2(20),
    descripcion varchar2(30),
    precio number(6,2),
    cantidad number(3)
);
 ```
Salida del Script

```sh
Table ARTICULOS creado.
```

##### 3. Vea la estructura de la tabla.

```sql
DESCRIBE articulos;
```
Salida del Script

```sh
Nombre      ¿Nulo? Tipo         
----------- ------ ------------ 
CODIGO             NUMBER(5)    
NOMBRE             VARCHAR2(20) 
DESCRIPCION        VARCHAR2(30) 
PRECIO             NUMBER(6,2)  
CANTIDAD           NUMBER(3)    
```
##### 4. Ingrese algunos registros:

```sql
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (1,'impresora','Epson Stylus C45',400.80,20);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (2,'impresora','Epson Stylus C85',500,30);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (3,'monitor','Samsung 14',800,10);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (4,'teclado','ingles Biswal',100,50);
insert into articulos (codigo, nombre, descripcion, precio,cantidad) values (5,'teclado','español Biswal',90,50);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 5. Seleccione los datos de las impresoras (2 registros)

```sql
select * from articulos where nombre='impresora';
```
Salida del Script

```sh
1	impresora	Epson Stylus C45	400,8	20
2	impresora	Epson Stylus C85	500	30
```
##### 6. Seleccione los artículos cuyo precio sea mayor o igual a 400 (3 registros)

```sql
select * from articulos where precio>=400;
```
Salida del Script

```sh
1	impresora	Epson Stylus C45	400,8	20
2	impresora	Epson Stylus C85	500	30
3	monitor	Samsung 14	800	10
```

##### 7. Seleccione el código y nombre de los artículos cuya cantidad sea menor a 30 (2 registros)

```sql
select codigo,nombre from articulos where cantidad<30;
```
Salida del Script

```sh
1	impresora
3	monitor
```
##### 8. Selecciones el nombre y descripción de los artículos que NO cuesten $100 (4 registros)

```sql
select nombre,descripcion from articulos where precio<>100;
```
Salida del Script

```sh
impresora	Epson Stylus C45
impresora	Epson Stylus C85
monitor	Samsung 14
teclado	español Biswal
```
### Ejercicio 02

#### Un video club que alquila películas en video almacena la información de sus películas en alquiler en una tabla denominada "peliculas".

##### 1. Elimine la tabla.

```sql
drop table peliculas;
```
Salida del Script

```sh
Table PELICULAS borrado.
```
##### 2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table peliculas(
    titulo varchar2(20),
    actor varchar2(20),
    duracion number(3),
    cantidad number(1)
);
 ```
Salida del Script

```sh
Table PELICULAS creado.
```
##### 3. Ingrese los siguientes registros:

```sql
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',120,3);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',180,4);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Mujer bonita','Julia R.',90,1);
insert into peliculas (titulo, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',80,2);
```
Salida del Script

```sh
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.
```
##### 4. Seleccione las películas cuya duración no supere los 90 minutos (2 registros)

```sql
select * from peliculas where duracion<=90;
```
Salida del Script

```sh
Mujer bonita	Julia R.	90	1
Elsa y Fred	China Zorrilla	80	2
```

##### 5. Seleccione el título de todas las películas en las que el actor NO sea "Tom Cruise" (2 registros)

```sql
select titulo from peliculas where actor<>'Tom Cruise';
```
Salida del Script

```sh
Mujer bonita
Elsa y Fred
```
##### 6. Muestre todos los campos, excepto "duracion", de todas las películas de las que haya más de 2 copias (2 registros)

```sql
select titulo,actor,cantidad from peliculas where cantidad>2;
```
Salida del Script

```sh
Mision imposible	Tom Cruise	3
Mision imposible 2	Tom Cruise	4
```