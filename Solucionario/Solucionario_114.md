# 114. Disparadores (habilitar y deshabilitar)

## Práctica de laboratorio

Una empresa almacena los datos de sus empleados en una tabla denominada "empleados". En una tabla denominada "controlCambios" guarda los cambios que se realizan en la tabla "empleados", en ella almacena el nombre del usuario que realiza la modificación, la fecha, el valor anterior del campo modificado y el nuevo valor.

Eliminamos las tablas:

```sql
drop table empleados;
drop table controlCambios;
```

Creamos las tablas, con las siguientes estructuras:

```sql
create table empleados(
    documento char(8) not null,
    nombre varchar2(30) not null,
    domicilio varchar2(30),
    seccion varchar2(20)
);

create table controlCambios(
    usuario varchar2(30),
    fecha date,
    datoanterior varchar2(30),
    datonuevo varchar2(30)
);
```

Ingresamos algunos registros:

```sql
insert into empleados values('22222222','Ana Acosta','Bulnes 56','Secretaria');
insert into empleados values('23333333','Bernardo Bustos','Bulnes 188','Contaduria');
insert into empleados values('24444444','Carlos Caseres','Caseros 364','Sistemas');
insert into empleados values('25555555','Diana Duarte','Colon 1234','Sistemas');
insert into empleados values('26666666','Diana Duarte','Colon 897','Sistemas');
insert into empleados values('27777777','Matilda Morales','Colon 542','Gerencia');
```

Creamos un disparador que se active cuando modificamos algún campo de "empleados" y almacene en "controlCambios" el nombre del usuario que realiza la actualización, la fecha, el dato que se cambia y el nuevo valor:

```sql
create or replace trigger tr_actualizar_empleados
    beforeupdate
    on empleados
    for each row
begin
    if updating('documento') then
        insert into controlCambios values(user,sysdate, :old.documento, :new.documento);
    end if;
    if updating('nombre') then
        insert into controlCambios values(user,sysdate, :old.nombre, :new.nombre);
    end if;
    if updating('domicilio') then
        insert into controlCambios values(user,sysdate, :old.domicilio, :new.domicilio);
    end if;
    if updating('seccion') then
        insert into controlCambios values(user,sysdate, :old.seccion, :new.seccion);
    end if;
end tr_actualizar_empleados;
/
```

Creamos otro desencadenador que se active cuando ingresamos un nuevo registro en "empleados", debe almacenar en "controlCambios" el nombre del usuario que realiza el ingreso, la fecha, "null" en "datoanterior" (porque se dispara con una inserción) y en "datonuevo" el documento:

```sql
create or replace trigger tr_ingresar_empleados
    before insert
    on empleados
    for each row
begin
    insert into controlCambios values(user,sysdate, null, :new.documento);
end tr_ingresar_empleados;
/
```

Creamos un tercer trigger sobre "empleados" que se active cuando eliminamos un registro en "empleados", debe almacenar en "controlCambios" el nombre del usuario que realiza la eliminación, la fecha, el documento en "datoanterior" y "null" en "datonuevo":

```sql
create or replace trigger tr_eliminar_empleados
    beforedelete
    on empleados
    for each row
begin
    insert into controlCambios values(user,sysdate, :old.documento, null);
end tr_eliminar_empleados;
/
```

Los tres triggers están habilitados. Consultamos el diccionario "user_triggers" para corroborarlo:

```sql
select trigger_name, triggering_event, status
from user_triggers
where trigger_name like 'TR%EMPLEADOS';
```

Vamos a ingresar un empleado y comprobar que el trigger "tr_ingresar_empleados" se dispara recuperando los registros de "controlCambios":

```sql
insert into empleados values('28888888','Pedro Perez','Peru 374','Secretaria');
select *from controlCambios;
```

Deshabilitamos el trigger "tr_ingresar_empleados":

```sql
alter  trigger tr_ingresar_empleados disable;
```

Consultamos el diccionario "user_triggers" para corroborarlo:

```sql
select trigger_name, status
from user_triggers
where trigger_name like 'TR%EMPLEADOS';
```

El trigger "tr_ingresar_empleados" está deshabilitado, "tr_actualizar_empleados" y "tr_elimnar_empleados" están habilitados.

Vamos a ingresar un empleado y comprobar que el trigger de inserción no se dispara recuperando los registros de "controlCambios":

```sql
insert into empleados values('29999999','Rosa Rodriguez','Rivadavia 627','Secretaria');
select *from controlCambios;
```

Vamos a actualizar el domicilio de un empleado y comprobar que el trigger de actualización se dispara recuperando los registros de "controlCambios":

```sql
update empleados set domicilio='Bulnes 567' where documento='22222222';
select *from controlCambios;
```

Deshabilitamos el trigger "tr_actualizar_empleados":

```sql
alter  trigger tr_actualizar_empleados disable;
```

Consultamos el diccionario "user_triggers" para corroborarlo:

```sql
select trigger_name, status
from user_triggers
where trigger_name like 'TR%EMPLEADOS';
```

Los triggers "tr_ingresar_empleados" y "tr_actualizar_empleados" están deshabilitados, "tr_eliminar_empleados" está habilitado.

Vamos a borrar un empleado de "empleados" y comprobar que el trigger de borrado se disparó recuperando los registros de "controlCambios":

```sql
delete from empleados where documento= '29999999';
select *from controlCambios;
```

Deshabilitamos el trigger "tr_eliminar_empleados":

```sql
alter  trigger tr_eliminar_empleados disable;
```

Consultamos el diccionario "user_triggers" para comprobarlo:

```sql
select trigger_name, status
from user_triggers
where table_name = 'EMPLEADOS';
```

Los tres trigger establecidos sobre "empleados" están deshabilitados.

Eliminamos un empleado de "empleados" y comprobamos que el trigger de borrado no se dispara recuperando los registros de "controlCambios":

```sql
delete from empleados where documento= '28888888';
select *from controlCambios;
```

Habilitamos el trigger "tr_actualizar_empleados":

```sql
alter  trigger tr_actualizar_empleados enable;
```

Actualizamos la sección de un empleado y comprobamos que el trigger de actualización se dispara recuperando los registros de "controlCambios":

```sql
update empleados set seccion='Sistemas' where documento='23333333';
select *from controlCambios;
```

Habilitamos todos los triggers establecidos sobre "empleados":

```sql
alter table empleados enable all triggers;
```

Consultamos el diccionario "user_triggers" para comprobar que el estado (status) de todos los triggers establecidos sobre "empleados" es habilitado:

```sql
select trigger_name, triggering_event, status
from user_triggers
where table_name = 'EMPLEADOS';
```

Los tres trigger establecidos sobre "empleados" han sido habilitados. Se activarán ante cualquier sentencia "insert", "update" y "delete".

## Ejercicios propuestos

Una librería almacena los datos de sus libros en una tabla denominada "libros" y controla las acciones que los empleados realizan sobre dicha tabla almacenando en la tabla "control" el nombre del usuario, la fecha, y el tipo de modificación que se realizó sobre la tabla "libros".

1. Elimine la tabla "libros" y la tabla "control":

```sql
drop table libros;
drop table control;
```

2. Cree las tablas con las siguientes estructuras:

```sql
create table libros(
    codigo number(6),
    titulo varchar2(40),
    autor varchar2(30),
    editorial varchar2(20),
    precio number(6,2)
);

create table control(
    usuario varchar2(30),
    fecha date,
    operacion varchar2(20)
);
```

3. Ingrese algunos registros en "libros":

```sql
insert into libros values(100,'Uno','Richard Bach','Planeta',25);
insert into libros values(103,'El aleph','Borges','Emece',28);
insert into libros values(105,'Matematica estas ahi','Paenza','Nuevo siglo',12);
insert into libros values(120,'Aprenda PHP','Molina Mario','Nuevo siglo',55);
insert into libros values(145,'Alicia en el pais de las maravillas','Carroll','Planeta',35);
```

4. Cree un disparador que se active cuando modificamos algún campo de "libros" y almacene en "control" el nombre del usuario que realiza la actualización, la fecha y en "operacion" coloque el nombre del campo actualizado
```sql
create or replace trigger tr_actualizar_libros
after update on libros
for each row
begin
    insert into control values(USER, SYSDATE, 'actualizacion');
end;
/

```
5. Cree otro desencadenador que se active cuando ingresamos un nuevo registro en "libros", debe almacenar en "control" el nombre del usuario que realiza el ingreso, la fecha e "insercion" en "operacion"
```sql
create or replace trigger tr_ingresar_libros
after insert on libros
for each row
begin
    insert into control values(USER, SYSDATE, 'insercion');
end;
/

```
6. Cree un tercer trigger sobre "libros" que se active cuando eliminamos un registro de "libros", debe almacenar en "control" el nombre del usuario que realiza la eliminación, la fecha y "borrado" en "operacion"
```sql
create or replace trigger tr_eliminar_libros
after delete on libros
for each row
begin
    insert into control values(USER, SYSDATE, 'borrado');
end;
/

```
7. Los tres triggers están habilitados. Consultamos el diccionario "user_triggers" para corroborarlo
```sql
select trigger_name, status from user_triggers;

```
8. Ingrese un libro y compruebe que el trigger "tr_ingresar_libros" se dispara recuperando los registros de "control"
```sql
insert into libros values(152,'El anillo del hechicero','Gaskin','Planeta',22);
select * from control;

```
9. Deshabilite el trigger "tr_ingresar_libros"
```sql
alter trigger tr_ingresar_libros disable;

```
10. Consulte el diccionario "user_triggers" para corroborarlo
El trigger "tr_ingresar_libros" está deshabilitado, "tr_actualizar_libros" y "tr_eliminar_libros" están habilitados.
```sql
select trigger_name, status from user_triggers;

```
11. Ingrese un libro y compruebe que el trigger de inserción no se dispara recuperando los registros de "control":

```sql
insert into libros values(152,'El anillo del hechicero','Gaskin','Planeta',22);
select *from control;
```

12. Actualice la editorial de varios libros y compruebe que el trigger de actualización se dispara recuperando los registros de "control"
```sql
update libros set editorial = 'Salamandra' where codigo in (103, 145);
select * from control;

```
13. Deshabilite el trigger "tr_actualizar_libros"
```sql
alter trigger tr_actualizar_libros disable;

```
14. Consulte el diccionario "user_triggers" para corroborarlo
Los triggers "tr_ingresar_libros" y "tr_actualizar_libros" están deshabilitados, "tr_eliminar_libros" está habilitado.
```sql
select trigger_name, status from user_triggers;

```
15. Borre un libro de "libros" y compruebe que el trigger de borrado se disparó recuperando los registros de "control"
```sql
delete from libros where codigo = 120;
select * from control;

```
16. Deshabilite el trigger "tr_eliminar_libros"
```sql
alter trigger tr_eliminar_libros disable;

```
17. Consulte el diccionario "user_triggers" para comprobarlo
Los tres trigger establecidos sobre "empleados" están deshabilitados.
```sql
select trigger_name, status from user_triggers;

```
18. Elimine un libro de "libros" y compruebe que tal registro se eliminó de "libros" pero que el trigger de borrado no se dispara recuperando los registros de "control"
```sql
delete from libros where codigo = 105;
select * from control;

```
19. Habilite el trigger "tr_actualizar_libros"
```sql
alter trigger tr_actualizar_libros enable;

```
20. Actualice el autor de un libro y compruebe que el trigger de actualización se dispara recuperando los registros de "control"
```sql
update libros set autor = 'Tolkien' where codigo = 100;
select * from control;

```
21. Habilite todos los triggers establecidos sobre "libros"
```sql
alter trigger tr_ingresar_libros enable;
alter trigger tr_eliminar_libros enable;

```
22. Consulte el diccionario "user_triggers" para comprobar que el estado (status) de todos los triggers establecidos sobre "libros" es habilitado
Los tres trigger establecidos sobre "libros" han sido habilitados. Se activarán ante cualquier sentencia "insert", "update" y "delete".
```sql
select trigger_name, status from user_triggers;

```